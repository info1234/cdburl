<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
use App\User;
class CheckPermission {

    /**

     * Handle an incoming request.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \Closure  $next

     * @return mixed

     */

    public function handle($request, Closure $next, $permission) {
        if(Auth::user()->type == 2) {
			return redirect('/home123');
		} else {
			return redirect('/dashboard');
		}
        return $next($request);
    }

}
