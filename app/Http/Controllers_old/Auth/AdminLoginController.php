<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Validator;
use App\Admin;
class AdminLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin');
    }
    public function showLoginForm()
    {
      return view('auth.admin-login');
    }
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('admin.dashboard'));
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
	
	public function showRegistrationForm()
    {	return redirect()->action('Auth\AdminLoginController@showLoginForm');
        return view('auth.admin-register');
    }
	
	public function register(Request $request) {
		$this->validator($request->all())->validate();

        $user = $this->create($request->all()); //event(new Registered($user = $this->create($request->all())));
		
        Auth::guard('admin')->login($user);

        return $this->registered($request, $user)
                        ?: redirect()->intended(route('admin.dashboard'));
	}
	
	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6|confirmed',
        ]);
    }
	
	protected function create(array $data)
    {
        return Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	
	protected function registered(Request $request, $user)
    {
        //
    }
}
