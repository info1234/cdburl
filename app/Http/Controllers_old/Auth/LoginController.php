<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
	protected function authenticated(Request $request, $user) {
        if($user->type == 2) {
            return redirect()->intended('store'); // it will be according to your routes.

        } else {
            return redirect()->intended('/dashboard'); // it also be according to your need and routes
        }
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		//$this->redirectTo = url()->previous();
        $this->middleware('guest', ['except' => 'logout']);
    }
	
	/*Custom*/
	protected function validator($data) {
        return Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
    }
	
	public function login(Request $request) {
		$this->validateLogin($request);
		if($request->ajax()){
			
			if ($this->hasTooManyLoginAttempts($request)) {
				$this->fireLockoutEvent($request);
				return $this->sendLockoutResponse($request);
			}
			
			if ($this->attemptLogin($request)) {
				return $this->sendLoginResponse($request);
			}
			
			$this->incrementLoginAttempts($request);
			
			return $this->sendFailedLoginResponse($request);
		} else {
			if ($this->hasTooManyLoginAttempts($request)) {
				$this->fireLockoutEvent($request);
				return $this->sendLockoutResponse($request);
			}
			
			if ($this->attemptLogin($request)) {
				return $this->sendLoginResponse($request);
			}
			$this->incrementLoginAttempts($request);
			
			return $this->sendFailedLoginResponse($request);
		}
	}
	
	protected function validateLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }
}
