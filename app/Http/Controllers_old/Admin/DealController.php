<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Deal;
use App\Store;
use App\Category;
use App\Image;
use Redirect, Validator;
use DB;
use App\Brand;
class DealController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$deals = Deal::select('id', 'name', 'deal_type', 'deal_type_price', 'store_id', 'category_id',
		'brand_id', 'approval')->get();
		return view('admin.deal.index',['deals' => $deals]);
    }
	
	public function create(Request $request) {
		$dealObj = null;
		$data = $request->all();
		if($request->isMethod('post')) {
			$validator = Validator::make($data, [
				'name' => 'required',
				'url' => 'required',
				//'category' => 'required',
			]);
			$shipping = $url = null;
			if(isset($data['url'])) {
				$url = $data['url'];
			}
			$domain = pathinfo(get_domain($url), PATHINFO_FILENAME);
			if($domain != 'amazon') {
				$validator->after(function($validator) {
					$validator->errors()->add('fendingDateield', 'Please enter valid Url.');
				});
			}

			$category = null;
			if(!isset($data['category'])) {
				$catObj = Category::select("id")->where("slug_name", "miscellaneous")->first();
				if($catObj != null) {
					$category = $catObj->id;
				} else {
					$validator->after(function($validator) {
						$validator->errors()->add('category_id', 'The category field is required.');
					});
				}
			} else {
				$category = $data['category'];
			}
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			
			if(isset($data['shipping'])) {
				$shipping = $data['shipping'];
			}
			if(isset($data['url'])) {
				$url = $data['url'];
			}
			$slugName = $data['name'];
			$dataSlug = Deal::where("name", $slugName)->count();
			if($dataSlug > 0) {
				$slugName = str_slug($data['name']).'-'.$dataSlug;
			} else {
				$slugName = str_slug($data['name']);
			}
			$price = 0;
			if(isset($data['price'])) {
				$price = $data['price'];
			}
			$total = 0;
			if(isset($data['totalprice'])) {
				$total = $data['totalprice'];
			}
			$response_cookie = null;
			if(isset($data['response_cookie'])) {
				$response_cookie = $data['response_cookie'];
			}
			
			$dataType = isset($data['deal_type']) ? $data['deal_type'] : null;
			$insertData = ['name' => $data['name'], 'category_id' => $category, 'slug_name' => $slugName, 'response_cookie' => $response_cookie,
			'store_id' => $data['store'], 'description' => $data['description'], 'url' => $url, 'price' => $price, 'total' => $total];
			//echo "<pre>"; print_r($insertData); die;
			$status = Deal::create($insertData);
			
			if($status) {
				if(isset($status->id)) {
					$count = 0;
					if(isset($data['imagethumb']) && count($data['imagethumb']) > 0) {
						foreach($data['imagethumb'] as $key => $imagethumbobj) {
							$count = Image::where(['item_id' => $status->id, 'main_image' => $status->id])->whereNull('parent_id')->get()->count();
							if($count == 0) {
								$insertImageData = ['name' => $imagethumbobj, 'item_id' => $status->id,
								'main_image' => $status->id];
								$imageObj = Image::create($insertImageData);
								if($imageObj) {
									if(isset($data['thumb']) && count($data['thumb']) > 0 && isset($data['thumb'][$key])) {
										Image::create(['name' => $data['thumb'][$key], 'item_id' => $status->id,
										'parent_id' => $imageObj->id, 'type' => 2]);
									}
								}
							} else {
								$insertImageData = ['name' => $imagethumbobj, 'item_id' => $status->id];
								$imageObj = Image::create($insertImageData);
								if($imageObj) {
									if(isset($data['thumb']) && count($data['thumb']) > 0 && isset($data['thumb'][$key])) {
										Image::create(['name' => $data['thumb'][$key], 'item_id' => $status->id,
										'parent_id' => $imageObj->id, 'type' => 2]);
									}
								}
							}
							
						}
					}
					if(count($request->file()) > 0) {
						$count = Image::where(['item_id' => $status->id, 'main_image' => $status->id])->whereNull('parent_id')->get()->count();
						foreach($request->file('image') as $file) {
							$tempName = file_get_contents($file->getPathName());
							$ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
							$imageName = 'data:image/' . $ext . ';base64,' . base64_encode($tempName);
							
							if($count == 0) {
								$insertImageData = ['name' => $imageName, 'item_id' => $status->id,
								'main_image' => $status->id];
							} else {
								$insertImageData = ['name' => $imageName, 'item_id' => $status->id];
							}
							$count = 1;
							Image::create($insertImageData);
						}
					}
				}
				return redirect()->back()->with('message', 'Deal successfully create.');			
			}
		}
		
		$categories = Category::leftjoin('categories AS P', 'categories.id', '=', 'P.parent_id')
                     ->select('categories.id', 'categories.name', 'P.id as pid', 'categories.slug_name')
					 ->whereNull('categories.parent_id')
					 ->groupBy('categories.id')
                     ->get();
		$brands = Brand::select("id", "name")->get();
		$stores = Store::select('id', 'name')->get();
		$images = [];
		return view('admin.deal.form',['dealObj' => $dealObj, 'categories' => $categories,
		'stores' => $stores, 'brands' => $brands, 'images' => $images]);
	}
	
	public function edit($id, Request $request) {
		$categories = Category::leftjoin('categories AS P', 'categories.id', '=', 'P.parent_id')
                     ->select('categories.id', 'categories.name', 'P.id as pid', 'categories.slug_name')
					 ->whereNull('categories.parent_id')
					 ->groupBy('categories.id')
                     ->get();
		$dealObj = Deal::where(['id' => $id])->first();
		if($dealObj == null) {
			return Redirect::route('admin.deal.index');
		}
		$stores = Store::select('id', 'name')->get();
		$brands = Brand::select("id", "name")->get();
		if($request->isMethod('post')) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'name' => 'required',
				'url' => 'required',
				'category' => 'required',
			]);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$dataType = isset($data['deal_type']) ? $data['deal_type'] : null;
			//$status = Category::where('id',$obj->id)->update($updateData);
			$shipping = null;
			if(isset($data['shipping'])) {
				$shipping = $data['shipping'];
			}
			$url = null;
			if(isset($data['url'])) {
				$url = $data['url'];
			}
			
			/* $slugName = str_slug($data['name']);
			$dataSlug = Deal::where("name", $slugName)->count();
			if($dataSlug > 0) {
				$slugName = $slugName.'-'.$dataSlug;
			} */
			$slugName = $data['name'];
			$dataSlug = Deal::where("name", $slugName)->count();
			if($dataSlug > 0) {
				$slugName = str_slug($data['name']).'-'.$dataSlug;
			} else {
				$slugName = str_slug($data['name']);
			}
			$price = 0;
			if(isset($data['price'])) {
				$price = $data['price'];
			}
			$total = 0;
			if(isset($data['totalprice'])) {
				$total = $data['totalprice'];
			}
			$response_cookie = null;
			if(isset($data['response_cookie'])) {
				$response_cookie = $data['response_cookie'];
			}
			
			$updateData = ['name' => $data['name'], 'category_id' => $data['category'], 'store_id' => $data['store'], 'url' => $url,
			'description' => $data['description'], 'slug_name' => $slugName, 'price' => $price, 'total' => $total, 'response_cookie' => $response_cookie];
			$status = Deal::where('id',$dealObj->id)->update($updateData);
			if($status) {
				if(isset($id)) {
					$count = 0;
					if(count($request->file()) > 0) {
						$count = Image::where(['item_id' => $id, 'main_image' => $id])->whereNull('parent_id')->get()->count();
						foreach($request->file('image') as $file) {
							/* $imageName = str_random() . time() .'.'. $file->getClientOriginalExtension();
							$file->move(
								base_path() . '/public/images/deal/', $imageName
							); */
							$tempName = file_get_contents($file->getPathName());
							$ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
							$imageName = 'data:image/' . $ext . ';base64,' . base64_encode($tempName);
							
							$insertImageData = ['name' => $imageName, 'item_id' => $id];
							Image::create($insertImageData);
						}
					}
					
					if(isset($data['imagethumb']) && count($data['imagethumb']) > 0) {
						foreach($data['imagethumb'] as $key => $imagethumbobj) {
							$insertImageData = ['name' => $imagethumbobj, 'item_id' => $id];
							$imageObj = Image::create($insertImageData);
							if($imageObj) {
								if(isset($data['thumb']) && count($data['thumb']) > 0 && isset($data['thumb'][$key])) {
									Image::create(['name' => $data['thumb'][$key], 'item_id' => $id,
									'parent_id' => $imageObj->id, 'type' => 2]);
								}
							}
						}
					}
					
					/* if(isset($data['setimagecontenttext'])) {
						$insertImage = ['name' => $data['setimagecontenttext'], 'item_id' => $id];
							Image::create($insertImage);
					} */
				}
				return Redirect::route('admin.deal.index');
			}
			return Redirect::route('admin.deal.index');
		}
		$images = Image::select("id", "name", "main_image", "item_id")->where("item_id", "=", $dealObj->id)->whereNull('parent_id')->get();
		/* foreach($images as $imageObj) {
			echo $imageObj->name.' '; die;
		} */
		return view('admin.deal.form',['dealObj' => $dealObj, 'categories' => $categories,
		'stores' => $stores, 'brands' => $brands, 'images' => $images]);
	}

	public function getcategoryById($id, Request $request) {
		//echo $id;
		if($request->ajax()) {
			$categories = Category::leftjoin('categories AS P', 'categories.id', '=', 'P.parent_id')
                     ->select('categories.id', 'categories.name', 'P.id as pid')
					 ->where(['categories.parent_id' => $id])
					 ->groupBy('categories.id')
                     ->get();
			//echo "<pre>"; print_r($categories); 
			return view('admin.category.categoryOption',['categories' => $categories]);
		}
	}
	
	public function setmainimage(Request $request) {
		if($request->ajax() && $request->isMethod('get')) {
			$data = $request->all();
			$imageid = $data['pkid'];
			$dealid = $data['dealid'];
			$data = Image::select("id")->where("item_id", "=", $dealid)->whereNotNull('main_image')->get();
			foreach($data as $obj) {
				$status = Image::where('id',$obj->id)->update(['main_image' => null]);
			}
			$image = Image::find($imageid);
			$image->main_image = $dealid;
			$status = $image->save();
			//$status = Image::where('id',$obj->id)->update(['main_image' => $dealid]);
			
			$images = Image::select("id", "name", "main_image", "item_id")->where("item_id", "=", $dealid)->whereNull('parent_id')->get();
			return view('admin.deal.deal_image_list',['images' => $images]);
		}
	}
	
	public function getdealcoupon($storeid, Request $request) {
		if($request->ajax() && $request->isMethod('get')) {
			$data = $request->all();
			$deals = Deal::select("id", "name")->where("store_id", "=", $storeid)->get();
			$dealData = [];
			foreach($deals as $obj) {
				//$dealData[] = ['id' => $obj->id, 'name' => $obj->name];
				$dealData[$obj->id] = $obj->name;
			}
			return response()->json(['status' => 'success', 'deals' => $dealData]);
		}
	}
	
	public function changestatus(Request $request) {
		if($request->ajax()) {
			if($request->isMethod('get')) {
				$params = $request->all();
				$pkid = $params['pkid'];
				$dealObj = Deal::find($pkid);
				if($dealObj) {
					$status = '';
					$dataDealStatus = '';
					$dealObj->approval = $params['status'];
					if($dealObj->save()) {
						if($params['status'] == 1) {
							$status = 'inActive';
							$dataDealStatus = 0;
						} else if($params['status'] == 0) {
							$status = 'Active';
							$dataDealStatus = 1;
						}
					}
				}
				return response()->json(['status' => 'success', 'html' => $status, 'datadealstatus' => $dataDealStatus]);
			}
		}
	}
	
	public function autosearch(Request $request) {
		if($request->ajax()) {
			if($request->isMethod('get')) {
				$data = $request->all();
				$search = $data['query'];
				$stores = \App\Store::select("id", "name")->where('name', 'LIKE', '%'.$search.'%')->get();
				$dataarr = [];
				foreach($stores as $obj) {
					$dataarr[] = ['id' => $obj->id, 'name' => $obj->name];
				}
				//return response()->json($dataarr);
				echo json_encode($dataarr);
			}
		}
		//return response()->json([1 => 'All', 2 => 'ABC']);

	}
	
	public function getcontentdata(Request $request) {
		if($request->ajax()) {
			$dataurl = $request->all();
			$url = $dataurl['url'];
			$domain = pathinfo(get_domain($url), PATHINFO_FILENAME);
			$content = [];
			if($domain == 'amazon') {
				$content = amazon($domain, $url);
			}
			return response()->json(['status' => 'success', $content]);
		}
	}

	public function deleteimage(Request $request) {
		if($request->ajax() && $request->isMethod('get')) {
			$data = $request->all();
			$pkid = $data['pkid'];
			$dealid = $data['dealid'];
			$image = Image::find($pkid);
			if($image->delete()) {
				//$thumbImage = Image::select($pkid);
				Image::where('parent_id', $pkid)->where('type', 2)->delete();
			}
			$images = Image::select("id", "name", "main_image", "item_id")->where("item_id", "=", $dealid)->whereNull('parent_id')->get();
			return view('admin.deal.deal_image_list',['images' => $images]);
		}
	}
	
	public function responsecookie(Request $request) {
		if($request->ajax() && $request->isMethod('get')) {
			return view('admin.deal.dealresponse',[]);
		}
	}
}
