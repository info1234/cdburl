<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Store;
use Redirect, Validator;
use App\Deal;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }
	
	public function index() {
		$commentCounts = [];
		$storeCounts = [];
		$totalStoreDatas = Deal::select("store_id")->groupBy('store_id')->get();
		$totalStores = count($totalStoreDatas);
		$totalCategories = \App\Category::select("id")->count();
		$totalDeals = Deal::select("id")->count();
		$totalComments = \App\Comment::select("id")->count();
		$deals = Deal::select("id", "name", "category_id", "store_id", "price", "total", "created_at")->orderBy("created_at", "desc")->get();
		
		 /*  \App\DealView::select( 
array(
    '*',
    \DB::raw('(SELECT count(*) FROM deal_views WHERE deal_id = id) as count_links')) 
)->with('deal_views')->orderBy('count_links','desc')->paginate(5);   
		die;  */
		
		$populerDeals = \App\DealView::select("deals.id", "deals.name")
		->join("deals", "deals.id", "=", "deal_views.deal_id")->groupBy("deal_views.deal_id")->get();
		$dealviews  = [];
        return view('admin.dashboard.index', ["totalDeals" => $totalDeals, "totalCategories" => $totalCategories,
		"totalStores" => $totalStores, 'totalComments' => $totalComments, "deals" => $deals, "populerDeals" => $populerDeals]);
    }
	
	public function changelogo(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'image' => 'mimes:jpeg,bmp,gif,png',
				//'image' => ''
			]);
			/* if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			} */
			if($request->file('image')) {
				if($validator->fails()) {
					return redirect()->back()->withErrors($validator->errors());
				}
				$count = \App\Setting::where("setting_key", 'header_logo')->count();
				if($count == 0) {
					$file = $request->file('image');
					$imageName = time().'headerlogo'.'.'. $file->getClientOriginalExtension();
					$status = \App\Setting::create(['name' => $imageName, "setting_key" => 'header_logo']);
					if($status) {
						$file->move(base_path() . '/public/images/logo/header/', $imageName);
					}
				} else {
					$file = $request->file('image');
					$imageName = time().'headerlogo'.'.'. $file->getClientOriginalExtension();
					$status = \App\Setting::where("setting_key", 'header_logo')->update(['name' => $imageName]);
					if($status) {
						$file->move(base_path() . '/public/images/logo/header/', $imageName);
					}
				}
			}
			
			if(isset($data['frontpagedeal']) && $data['frontpagedeal'] > 0) {
				$frontpagedeal = $data['frontpagedeal'];
			}
			
			
			
		}
		$data = $this->settingData();
		return view('admin.dashboard.setting', ['data' => $data['data']]);
	}
	
	private function settingData() {
		$data = \App\Logo::where("type", 1)->first();
		return array('data' => $data);
	}
	
	public function setting(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			//echo "<pre>"; print_r($data); die;
			if(isset($data['pageoption'])) {
				if($data['pageoption'] == 1) {
					$count = \App\Setting::where("setting_key", 'home_page_add')->count();
					if($count == 0) {
						$status = \App\Setting::create(['text' => $data['text'], "setting_key" => 'home_page_add', "value" => $data['homeadvertisementtitle']]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					} else {
						$status = \App\Setting::where("setting_key", 'home_page_add')->update(['text' => $data['text'], "value" => $data['homeadvertisementtitle']]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					}
				} else if($data['pageoption'] == 2) {
					$category = null;
					if(isset($data['category'])) {
						$category = $data['category'];
					}
					$count = \App\Setting::where("setting_key", 'deal_page_advertisement')->where("category_id", $category)->count();
					if($count == 0) {
						$status = \App\Setting::create(['text' => $data['text'], "setting_key" => 'deal_page_advertisement', "value" => $data['homeadvertisementtitle'], 'category_id' => $category]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					} else {
						$status = \App\Setting::where("setting_key", 'deal_page_advertisement')
						->where("category_id", $category)
						->update(['text' => $data['text'], "value" => $data['homeadvertisementtitle'], 'category_id' => $category]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					}
				}
			}
		}
		$data = $this->settingData();
		$headerlogoimage = \App\Setting::where("setting_key", 'header_logo')->first();
		$frontPerPage = \App\Setting::where("setting_key", 'frontdeal_per_page')->first();
		$couponPerPage = \App\Setting::where("setting_key", 'coupon_per_page')->first();
		return view('admin.dashboard.setting', ['headerlogoimage' => $headerlogoimage, 'frontPerPage' => $frontPerPage,
		'couponPerPage' => $couponPerPage]);
	}
   
}
