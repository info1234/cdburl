<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Coupon;
use App\Store;
use Redirect, Validator;
class CouponController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$coupons = Coupon::select('id', 'title')->get();
		return view('admin.coupon.index',['coupons' => $coupons]);
    }
	
	public function create(Request $request) {
		$couponObj = null;
		$data = $request->all();
		if($request->isMethod('post')) {
			
			$validator = Validator::make($data, [
				'title' => 'required',
				'coupon_type' => 'required',
				'image' => 'mimes:jpeg,bmp,gif,png',
				'store_id' => 'required'
				/* 'store_id' => 'required|unique:coupons,store_id,NULL,NULL,deal_id, ' . $request['deal_id'],
				'deal_id' => 'required|unique:coupons,deal_id,NULL,NULL,store_id, ' . $request['store_id'], */
			]);
			
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			//echo 'test'; die;
			$slugName = str_slug($data['title']);
			$imageName = null;
			if(count($request->file()) > 0) {
				$imageName = 'coupon' . time() . '.' .$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move(
					base_path() . '/public/images/coupon/', $imageName
				);
			}
			$insertData = ['title' => $data['title'], 'text' => $data['text'], 'store_id' => $data['store_id'],
			'coupon_type' => $data['coupon_type'], 'image' => $imageName, 'price' => $data['price'],
			'deal' => $data['deal'], 'coupon_type_price' => $data['coupon_type_price']];
			$coupon = new Coupon;
			$coupon->title = $data['title'];
			$coupon->text = $data['text'];
			$coupon->store_id = $data['store_id'];
			$coupon->coupon_type = $data['coupon_type'];
			$coupon->image = $imageName;
			
			$coupon->price = $data['price'];
			$coupon->deal = $data['deal'];
			$coupon->coupon_type_price = $data['coupon_type_price'];
			//$coupon->deal_id = $data['deal_id'];
			
			//$status = Coupon::create($data);
			//echo "<pre>"; print_r($insertData); die;
			if($coupon->save()) {
				return Redirect::route('admin.coupon.index');
			}
		}
		$stores = Store::select('id', 'name')->get();
		$deals = [];
		return view('admin.coupon.form',['couponObj' => $couponObj, 'stores' => $stores, 'deals' => $deals]);
	}
	
	public function edit($id, Request $request) {
		$stores = Store::select('id', 'name')->get();
		$couponObj = Coupon::select('id', 'title', 'store_id', 'coupon_type', 'image', 'text',
		'price', 'deal', 'coupon_type_price')->find($id);
		if($couponObj == null) {
			return Redirect::route('admin.coupon.index');
		}
		//echo "<pre>"; print_r($couponObj); die;
		$data = $request->all();
		if($request->isMethod('post')) {
			$validator = Validator::make($data, [
				'title' => 'required',
				'store_id' => 'required',
				'coupon_type' => 'required',
				'image' => 'mimes:jpeg,bmp,gif,png',
				//'deal_id' => 'required',
			]);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$slugName = str_slug($data['title']);
			$imageName = null;
			if(count($request->file()) > 0) {
				$imageName = 'coupon' . time() . '.' .$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move(
					base_path() . '/public/images/coupon/', $imageName
				);
			}
			$updateData = ['title' => $data['title'], 'text' => $data['text'], 'store_id' => $data['store_id'],
			'coupon_type' => $data['coupon_type'], 'price' => $data['price'],
			'deal' => $data['deal'], 'coupon_type_price' => $data['coupon_type_price']];
			if($imageName != null) {
				$updateData['image'] = $imageName;
			}
			$status = Coupon::where('id',$id)->update($updateData);
			if($status) {
				return Redirect::route('admin.coupon.index');
			}
		}
		$deals = \App\Deal::select("id", "name")->where("store_id", "=", $couponObj->store_id)->get();
		return view('admin.coupon.form',['couponObj' => $couponObj, 'stores' => $stores, 'deals' => $deals]);
	}
	
	public function deletecoupon($id) {
		$coupon = Coupon::find($id);
		$coupon->delete();
		return Redirect::route('admin.coupon.index');
	}
}
