<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Store;
use Redirect, Validator;
class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$stores = Store::select('id', 'name', 'email', 'phone_number', 'county', 'state',
		'city', 'code')->get();
		return view('admin.store.index',['stores' => $stores]);
    }
	
	public function create(Request $request) {
		$storeObj = null;
		$data = $request->all();
		if($request->isMethod('post')) {
			$phonePattern = '/\b\d{3}[-.]?\d{3}[-.]?\d{4}\b/';
			$validator = Validator::make($data, [
				'name' => 'required',
				'email' => 'required|string|email|max:255',
				'password' => 'required|string|min:6',
				'image' => 'mimes:jpeg,bmp,gif,png',
				'phone' => 'required', //'required|regex:' . $phonePattern,
				'county' => 'required',
				'state' => 'required',
				'city' => 'required',
				'code' => 'required',
				'address' => 'required',
			]);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$imageName = null;
			$slugName = str_slug($data['name']);
			if(count($request->file()) > 0) {
				$imageName = $slugName . time() . '.' .$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move(
					base_path() . '/public/images/store/', $imageName
				);
			}
			$insertData = ['name' => $data['name'], 'email' => $data['email'],
			'description' => $data['description'], 'password' => bcrypt($data['password']),
			'slug_name' => $slugName, 'image' => $imageName];
			$insertData['phone_number'] = $data['phone'];
			$insertData['county'] = $data['county'];
			$insertData['state'] = $data['state'];
			$insertData['city'] = $data['city'];
			$insertData['code'] = $data['code'];
			$insertData['address'] = $data['address'];
			
			$status = Store::create($insertData);
			if($status) {
				return Redirect::route('admin.store.index');
			}
		}
		return view('admin.store.form',['storeObj' => $storeObj]);
	}
	
	public function edit($id, Request $request) {
		$storeObj = null;
		$storeObj = Store::select('id', 'name', 'email', 'description', 'address',
								'phone_number', 'county', 'state', 'city', 'code')->where(['id' => $id])->first();
		if($storeObj == null) {
			return Redirect::route('admin.store.index');
		}
		$data = $request->all();
		if($request->isMethod('post')) {
			$phonePattern = '/\b\d{3}[-.]?\d{3}[-.]?\d{4}\b/';
			$validator = Validator::make($data, [
				'name' => 'required',
				'email' => 'required|email',
				'image' => 'mimes:jpeg,bmp,gif,png',
				'phone' => 'required', //'required|regex:' . $phonePattern,
				'county' => 'required',
				'state' => 'required',
				'city' => 'required',
				'code' => 'required',
				'address' => 'required',
			]);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$imageName = null;
			$slugName = str_slug($data['name']);
			if(count($request->file()) > 0) {
				$imageName = $slugName . time() . '.' .$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move(
					base_path() . '/public/images/store/', $imageName
				);
			}
			$updateData = ['name' => $data['name'], 'email' => $data['email'], 'description' => $data['description'],
			'slug_name' => $slugName];
			$updateData['phone_number'] = $data['phone'];
			$updateData['county'] = $data['county'];
			$updateData['state'] = $data['state'];
			$updateData['city'] = $data['city'];
			$updateData['code'] = $data['code'];
			$updateData['address'] = $data['address'];
			if($imageName != null) {
				$updateData['image'] = $imageName;
			}
			$status = Store::where('id',$id)->update($updateData);
			
			if($status) {
				return Redirect::route('admin.store.index');
			}
		}
		return view('admin.store.form',['storeObj' => $storeObj]);
	}
}
