<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
	
	/* public function login() {
		return view('admin.login');
	} */
	
	public function register() {
		return view('admin.register');
	}
	
	public function showRegistrationForm()
    {
        return view('admin.register');
    }
	
	public function showLoginForm() {
		return view('admin.login');
	}
	
	public function guard() {
		Auth::guard('admin');
	}
	
	public function logout(Request $request) {
        Auth::guard('admin')->logout();
        //$request->session()->invalidate();
        return redirect('admin/login');
    }
}
