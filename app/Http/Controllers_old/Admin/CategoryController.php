<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Redirect, Validator;
use DB;
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$categories = Category::select('id', 'name', 'slug_name', 'parent_id', 'feature_category', 'category_type')->get();
		return view('admin.category.index',['categories' => $categories]);
    }
	
	public function delete($categoryId) {
		$category = Category::find($categoryId);
		$category->delete();
		return Redirect::route('admin.category.index');
	}
	
	public function edit($id, Request $request) {
		$data = $request->all();
		if($request->isMethod('post')) {
			$obj = Category::find($id);
			/* $validator = Validator::make($data, [
				'name' => 'unique:categories,name,'.$obj->id.',id',
				'image' => 'mimes:jpeg,bmp,gif,png',
				'category_type' => 'required',
			]); */
			$name = $data['name'];
			$parID = isset($data['parent_id']) ? $data['parent_id'] : null;
			
			if(isset($data['category_type']) && $data['category_type'] == 1) {
				$validator = Validator::make($data, [
					//'name' => 'required|unique:categories,name,'.$data['name'],
					'name' => 'required',
					//'name'  => 'required|unique:categories,name,'.$parID.',parent_id',
					'category_type' => 'required',
					'image' => 'mimes:jpeg,bmp,gif,png',
					'parent_id' => 'required'
				]);
				$res = DB::table('categories')->where('name',$name)->where('parent_id',$parID)
				->where('category_type',1)
				->where('id', '!=' , $obj->id)
				->count();
				if($res > 0) {
					$validator->after(function($validator) {
						$validator->errors()->add('fendingDateield', 'Name with same parent category already exist!');
					});
				}
				
			} else {
				$validator = Validator::make($data, [
					'name' => 'required|unique:categories,name,'.$data['name'],
					'category_type' => 'required',
					'image' => 'mimes:jpeg,bmp,gif,png'
				]);
			}
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			
			//$slugName = str_replace(' ', '-', $data['name']);
			$slugName = str_slug($data['name']);
			/* if($parID != null) {
				$parentCategoryObj = \App\Category::find($parID);
				if($parentCategoryObj) {
					$slugName = $parentCategoryObj->slug_name.'-'.$slugName;
				}
			} */
			$description = $data['description'];
			
			$imageName = null;
			if(count($request->file()) > 0) {
				$imageName = $slugName . time() . '.' .$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move(
					base_path() . '/public/images/category/', $imageName
				);
			}
			
			/* $parent_id = null;
			if(isset($data['parent_id']) && $data['parent_id'] > 0) {
				$parent_id = $data['parent_id'];
			} */
			
			$category_type = null;
			if(isset($data['category_type'])) {
				$category_type = $data['category_type'];
			}
			$parent_id = null;
			if(isset($data['parent_id']) && $data['parent_id'] > 0) {
				if($category_type == 1) {
					$parent_id = $data['parent_id'];
				}
			}
			
			$updateData = ['name' => $data['name'], 'parent_id' => $parent_id, 'slug_name' => $slugName,
			'description' => $description, 'image' => $imageName, 'category_type' => $category_type];
			
			$status = Category::where('id',$obj->id)->update($updateData);
			
			if($status) {
				return Redirect::route('admin.category.index');
			}
		}
		/* $categorySlugObj = Category::select('id')->where(['slug_name' => $name])->first();
		$categoryObj = null;
		if($categorySlugObj != null) {
			$categoryObj = Category::select('id', 'name', 'description', 'image', 'slug_name', 'parent_id')->where(['id' => $categorySlugObj->id])->first();
		} */
		$categoryObj = Category::find($id);
		//echo "<pre>"; print_r($categoryObj->category_type); die;
		//$parentCategories = Category::select('id', 'name')->where(['parent_id' => null])->get();
		$parentCategories = Category::select('id', 'name')->whereNull('parent_id')->get();
		return view('admin.category.form',['categoryObj' => $categoryObj, 'parentCategories' => $parentCategories]);
	}
	
	public function create(Request $request) {
		$categoryObj = null;
		//$parentCategories = Category::select('id', 'name')->where(['parent_id' => null])->get();
		$data = $request->all();
		if($request->isMethod('post')) {
			
			$name = $data['name'];
			$parID = isset($data['parent_id']) ? $data['parent_id'] : null;
			
			if(isset($data['category_type']) && $data['category_type'] == 1) {
				$validator = Validator::make($data, [
					//'name' => 'required|unique:categories,name,'.$data['name'],
					'name' => 'required',
					//'name'  => 'required|unique:categories,name,'.$parID.',parent_id',
					'category_type' => 'required',
					'image' => 'mimes:jpeg,bmp,gif,png',
					'parent_id' => 'required'
				]);
				$res = DB::table('categories')->where('name',$name)->where('parent_id',$parID)->where('category_type',1)->count();
				if($res > 0) {
					$validator->after(function($validator) {
						$validator->errors()->add('fendingDateield', 'Name with same parent category already exist!');
					});
				}
				
			} else {
				$validator = Validator::make($data, [
					'name' => 'required|unique:categories,name,'.$data['name'],
					'category_type' => 'required',
					'image' => 'mimes:jpeg,bmp,gif,png'
				]);
			}
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$slugName = str_slug($name);
			$description = $data['description'];
			$imageName = null;
			if(count($request->file()) > 0) {
				$imageName = $slugName . time() . '.' .$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move(
					base_path() . '/public/images/category/', $imageName
				);
			}
			
			$category_type = null;
			if(isset($data['category_type'])) {
				$category_type = $data['category_type'];
			}
			$parent_id = null;
			if(isset($data['parent_id']) && $data['parent_id'] > 0) {
				if($category_type == 1) {
					$parent_id = $data['parent_id'];
				}
			}
			$insertData = ['name' => $name, 'parent_id' => $parent_id, 'slug_name' => $slugName,
			'description' => $description, 'image' => $imageName, 'category_type' => $category_type];
			$status = Category::create($insertData);
			if($status) {
				return Redirect::route('admin.category.index');
			}
		}
		$parentCategories = Category::select('id', 'name')->whereNull('parent_id')->get();
		return view('admin.category.form',['categoryObj' => $categoryObj, 'parentCategories' => $parentCategories]);
	}
	
	public function featurecategory(Request $request) {
		if($request->isMethod('get')) {
			$data = $request->all();
			$arr = [];
			foreach($data as $k => $val) {
				$arr[$k] = $val;
			}
			print_r($data);
			return response()->json(['status' => 'success', 'data' => $data]);
		}
	}
	
	public function updatefeaturecategory(Request $request) {
		if($request->isMethod('get')) {
			$data = $request->all();
			if(isset($data['fkid'])) {
				$fkid = $data['fkid'];
				$model = Category::find($fkid);
				if($model->feature_category == 0) {
					Category::where('id', $fkid)->update(['feature_category' => 1]);
					return response()->json(['status' => 'success', 'value' => 1, 'catid' => $fkid]);
				} else if($model->feature_category == 1) {
					Category::where('id', $fkid)->update(['feature_category' => 0]);
					return response()->json(['status' => 'success', 'value' => 0, 'catid' => $fkid]);
				}
				
			} else {
				return response()->json(['status' => 'error', 'data' => $data]);
			}
			
		}
	}
}
