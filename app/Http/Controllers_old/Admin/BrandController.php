<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use Redirect, Validator;
class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$brands = Brand::select('id', 'name')->get();
		return view('admin.brand.index',['brands' => $brands]);
    }
	
	public function create(Request $request) {
		$brandObj = null;
		$data = $request->all();
		if($request->isMethod('post')) {
			$validator = Validator::make($data, [
				'name' => 'required',
			]);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$insertData = ['name' => $data['name']];
			$status = Brand::create($insertData);
			if($status) {
				return Redirect::route('admin.brand.index');
			}
		}
		return view('admin.brand.form',['brandObj' => $brandObj]);
	}
	
	public function edit($id, Request $request) {
		$brandObj = Brand::select('id', 'name')->where(['id' => $id])->first();
		if($brandObj == null) {
			return Redirect::route('admin.brand.index');
		}
		$data = $request->all();
		if($request->isMethod('post')) {
			$validator = Validator::make($data, [
				'name' => 'required',
			]);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}

			$updateData = ['name' => $data['name']];
			$status = Brand::where('id',$id)->update($updateData);
			
			if($status) {
				return Redirect::route('admin.brand.index');
			}
		}
		return view('admin.brand.form',['brandObj' => $brandObj]);
	}
}
