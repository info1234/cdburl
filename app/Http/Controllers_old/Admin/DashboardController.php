<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Store;
use Redirect, Validator;
use App\Deal;
use Session;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }
	
	public function index(Request $request) {
		if($request->ajax() && $request->isMethod('get')) {
			$data = $request->all();
			if(isset($data['dashboard']) && $data['dashboard'] == "populerdeal") {
				$populerDeals = \App\Deal::select(['deals.id', 'deals.name', \DB::raw('count(deal_views.deal_id) as total')])
				->join('deal_views', 'deals.id', '=', 'deal_views.deal_id')
				->groupBy('deals.id')
				->orderBy('total', 'DESC')
				->get();
				return view('admin.dashboard.populerdeal', ['populerDeals' => $populerDeals]);
			}
		}
		
		$dealObj = Deal::select("id", "name", "category_id", "store_id", "price", "total", "created_at");
		if($request->ajax() && $request->isMethod('get')) {
			$data = $request->all();
			if(isset($data['dashboard']) && $data['dashboard'] == "deal") {
				$deals = $dealObj->orderBy("created_at", "desc")->get();
				return view('admin.dashboard.deal', ['deals' => $deals]);
			}
		}
		
		$deals = $dealObj->orderBy("created_at", "desc")->paginate(10);
		$commentCounts = [];
		$storeCounts = [];
		$totalStoreDatas = Deal::select("store_id")->groupBy('store_id')->get();
		//Deal::select(\DB::raw('count(store_id) as store_id'))->groupBy('store_id')->get()->count();
		$totalStores = count($totalStoreDatas);
		$totalCategories = \App\Category::select("id")->count();
		$totalDeals = Deal::select("id")->count();
		$totalComments = \App\Comment::select("id")->count();
		

		/* $populerDeals = \App\DealView::select("deals.id", "deals.name")
		->join("deals", "deals.id", "=", "deal_views.deal_id")->groupBy("deal_views.deal_id")->get(); */
		
		$populerObj = \App\Deal::select(['deals.id', 'deals.name', \DB::raw('count(deal_views.deal_id) as total')])
		->join('deal_views', 'deals.id', '=', 'deal_views.deal_id')
		->groupBy('deals.id');
		$populerDealCounts = $populerObj->get()->count(); //count
		$populerDeals = $populerObj->orderBy('total', 'DESC')->paginate(10); //data
		
		$dealviews  = [];
		//echo "<pre>"; print_r($populerDeals); die;
        return view('admin.dashboard.index', ["totalDeals" => $totalDeals, "totalCategories" => $totalCategories,
		"totalStores" => $totalStores, 'totalComments' => $totalComments, "deals" => $deals, "populerDeals" => $populerDeals,
		"populerDealCounts" => $populerDealCounts]);
    }
	
	public function changelogo(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'image' => 'mimes:jpeg,bmp,gif,png',
				//'image' => ''
			]);
			/* if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			} */
			if($request->file('image')) {
				if($validator->fails()) {
					return redirect()->back()->withErrors($validator->errors());
				}
				$count = \App\Setting::where("setting_key", 'header_logo')->count();
				if($count == 0) {
					$file = $request->file('image');
					$imageName = time().'headerlogo'.'.'. $file->getClientOriginalExtension();
					$status = \App\Setting::create(['name' => $imageName, "setting_key" => 'header_logo']);
					if($status) {
						$file->move(base_path() . '/public/images/logo/header/', $imageName);
					}
				} else {
					$file = $request->file('image');
					$imageName = time().'headerlogo'.'.'. $file->getClientOriginalExtension();
					$status = \App\Setting::where("setting_key", 'header_logo')->update(['name' => $imageName]);
					if($status) {
						$file->move(base_path() . '/public/images/logo/header/', $imageName);
					}
				}
			}
			
			if(isset($data['frontpagedeal']) && $data['frontpagedeal'] > 0) {
				$frontpagedeal = $data['frontpagedeal'];
			}
			
			
			
		}
		$data = $this->settingData();
		return view('admin.dashboard.setting', ['data' => $data['data']]);
	}
	
	private function settingData() {
		$data = \App\Logo::where("type", 1)->first();
		return array('data' => $data);
	
	}
	public function setting(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'pageoption' => 'required',
				'title' => 'required',
				'url' => 'required|url', //url
				'image' => 'required|mimes:jpeg,bmp,gif,png',				
			], ['pageoption.required' => 'The page field is required.']);
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			//echo "<pre>"; print_r($data); die;
			if(isset($data['pageoption'])) {
				$desc = null;
				if($data['pageoption'] == 1) {
					$count = \App\Setting::where("setting_key", 'home_page_add')->count();
					$imageName = null;
					if($count == 0) {
						if($request->file('image')) {
							if($validator->fails()) {
								return redirect()->back()->withErrors($validator->errors());
							}
							$file = $request->file('image');
							$imageName = time().'advertisement'.'.'. $file->getClientOriginalExtension();
							$file->move(base_path() . '/public/images/advertisement/', $imageName);
						}
						$status = \App\Setting::create(['value' => $data['title'], "setting_key" => 'home_page_add', "value" => $desc, "image" => $imageName, 'url' => $data['url']]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					} else {
						$imageName = null;
						if($request->file('image')) {
							if($validator->fails()) {
								return redirect()->back()->withErrors($validator->errors());
							}
							$file = $request->file('image');
							$imageName = time().'advertisement'.'.'. $file->getClientOriginalExtension();
							$file->move(base_path() . '/public/images/advertisement/', $imageName);
						}
						$status = \App\Setting::where("setting_key", 'home_page_add')->update(['value' => $data['title'], "value" => $desc, "image" => $imageName, 'url' => $data['url']]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					}
				} else if($data['pageoption'] == 2) {
					$category = null;
					if(isset($data['category'])) {
						$category = $data['category'];
					}
					
					$count = \App\Setting::where("setting_key", 'deal_page_advertisement')->where("category_id", $category)->count();
					if($count == 0) {
						$imageName = null;
						if($request->file('image')) {
							if($validator->fails()) {
								return redirect()->back()->withErrors($validator->errors());
							}
							$file = $request->file('image');
							$imageName = time().'advertisement'.'.'. $file->getClientOriginalExtension();
							$file->move(base_path() . '/public/images/advertisement/', $imageName);
						}
						$status = \App\Setting::create(['value' => $data['title'], "setting_key" => 'deal_page_advertisement', "value" => $desc, 'category_id' => $category, 'image' => $imageName, 'url' => $data['url']]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					} else {
						$imageName = null;
						if($request->file('image')) {
							if($validator->fails()) {
								return redirect()->back()->withErrors($validator->errors());
							}
							$file = $request->file('image');
							$imageName = time().'advertisement'.'.'. $file->getClientOriginalExtension();
							$file->move(base_path() . '/public/images/advertisement/', $imageName);
						}
						$status = \App\Setting::where("setting_key", 'deal_page_advertisement')
						->where("category_id", $category)
						->update(['value' => $data['title'], "image" => $imageName, "value" => $desc, 'category_id' => $category, 'url' => $data['url']]);
						redirect()->back()->with('success_deal_page_advertisement', 'Successfully create advertisement.');
					}
				}
			}
		}
		$data = $this->settingData();
		$headerlogoimage = \App\Setting::where("setting_key", 'header_logo')->first();
		$frontPerPage = \App\Setting::where("setting_key", 'frontdeal_per_page')->first();
		$couponPerPage = \App\Setting::where("setting_key", 'coupon_per_page')->first();
		return view('admin.dashboard.setting', ['headerlogoimage' => $headerlogoimage, 'frontPerPage' => $frontPerPage,
		'couponPerPage' => $couponPerPage]);
	}
   
	public function links(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'facebook' => 'url',
				'youtube' => 'url',
				'twitter' => 'url',
				'linkdin' => 'url',
				'google' => 'url',
			]);
			//echo "<pre>"; print_r($data); die;
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			$flag = false;
			if(isset($data['facebook'])) {
				$count = \App\Setting::where("setting_key", 'facebook_link')->count();
				if($count == 0) {
					$flag = true;
					$status = \App\Setting::create(["setting_key" => 'facebook_link', "value" => $data['facebook']]);
				} else {
					$flag = true;
					$status = \App\Setting::where("setting_key", 'facebook_link')->update(["value" => $data['facebook']]);
				}
			}
			if(isset($data['youtube'])) {
				$count = \App\Setting::where("setting_key", 'youtube_link')->count();
				if($count == 0) {
					$flag = true;
					$status = \App\Setting::create(["setting_key" => 'youtube_link', "value" => $data['youtube']]);
				} else {
					$flag = true;
					$status = \App\Setting::where("setting_key", 'youtube_link')->update(["value" => $data['youtube']]);
				}
			}
			if(isset($data['twitter'])) {
				$count = \App\Setting::where("setting_key", 'twitter_link')->count();
				if($count == 0) {
					$flag = true;
					$status = \App\Setting::create(["setting_key" => 'twitter_link', "value" => $data['twitter']]);
				} else {
					$flag = true;
					$status = \App\Setting::where("setting_key", 'twitter_link')->update(["value" => $data['twitter']]);
				}
			}
			if(isset($data['linkdin'])) {
				$count = \App\Setting::where("setting_key", 'linkdin_link')->count();
				if($count == 0) {
					$flag = true;
					$status = \App\Setting::create(["setting_key" => 'linkdin_link', "value" => $data['linkdin']]);
				} else {
					$flag = true;
					$status = \App\Setting::where("setting_key", 'linkdin_link')->update(["value" => $data['linkdin']]);
				}
			}
			if(isset($data['google'])) {
				$count = \App\Setting::where("setting_key", 'google_link')->count();
				if($count == 0) {
					$flag = true;
					$status = \App\Setting::create(["setting_key" => 'google_link', "value" => $data['google']]);
				} else {
					$flag = true;
					$status = \App\Setting::where("setting_key", 'google_link')->update(["value" => $data['google']]);
				}
			}
			if($flag == true) {
				redirect()->back()->with('success_social_link', 'Successfully create link.');
				/* return view('admin.dashboard.sociallink', ['facebook' => $facebook, 'youtube' => $youtube, 'twitter' => $twitter, 'linkdin' => $linkdin])->with('success_social_link', 'Successfully create link.') */;
				//redirect()->back()->with('success_social_link', 'Successfully create link.');
			}
		}
		return view('admin.dashboard.sociallink', []);
	}
}
