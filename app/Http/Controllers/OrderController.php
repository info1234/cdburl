<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Input;
use Redirect, Validator;
use Auth;
use App\User;
use DB;
class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $params = null) {
		if(Auth::user()->type == 1) {
			if($params == 'ordered' || $params == 'paid' || $params == 'shipped' || $params == 'refund' || $params == 'shelve') {
				return view('order/index', []);
			} else {
				return view('order/index', []);
			}
		} else {
			return redirect('admin/dashboard');
		}
	}
}

