<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Auth;
use DB;
use Cookie;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request) {
		if(Auth::user()->type == 1) {
			//$count = DB::table('products')->count();
			return view('dashboard/index', []);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	public function home(Request $request) {
		return view('home/index', []);
	}

	public function register() {
		return redirect()->action('DashboardController@home');
	}

	public function login() {
		return redirect()->action('DashboardController@home');
	}
}

