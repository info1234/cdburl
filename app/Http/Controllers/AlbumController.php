<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Input;
use Redirect, Validator;
use Auth;
use DB;
class AlbumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage() {
		if(Auth::user()->type == 1) {
			return view('album.manage');
		} else {
			return redirect('admin/dashboard');
		}
    }
	
	public function upload() {
		if(Auth::user()->type == 1) {
			return view('album.upload');
		} else {
			return redirect('admin/dashboard');
		}
    }
}

