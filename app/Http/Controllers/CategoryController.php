<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Auth;
use DB;
use Cookie;
use Validator;
use App\Product;
use App\Category;
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function inventory(Request $request) {
		if(Auth::user()->type == 1) {
			$categories = Category::get();
			return view('category/inventory', ['categories' => $categories]);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	public function image() {
		return view('category/image', []);
	}
	
	public function create(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			$name = $data['category'];
			if(empty($name)) {
				return response()->json(['status'=>'error', 'message' => 'Please enter category name.']);
			} else {
				$categoryObj = Category::select("id", "name")->where('name',$name)->get();;
				if($categoryObj->count() > 0) {
					return response()->json(['status'=>'error', 'message' => 'category already exist.']);
				} else {
					Category::create(array('name' => $name));
					return response()->json(['status'=>'success']);
				}
			}
			
		}
	}
	
	public function update(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			$name = $data['value'];
			if(empty($name)) {
				return response()->json(['status'=>'error', 'message' => 'Please enter category name.']);
			} else {
				$res = DB::table('categories')->where('name',$name)
				->where('id', '!=' , $data['categoryid'])
				->count();
				if($res > 0) {
					return response()->json(['status'=>'error', 'message' => 'category already exist.']);
				} else {
					Category::where('id',$data['categoryid'])->update(array('name' => $name));
					return response()->json(['status'=>'success', 'categoryid' => $data['categoryid'], 'name' => $name]);
				}
			}
			
		}
	}
	
	public function delete($pk, Request $request) {
		if($request->ajax()) {
			$status = DB::table('categories')->where('id', $pk)->delete();
			if($status) {
				Product::where('category',$pk)->update(array('category' => 1));
				return response()->json(['status'=>'success', 'categoryid' => $pk]);
			} else {
				return response()->json(['status'=>'error', 'message' => 'something wrong.']);
			}
		}
	}
}

