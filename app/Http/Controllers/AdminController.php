<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Auth;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		//echo "<pre>"; print_r(Auth::user()); die;
        //return view('admin.home');
		
		return view('admin.admin');
    }
	
	public function dashboard() {
		
	}
}
