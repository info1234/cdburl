<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Auth;
use DB;
use Cookie;
class EbayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function message(Request $request, $params = null) {
		if(Auth::user()->type == 1) {
			return view('ebay/message', []);
		} else {
			return redirect('admin/dashboard');
		}
	}
}

