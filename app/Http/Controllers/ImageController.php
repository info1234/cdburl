<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;
use DB;
use Cookie;
//use Input;
use Illuminate\Support\Facades\Input as input;

use Validator;
use App\Product;
use App\Images;
use App\Variation;
use App\SkuAliases;
use App\ProductIdentifier;
class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $params = null) {
		if(Auth::user()->type == 1) {
			$images = Images::select('*')
			->where('image_upload_type',1)
			->leftJoin("products",function($join){

				$join->on("images.item_id","=","products.id")->where('products.user_id', Auth::user()->id);

			})
			->whereNull('parent_id')
			->get();
			$img = array();
			foreach($images as $imageObj) {
				$img[] = array('image' => $imageObj['image'],'item_id' => $imageObj['item_id'],'main_image' => $imageObj['main_image']
				,'type' => $imageObj['type'],'image_upload_type' => $imageObj['image_upload_type']);
			}
			$imgs = array_chunk($img, 4);
			//echo '<pre>'; print_r($imgs); die;
			return view('images/index', ['images' => $imgs]);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
}

