<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Auth;
use DB;
use Cookie;
class ShopifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function product(Request $request, $params = null, $params2 = null) {
		if(Auth::user()->type == 1) {
			return view('shopify/product', []);
		} else {
			return redirect('admin/dashboard');
		}
	}
}

