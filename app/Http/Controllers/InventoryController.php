<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;
use DB;
use Cookie;
//use Input;
use Illuminate\Support\Facades\Input as input;

use Validator;
use App\Product;
use App\Category;
use App\Images;
use App\Variation;
use App\SkuAliases;
use App\ProductIdentifier;
use App\Template;
class InventoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function scraper(Request $request, $params = null) {
		if(Auth::user()->type == 1) {
			return view('inventory/scraper', []);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	public function product(Request $request) {
		if(Auth::user()->type == 1) {
			$products = Product::select('products.id', 'products.name', 'products.sku', 'products.price', 'products.quantity', 'products.unit_cost',
			'products.created_at', 'products.updated_at', 'products.category', 'images.image', 'images.image_upload_type')

				->leftJoin("images",function($join){

					$join->on("images.item_id","=","products.id")->where('images.main_image', 1);

				})

				->where('products.user_id', Auth::user()->id)
				->paginate(50);
				//echo '<pre>'; print_r($products); die;
			return view('inventory/product', ['products' => $products]);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	public function variation(Request $request) {
		if(Auth::user()->type == 1) {
			return view('inventory/variation', []);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	public function add(Request $request) {
		if(Auth::user()->type == 1) {
			if($request->isMethod('post')) {
				$data = $request->all();
				$validator = Validator::make($data, [
					'sku' => 'required',
					'product_name' => 'required',
					'price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
				]);
				if(isset($data['MSRP']) && !is_numeric($data['MSRP'])) {
					$validator->after(function($validator) {
						$validator->errors()->add('MSRP', 'The MSRP format is invalid.');
					});
				}
				if(isset($data['quantity']) && !is_numeric($data['quantity'])) {
					$validator->after(function($validator) {
						$validator->errors()->add('quantity', 'The quantity format is invalid.');
					});
				}
				if(isset($data['unit_cost']) && !is_numeric($data['unit_cost'])) {
					$validator->after(function($validator) {
						$validator->errors()->add('unit_cost', 'The unit cost format is invalid.');
					});
				}
				
				if($validator->fails()) {
					return redirect()->back()->withErrors($validator->errors());
				}
				$images = $request->file('images');
				$status = $this->handleData($data, $request);
				
				if($status['flag']) {
					$request->session()->flash('alert-success', 'Product successfully added!');
					return redirect('inventory/edit/'.$status['id']);
				}
				
			}
			$productIdentifierData = array();
			$skuAliases = array();
			return view('inventory/form', ['obj' => null, 'productIdentifierData' => $productIdentifierData, 'skuAliases' => $skuAliases]);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	public function edit($id, Request $request) {
		if(Auth::user()->type == 1) {
			if($request->isMethod('post')) {
				$data = $request->all();
				$validator = Validator::make($data, [
					'sku' => 'required',
					'product_name' => 'required',
					'price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
				]);
				if(isset($data['MSRP']) && !is_numeric($data['MSRP'])) {
					$validator->after(function($validator) {
						$validator->errors()->add('MSRP', 'The MSRP format is invalid.');
					});
				}
				if(isset($data['quantity']) && !is_numeric($data['quantity'])) {
					$validator->after(function($validator) {
						$validator->errors()->add('quantity', 'The quantity format is invalid.');
					});
				}
				if(isset($data['unit_cost']) && !is_numeric($data['unit_cost'])) {
					$validator->after(function($validator) {
						$validator->errors()->add('unit_cost', 'The unit cost format is invalid.');
					});
				}
				
				if($validator->fails()) {
					return redirect()->back()->withErrors($validator->errors());
				}
				$images = $request->file('images');
				$status = $this->handleData($data, $request, $id);
				
				if($status['flag']) {
					$request->session()->flash('alert-success', 'Product successfully update!');
					return redirect('inventory/edit/'.$status['id']);
				}
				
			}
			$obj = Product::find($id);
			if($obj == null) {
				return redirect('inventory/add');
			}
			$productIdentifierData = ProductIdentifier::where('product_id',$id)->get();
			$skuAliases = SkuAliases::where('product_id',$id)->get();
			return view('inventory/form', ['obj' => $obj, 'productIdentifierData' => $productIdentifierData, 'skuAliases' => $skuAliases]);
		} else {
			return redirect('admin/dashboard');
		}
	}
	
	protected function handleData($data, $request, $productId = null) {
		$insert = array(
			'name' => $data['product_name'],
			'category' => $data['category'],
			'sku' => $data['sku'],
			'brand' => $data['brand'],
			'short_description' => $data['short_description'],
			'description' => $data['description'],
			'price' => $data['price'],
			'msrp' => $data['MSRP'],
			'quantity' => $data['quantity'],
			'unit_cost' => $data['unit_cost'],
			'MPN' => $data['MPN'],
			'condition' => $data['condition'],
			'dimensions_length_value' => $data['dimensions_length'],
			'dimensions_width_value' => $data['dimensions_width'],
			'dimensions_height_value' => $data['dimensions_height'],
			'dimensions_option' => $data['dimensions_option'],
			'weight' => $data['weight'],
			'weight_option' => $data['weight_option'],
			'customs_description' => $data['customs_description'],
			'declared_value' => $data['declared_value'],
			'declared_option' => $data['declared_option_value'],
			'country' => $data['country'],
			'tariff_number' => $data['tariffNumber'],
			'user_id' => Auth::user()->id
		);
		
		$flag = false;
		if($productId != null) {
			Product::where('id',$productId)->update($insert);
		} else {
			$productId = Product::create($insert)->id;
		}
		
		if($productId) {
			$mainImage = null;
			if(count($request->file()) > 0) {
				$images = Images::select('id', 'main_image')->where('item_id',$productId)->get();
				$isMainImage = false;
				foreach($images as $imagesObj) {
					if($imagesObj['main_image'] == PRODUCT_MAIN_IMAGE) {
						$isMainImage = true;
					}
				}
				foreach($request->file('images') as $fileKey => $file) {
					$name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
					$name = str_slug($name, '_');
					$name = $name.time().'.'.$file->getClientOriginalExtension();
					if($file->move(public_path('images/product'), $name)) {
						$insertImage = array('image' => $name, 'item_id' => $productId, 'type' => PRODUCT_TYPE_IMAGES, 'image_upload_type' => 2);
						if($fileKey == 0 && $isMainImage == false) {
							$insertImage['main_image'] = PRODUCT_MAIN_IMAGE;
						}
						Images::create($insertImage);
					}
					
				}
			}
		
			if(!empty($data['product_Identifier'])) {
				if($productId != null) {
					DB::table('product_identifier')->where('product_id', $productId)->delete();
				}
				foreach($data['product_Identifier'] as $key => $productIdentifierObj) {
					if($productIdentifierObj['value']) {
						$insertProductIdentifier = array(
														'product_id' => $productId,
														'product_Identifier' => $productIdentifierObj['option'],
														'product_Identifier_value' => $productIdentifierObj['value']);
						ProductIdentifier::create($insertProductIdentifier);
					}
				}
			}
			
			if(!empty($data['SKU_aliases'])) {
				if($productId != null) {
					DB::table('sku_aliases')->where('product_id', $productId)->delete();
				}
				foreach($data['SKU_aliases'] as $key => $SKUaliasesObj) {
					if(!empty($SKUaliasesObj['value'])) {
						$insertProductIdentifier = array(
														'product_id' => $productId,
														'SKU_aliases_option' => $SKUaliasesObj['option'],
														'SKU_aliases_value' => $SKUaliasesObj['value']);
						SkuAliases::create($insertProductIdentifier);
					}
				}
			}
			
			if(isset($data['variation']) && !empty($data['variation'])) {
				foreach($data['variation'] as $variationid=>$obj) {
					if($obj['name'] != '' && $obj['value'] != '') {
						if($variationid > 0) {
							Variation::where('id',$variationid)->update(array('name' => $obj['name'], 'value' => $obj['value']));
						} else {
							Variation::create(array('product_id' => $productId, 'name' => $obj['name'], 'value' => $obj['value']));
						}
					}
				}
			}
			
			$flag = true;
		}
		return array('flag' => $flag, 'id' => $productId);
	}
	
	public function export(Request $request) {
		header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=product.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $products = Product::where('user_id', Auth::user()->type)->get();
        $columns = array('*Name', 'Description', '*Parent SKU', '*Price($)',
		'Quantity','Cost($)','Category','Brand');

        $file = fopen('php://output', 'w');
		foreach($products as $obj) {
			$images = Images::where('item_id',$obj->id)->get(); //1 product image
			foreach($images as $key => $imageObj) {
				$key++;
				$columns[] = 'image_'.$key;
			}
		}
        fputcsv($file, $columns);
		$flag = false;
        foreach($products as $obj) {
			$flag = true;
			$productData = array($obj['name'],$obj['description'],$obj['sku'],$obj['price'],$obj['quantity'],
			$obj['unit_cost'],'',$obj['brand']);
			$images = Images::where('item_id',$obj->id)->get(); //1 product image
			foreach($images as $key => $imageObj) {
				$key++;
				$productData['image_'.$key] = $imageObj['image'];
			}
            fputcsv($file, $productData);
        }
        exit();
	}
	
	public function search(Request $request) {
		if($request->isMethod('post')) {
			$products = Product::select('products.id', 'products.name', 'products.sku', 'products.price', 'products.quantity', 'products.unit_cost',
			'products.created_at', 'products.updated_at', 'products.category', 'images.image')
				->leftJoin("images",function($join){
					$join->on("images.item_id","=","products.id")->where('images.main_image', 1);
				})

				->get();
			$data = array();
			foreach($products as $productObj) {
				$nestedData=array();
				$nestedData[] = $productObj["name"];
			 
				$data[] = $nestedData;
			}
			return response()->json(['draw' => 1, 'recordsTotal'=>1, 'recordsFiltered'=>1,'data' => $data]);
			//return view('inventory/product', ['products' => $products]);
		}
	}
	
	public function import(Request $request) {
		if($request->isMethod('post')) {
			if(count($request->file()) > 0) {
				$file = Input::file('import');
				$name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
				$name = str_slug($name, '_');
				$name = $name.time().'.'.$file->getClientOriginalExtension();
				if($file->move(storage_path('import'), $name)) {
					if(($handle = fopen (storage_path('import/'.$name), 'r' )) !== FALSE) {
						$i=0;
						while (($data = fgetcsv ($handle, 1000, ',' )) !== FALSE ) {
							if($i != 0) {
								$inserted_data=array(
									'user_id' => Auth::user()->id,
									'name'=>$data[0],
									'short_description'=>$data[1],
									'sku'=>$data[2],
									'price'=>$data[3],
									'quantity'=>$data[4],
									'unit_cost'=>$data[5],
									'category'=>$data[6],
									'brand'=>$data[7]
								);
								Product::create($inserted_data);
							}
							$i++;
						}
						fclose ( $handle );
					}
					if(file_exists(storage_path('import/'.$name))) {  
						unlink(storage_path('import/'.$name));
					}
				}
			}
		}
		return redirect('inventory/product');
	}
	
	public function sample() {
		$path = storage_path('import/productimport.csv');
		return response()->download($path);
	}
	
	public function removevariantbyid($pk, Request $request) {
		if($request->ajax()) {
			$status = DB::table('variation')->where('id', $pk)->delete();
			if($status) {
				return response()->json(['status'=>'success','variantid' => $pk]);
			} else {
				return response()->json(['status'=>'error','variantid' => $pk]);
			}			
		}
	}
	
	/* public function duplicate() {
		if($request->ajax()) {
			$product = Task::find(1);
			$new = $task->replicate();
		}
	} */
	
	public function getcontentdata(Request $request) {
		if($request->ajax()) {
			$data = $request->all();
			
			/* $array = explode(PHP_EOL, $data['value']);
			$value = explode(' ', $array[0]); */
			
			$value = explode(' ', $data['value']);
			$status = array();
			$link = $data['link'];
			$allValue = array();
			
			foreach($value as $valueObj) {
				$key = trim($valueObj);
				$allValue[$key] = $key;
			}
			//echo '<pre>'; print_r($allValue); die;
			foreach($allValue as $allValueObj) {
				$url = $link.$allValueObj;
				$content = amazon('amazon', $url);
				$isExixt = false;
				if(isset($content['asin']) && $content['asin'] != '') {
					$check = ProductIdentifier::select('id')
					->join("products",function($join){
						$join->on("product_identifier.product_id","=","products.id")->where('products.user_id', Auth::user()->id);
					})					
					->where('product_Identifier', 'ASIN')
					->where('product_Identifier_value', $content['asin'])
					->count();
					if($check > 0) {
						$isExixt = true;
						$status[] = array('status' => 'error', 'message' => 'Product already use.', 'value' => $allValueObj, 'asin' => $content['asin'],
						'link' => $link);
					}
				}
				if($isExixt == false) {
					$status[] = array('status' => 'success', $content, 'value' => $allValueObj, 'url' => $url, 'link' => $link);
				}
			}
			//echo '<pre>'; print_r($status);
			return response()->json($status);
		}
	}
	
	public function create(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			//echo '<pre>'; print_r($data); die;
			$flag = false;
			$lastCategory = 1;
			foreach($data['product'] as $productid => $dataObj) {
				$parentKey = '';
				if(isset($dataObj['category'])) {
					$keys = array();
					foreach($dataObj['category'] as $key => $categoryObj) {
						$catrgory = trim($categoryObj);
						$obj = Category::where('name',$catrgory)->count();
						if($obj == 0) {
							if($key == 0) {
								$id = Category::create(array('name' => $catrgory))->id;
								$parentKey = $id;
								$lastCategory = $id;
							} else {
								$insert = array('name' => $catrgory);
								if($parentKey > 0) {
									$insert = array('name' => $catrgory, 'parent_id' => $parentKey);
								}
								$id = Category::create($insert)->id;
								$lastCategory = $id;
								if($id) {
									$parentKey = $id;
								}
							}
						} else {
							$categoryObj = Category::select('id', 'name')
								->where('name', $catrgory)->first();
							if($categoryObj) {
								$parentKey = $categoryObj->id;
								$lastCategory = $categoryObj->id;
							}
						}
					}
				}
				$inserted_data = array(
					'source_market' => $data['source_market'],
					'url' => $dataObj['url'], //
					'name' => $dataObj['title'],
					'description' => $dataObj['desc'],
					'price' => $dataObj['price'],
					'user_id' => Auth::user()->id,
					'quantity' => 1,
					'category' => $lastCategory
				);
				$obj = Product::create($inserted_data);
				if($obj) {
					$flag = true;
					if(isset($obj->id)) {
						if(isset($dataObj['asin']) && !empty($dataObj['asin'])) {
							$insertProductIdentifier = array(
															'product_id' => $obj->id,
															'product_Identifier' => 'ASIN',
															'product_Identifier_value' => $dataObj['asin']);
							ProductIdentifier::create($insertProductIdentifier);
						}
						if(isset($dataObj['imagethumb']) && count($dataObj['imagethumb']) > 0) {
							foreach($dataObj['imagethumb'] as $key => $imagethumbobj) {
								$count = Images::where(['item_id' => $obj->id, 'main_image' => 1])->whereNull('parent_id')->get()->count();
								if($count == 0) {
									$insertImageData = ['image' => $imagethumbobj, 'item_id' => $obj->id,
									'main_image' => 1, 'image_type' => 1, 'type' => 1, 'image_upload_type' => 1];
									$imageObj = Images::create($insertImageData);
									if($imageObj) {
										if(isset($dataObj['thumb']) && count($dataObj['thumb']) > 0 && isset($dataObj['thumb'][$key])) {
											Images::create(['image' => $dataObj['thumb'][$key], 'item_id' => $obj->id,
											'parent_id' => $imageObj->id, 'type' => 1, 'image_type' => 2, 'image_upload_type' => 1]);
										}
									}
								} else {
									$insertImageData = ['image' => $imagethumbobj, 'item_id' => $obj->id, 'image_type' => 1, 'type' => 1, 'image_upload_type' => 1];
									$imageObj = Images::create($insertImageData);
									if($imageObj) {
										if(isset($dataObj['thumb']) && count($dataObj['thumb']) > 0 && isset($dataObj['thumb'][$key])) {
											Images::create(['image' => $dataObj['thumb'][$key], 'item_id' => $obj->id,
											'parent_id' => $imageObj->id, 'type' => 1, 'image_type' => 2, 'image_upload_type' => 1]);
										}
									}
								}
								
							}
						}
					}
				}
			}
			
			if($flag == true) {
				$request->session()->flash('alert-success', 'Product successfully added');
				return redirect('inventory/scraper');
			}
			return redirect('inventory/scraper');
		}
	}
	
	public function detail($id, Request $request) {
		$data = Template::where('user_id', Auth::user()->id)->first();
		$product = Product::select('products.id', 'products.name', 'products.price', 'products.quantity', 'products.category', 
		'products.description', 'images.image', 'categories.name as catname')
		->leftJoin("images",function($join) {

			$join->on("images.item_id","=","products.id")->where('images.main_image', 1);

		})
		->leftJoin("categories",function($join) {

			$join->on("categories.id","=","products.category");

		})
		->where('products.id',$id)
		->first();
		//echo '<pre>'; print_r($product->category); die;
		if($data == null) {
			return view('templates/template1', ['product' => $product]);
		} else if($data->template_id == 2) {
			return view('templates/template2', ['product' => $product]);
		} else if($data->template_id == 1) {
			return view('templates/template1', ['product' => $product]);
		} else {
			return view('templates/template1', ['product' => $product]);
		}
	}
}

