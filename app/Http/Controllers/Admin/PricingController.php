<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect, Validator;
use App\Pricing;
use Session;
use Auth;
class PricingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
	
	public function pricing() {
		if(Auth::user()->type == 2) {
			$pricing = Pricing::get();
			return view('admin.pricing.pricing', ['pricing' => $pricing]);
		} else {
			return redirect('dashboard');
		}
	}
	
	public function edit($id, Request $request) {
		if(Auth::user()->type == 2) {
			if($request->isMethod('post')) {
				$data = $request->all();
				$support = array();
				
				if(isset($data['price'])) {
					if(!is_numeric($data['price'])) {
						$request->session()->flash('alert-danger', 'Please enter valid price.');
						return redirect('admin/pricing/edit/'.$id);
					}
				}
				
				if(isset($data['support'])) {
					$support = $data['support'];
				}
				$params = array(
					'price' => $data['price'],
					'price_per' => $data['price_per'],
					'integrated_channel' => $data['integrated_channel'],
					'multiple_account' => $data['multiple_account'],
					'staff_accounts' => $data['staff_accounts'],
					'managed_active_listing' => $data['managed_active_listing'],
					'orders_per_month' => $data['orders_per_month'],
					'image_hosting' => $data['image_hosting'],
					'powerful_pricing_rules' => $data['powerful_pricing_rules'],
					'daily_inventory_monitor' => $data['daily_inventory_monitor'],
					'daily_price_monitor' => $data['daily_price_monitor'],
					'automatic_order_fulfillment' => $data['automatic_order_fulfillment'],
					'order_tracking_number_sync' => $data['order_tracking_number_sync'],
					'dropshipping_supplier_api' => $data['dropshipping_supplier_api'],
					'ebay_templates_and_presets' => $data['ebay_templates_and_presets'],
					'ebay_auto_restock' => $data['ebay_auto_restock'],
					'ebay_message_assistant' => $data['ebay_message_assistant'],
					'usps_shipping_labels' => $data['usps_shipping_labels'],
					'automatic_tracking_info_updates' => $data['automatic_tracking_info_updates'],
					'support' => json_encode($support),
				);
				if(Pricing::where('id',$id)->update($params)) {
					$request->session()->flash('alert-success', 'successfully update.');
					return redirect('admin/pricing/edit/'.$id);
				}
			}
			
			$pricingObj = Pricing::find($id);
			if(!$pricingObj) {
				return redirect('admin/pricing');
			}			
			return view('admin.pricing.form', ['pricingObj' => $pricingObj]);
		} else {
			return redirect('dashboard');
		}
	}
}
