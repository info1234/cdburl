<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect, Validator;
use Session;
use App\User;
use Auth;
use App\Product;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
	
	public function userlisting() {
		if(Auth::user()->type == 2) {
			$users = User::select('id', 'name', 'email', 'mobile')->where('type',1)->get();
			return view('admin.users.userlisting', ['users' => $users]);
		} else {
			return redirect('dashboard');
		}
	}
}
