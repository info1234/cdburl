<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect, Validator;
use Session;
use App\User;
use Auth;
use App\Product;
class InventoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
	
	public function inventorylisting($userid, Request $request) {
		if(Auth::user()->type == 2) {
			$inventory = Product::select('products.id', 'products.name', 'products.brand', 'products.price',
			'products.msrp', 'products.quantity', 'products.unit_cost', 'categories.name as categoryname',
			'images.image', 'images.image_upload_type')
			->leftjoin("categories",function($join){

				$join->on("categories.id","=","products.category");

			})
			->leftJoin("images",function($join){

				$join->on("images.item_id","=","products.id")->where('images.main_image', 1);

			})
			->where('products.user_id',$userid)->get();
			return view('admin.users.inventorylisting', ['inventory' => $inventory]);
		} else {
			return redirect('dashboard');
		}
	}
	
	public function detail($productid, Request $request) {
		if(Auth::user()->type == 2) {
			$inventory = Product::select('products.id', 'products.name', 'products.brand', 'products.price',
			'products.msrp', 'products.quantity', 'products.unit_cost', 'categories.name as categoryname')
			->leftjoin("categories",function($join){

				$join->on("categories.id","=","products.category");

			})
			->where('products.id',$productid)->first();
			return view('admin.inventory.detail', ['inventory' => $inventory]);
			
		} else {
			return redirect('dashboard');
		}
	}
}
