<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect, Validator;
use Session;
use Auth;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
	
	public function dashboard() {
		if(Auth::user()->type == 2) {
			return view('admin.dashboard.dashboard', []);
		} else {
			return redirect('dashboard');
		}
	}
}
