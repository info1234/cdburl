<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Input;
use Redirect, Validator;
use App\User;
use Auth;
use Hash;
use DB;
use Cookie;
class PasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request) {
		if (Auth::check()) {
			if($request->isMethod('post')) {
				$data = $request->all();

				$validator = Validator::make($data, [
					'password' => 'required',
					'newPassword' => 'min:6',
					'confirmNewPassword' => 'required_with:newPassword|same:newPassword|min:6'
				]);

				if($validator->fails()) {
					return redirect()->back()->withErrors($validator->errors());
				}

				if(!(Hash::check($data['password'], Auth::user()->password))) {
					$request->session()->flash('alert-danger', 'Your current password does not matches with the password you provided. Please try again.');
					return Redirect::route('changePassword');
				}
				
				$id = Auth::user()->id;
				$status = User::where('id', $id)->update(['password' => bcrypt($data['newPassword'])]);
				if($status) {
					$request->session()->flash('alert-success', 'Your password has been changed successfully!');
					return Redirect::route('changePassword');
				}
				
				$request->session()->flash('alert-danger', 'Somethingwrong please try again');
				return Redirect::route('changePassword');
			}
			return view('password/form', []);
		} else {
			return redirect()->action('DashboardController@dashboard');
		}
		
	}

	public function register() {
		return redirect()->action('IndexController@home');
	}

	public function login() {
		return redirect()->action('IndexController@home');
	}
}

