<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Input;
use Redirect;
use App\Deal;
use App\Store;
use Auth;
use DB;
use App\Like;
use App\Brand;
use Cookie;
class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request) {
		return view('home/index', []);
	}

	public function register() {
		return redirect()->action('IndexController@home');
	}

	public function login() {
		return redirect()->action('IndexController@home');
	}
}

