<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
	protected function authenticated(Request $request, $user) {
		if(Auth::user()->type == 1) {
			return redirect()->intended('/dashboard');
		} else {
			return redirect()->intended('/admin/dashboard');
		}
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
	/* public function redirectTo() {
		if (Auth()->user()->type_id === 1) {
			return '/dashboard';
		} elseif (Auth()->user()->type_id === 2) {
			return '/admin/dashboard';
		}
	} */


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		//$this->redirectTo = url()->previous();
        $this->middleware('guest', ['except' => 'logout']);
    }
	
	/*Custom*/
	protected function validator($data) {
        return Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
    }
	
	public function login(Request $request) {
		$this->validateLogin($request);
		if($request->ajax()){
			
			if ($this->hasTooManyLoginAttempts($request)) {
				$this->fireLockoutEvent($request);
				return $this->sendLockoutResponse($request);
			}
			
			if ($this->attemptLogin($request)) {
				return $this->sendLoginResponse($request);
			}
			
			$this->incrementLoginAttempts($request);
			
			return $this->sendFailedLoginResponse($request);
		} else {
			if ($this->hasTooManyLoginAttempts($request)) {
				$this->fireLockoutEvent($request);
				return $this->sendLockoutResponse($request);
			}
			
			if ($this->attemptLogin($request)) {
				return $this->sendLoginResponse($request);
			}
			$this->incrementLoginAttempts($request);
			
			return $this->sendFailedLoginResponse($request);
		}
	}
	
	protected function validateLogin(Request $request) {
		$data = $request->all();
		if(isset($data['type']) && $data['type'] == 1) {
			$this->validate($request, [
				'email' => 'required|exists:users,'.$this->username().',type,1',
				'password' => 'required|string',
			]);
		} else {
			$this->validate($request, [
				'email' => 'required|exists:users,'.$this->username().',type,2',
				'password' => 'required|string',
			]);
		}
       
    }
}
