<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Auth;
use DB;
use Cookie;
use App\Pricing;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request) {
		if (Auth::check()) {
			return redirect()->action('DashboardController@dashboard');
		} else {
			$userType = 1;
			$logintype = 'login';
			$route = $request->segment(1);
			if($route == 'admin') {
				$userType = 2;
				$logintype = 'admin';
			}
			$pricing = Pricing::get();
			
			$pricingHeaderData[] = array(
				'type' => 'Pricing',
				'price' => '',
				'price_per' => '',
				'Integrated_Channel' => 'Integrated Channel',
				'multiple_account' => 'Multiple Accounts for Each Channel',
				'staff_accounts' => 'Staff Accounts',
				'managed_active_listing' => 'Managed Active Listings',
				'orders_per_month' => 'Orders per Month',
				'image_hosting' => 'Image Hosting',
				'powerful_pricing_rules' => 'Powerful Pricing Rules',
				'daily_inventory_monitor' => 'Daily Inventory Monitor',
				'daily_price_monitor' => 'Daily Price Monitor',
				'automatic_order_fulfillment' => 'Automatic Order Fulfillment',
				'order_tracking_number_sync' => 'Order Tracking Number Sync',
				'dropshipping_supplier_api' => 'Dropshipping Supplier API',
				'ebay_templates_and_presets' => 'eBay Templates & Presets',
				'ebay_auto_restock' => 'eBay Auto Restock',
				'ebay_message_assistant' => 'eBay Message Assistant',
				'usps_shipping_labels' => 'USPS Shipping Labels',
				'automatic_tracking_info_updates' => 'Automatic Tracking Info Updates',
				'support' => 'Support',
				'select_plan' => 'Select Plan',
			); 
			foreach($pricing as $pricingObj) {
				$support = json_decode($pricingObj['support']);
				$pricingHeaderData[] = array(
					'type' => $pricingObj['type'],
					'price' => $pricingObj['price'],
					'price_per' => $pricingObj['price_per'],
					'Integrated_Channel' => $pricingObj['integrated_channel'],
					'multiple_account' => $pricingObj['multiple_account'],
					'staff_accounts' => $pricingObj['staff_accounts'],
					'managed_active_listing' => $pricingObj['managed_active_listing'],
					'orders_per_month' => $pricingObj['orders_per_month'],
					'image_hosting' => $pricingObj['image_hosting'],
					'powerful_pricing_rules' => $pricingObj['powerful_pricing_rules'],
					'daily_inventory_monitor' => $pricingObj['daily_inventory_monitor'],
					'daily_price_monitor' => $pricingObj['daily_price_monitor'],
					'automatic_order_fulfillment' => $pricingObj['automatic_order_fulfillment'],
					'order_tracking_number_sync' => $pricingObj['order_tracking_number_sync'],
					'dropshipping_supplier_api' => $pricingObj['dropshipping_supplier_api'],
					'ebay_templates_and_presets' => $pricingObj['ebay_templates_and_presets'],
					'ebay_auto_restock' => $pricingObj['ebay_auto_restock'],
					'ebay_message_assistant' => $pricingObj['ebay_message_assistant'],
					'usps_shipping_labels' => $pricingObj['usps_shipping_labels'],
					'automatic_tracking_info_updates' => $pricingObj['automatic_tracking_info_updates'],
					'support' => $support,
					//'select_plan' => '<button class="btn get-started" onclick="window.location.href="#login">get started now</button>'
				);
			}
			$tablehead = array();
			foreach($pricingHeaderData as $K => $datainarray) {
				$tablehead['type'][] = array('type' => $datainarray['type'],'price' => $datainarray['price'],'price_per' => $datainarray['price_per']);
			}
			//echo '<pre>'; print_r($pricingHeaderData); die;
			return view('home/home', ['userType' => $userType, 'logintype' => $logintype, 'tablehead' => $tablehead, 'pricingData' => $pricingHeaderData]);
		}
	}

}

