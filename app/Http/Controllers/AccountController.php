<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Input;
use Redirect, Validator;
use Auth;
use App\User;
use DB;
class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function account(Request $request) {
		if($request->isMethod('post')) {
			$data = $request->all();
			
			$validate = ['name' => 'required', 'title' => 'required'];
			
			if(!empty($data['mobile'])) {
				$validate = ['name' => 'required', 'title' => 'required', 'mobile' => 'required|min:11|numeric'];
			}
			
			$validator = Validator::make($data, $validate);
			
			if($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			
			$id = Auth::user()->id;
			$updateData = ['name' => $data['name'], 'title' => $data['title'], 'mobile' => $data['mobile']];
			$status = User::where('id', $id)->update($updateData);

			if($status) {
				$request->session()->flash('alert-success', 'Account successfully update!');
				return Redirect::route('accountInfo');
			} else {
				$request->session()->flash('alert-danger', 'Something wrong please try again!');
				return Redirect::route('accountInfo');
			}
		}
		return view('account/form', []);
	}

	public function register() {
		return redirect()->action('IndexController@home');
	}

	public function login() {
		return redirect()->action('IndexController@home');
	}
}

