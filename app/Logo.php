<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type'
    ];
}
