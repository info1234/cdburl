<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id', 'image', 'type', 'main_image', 'image_type', 'parent_id', 'image_upload_type'
    ];
}
