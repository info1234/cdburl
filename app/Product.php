<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sku', 'brand', 'description', 'price', 'dimensions_width_value', 'category',
		'msrp', 'quantity', 'unit_cost', 'MPN', 'condition', 'dimensions_length_value', 'short_description',
		'dimensions_height_value', 'dimensions_option', 'weight', 'weight_option', 'customs_description',
		'declared_value', 'declared_option', 'country', 'tariff_number', 'user_id', 'url', 'source_market'
    ];
}
