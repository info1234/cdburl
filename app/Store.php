<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
	//public $timestamps = false;
}
