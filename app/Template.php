<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id', 'template_id'];
	//public $timestamps = false;
}
