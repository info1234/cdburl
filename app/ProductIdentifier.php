<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductIdentifier extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_identifier';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'product_Identifier', 'product_Identifier_value'];
}
