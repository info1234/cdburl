<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuAliases extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sku_aliases';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'SKU_aliases_option', 'SKU_aliases_value'];
}
