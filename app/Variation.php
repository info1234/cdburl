<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'variation';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'name', 'value'];
	//public $timestamps = false;
}
