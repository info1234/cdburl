<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pricing';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'type', 'price_per', 'integrated_channel', 'multiple_account', 'staff_accounts', 'managed_active_listing', 'orders_per_month',
		'image_hosting', 'powerful_pricing_rules', 'daily_inventory_monitor', 'daily_price_monitor', 'automatic_order_fulfillment',
		'order_tracking_number_sync', 'dropshipping_supplier_api', 'ebay_templates_and_presets', 'ebay_auto_restock', 'ebay_message_assistant',
		'usps_shipping_labels', 'automatic_tracking_info_updates', 'support'
    ];
}
