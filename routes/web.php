<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Route::get('/', function () {
    return view('welcome');
}); */

Auth::routes();

Route::get('/', 'HomeController@home');

Route::get('/login', 'HomeController@home')->name('login');
Route::get('/register', 'HomeController@home')->name('register');

//Route::get('/', 'DashboardController@dashboard');
Route::get('/dashboard2', 'DashboardController@dashboard2');
Route::get('/dashboard', 'DashboardController@dashboard');
Route::get('/accountInfo', 'AccountController@account')->name('accountInfo');
Route::post('/accountInfo', 'AccountController@account');
Route::match(['get', 'post'], '/changePassword', 'PasswordController@changePassword')->name('changePassword');

Route::get('/order/index/{params?}', 'OrderController@index');
Route::get('/shopify/product/{params?}/{params2?}', 'ShopifyController@product');
Route::get('/user/templates/{params?}/{params2?}', 'UserController@templates');
Route::get('/templates/add', 'TemplateController@add');
Route::post('/templates/add', 'TemplateController@add');
Route::get('/templates/templates', 'TemplateController@templates');
Route::post('/template/change', 'TemplateController@change');
Route::get('/template/preview', 'TemplateController@preview');

Route::get('/ebay/message/{params?}', 'EbayController@message');
Route::get('/album/manage', 'AlbumController@manage');
Route::get('/album/upload', 'AlbumController@upload');

Route::get('/product/index', 'ProductController@index');
Route::get('/product/create', 'ProductController@create');

Route::get('/inventory/scraper/{params?}', 'InventoryController@scraper');
Route::get('/inventory/product', 'InventoryController@product');
Route::get('/inventory/variation', 'InventoryController@variation');
Route::get('/inventory/add', 'InventoryController@add');
Route::post('/inventory/add', 'InventoryController@add');
Route::get('/inventory/edit/{params}', 'InventoryController@edit');
Route::post('/inventory/edit/{params}', 'InventoryController@edit');
Route::get('/inventory/import', 'InventoryController@import');
Route::get('/inventory/export', 'InventoryController@export');
Route::post('/inventory/search', 'InventoryController@search');
Route::post('/inventory/import', 'InventoryController@import');
Route::get('/inventory/sample', 'InventoryController@sample');

Route::get('/inventory/detail/{id}', 'InventoryController@detail');

Route::get('/shop', 'ShopController@shop');
Route::get('/setting/dropshipSettings', 'SettingController@dropshipSettings');
Route::get('/category/inventory', 'CategoryController@inventory');
Route::get('/category/image', 'CategoryController@inventory');

Route::get('/message/inbox', 'MessageController@inbox');
Route::get('/message/send', 'MessageController@send');
Route::get('/message/messagebots', 'MessageController@messagebots');

Route::post('/category/create', 'CategoryController@create');
Route::post('/category/update', 'CategoryController@update');
Route::delete('category/delete/{id}', 'CategoryController@delete');

Route::delete('inventory/removevariantbyid/{id}', 'InventoryController@removevariantbyid');

Route::get('/inventory/getcontentdata', 'InventoryController@getcontentdata');
Route::post('/inventory/create', 'InventoryController@create');
Route::get('/image/index', 'ImageController@index');

Route::get('/billing', 'PaymentSettingsController@billing');

/*start admin routes*/
Route::get('/admin', 'HomeController@home')->name('admin.login');
Route::prefix('admin')->group(function() {
	Route::get('/dashboard', 'Admin\DashboardController@dashboard')->name('admin.dashboard');
	Route::get('/userlisting', 'Admin\UserController@userlisting');
	Route::get('/inventorylisting/{userid}', 'Admin\InventoryController@inventorylisting');
	
	Route::get('/inventory/detail/{productid}', 'Admin\InventoryController@detail');
	Route::get('/pricing', 'Admin\PricingController@pricing');
	Route::get('/pricing/edit/{id}', 'Admin\PricingController@edit');
	Route::post('/pricing/edit/{id}', 'Admin\PricingController@edit');
});