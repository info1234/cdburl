@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Shopify</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->

<div class="product-inventory-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="inventory-prod">
                                <div class="tory-left">
                                    <ul class="breadcrumb">
                                        <li><a class="breadcrumb-item" href="#">Inventory</a></li>
                                        <li><a class="breadcrumb-item" href="#">Product Scraper</a></li>
                                    </ul>

                                </div>
                                <span class="tory-right">
                                    <a href="#https://support.shopmaster.com/help/Import-Products-to-ShopMaster-&-Online-Stores/indexPage.htm?type=1&isVideo=0&helpCategoryId=8&helpContentId=48">Help</a>
                                </span>
                            </div>


                            <!--save-time-->
                            <div class="save-time">
                                <span class="extension">
                                    <img src="{{asset('images/extension.png')}}">
                                </span>
                                <span class="adding">
                                    <h6>Save time adding products by using the ShopMaster Chrome Extension</h6>
                                </span>
                                <span class="chrome-ext-btn">
                                    <a href="#"> Get Chrome Extension</a>
                                </span>
                            </div>


                            <!--list_nav-->
                            <div class="list_nav">
                                <ul>
                                    <li class="import-pro"><a href="#">Import Products</a></li>
                                    <li><a href="#"><span>Bulk Import</span>  <img src="{{asset('images/bulk.png')}}"></a></li>
                                </ul>
                                <a class="setting pull-right" href="https://www.shopmaster.com/setting/dropshipSettings/importIndex.htm"><i class="fa fa-cog" aria-hidden="true"></i> Settings</a>
                            </div>


                            <!--please-enter-part-->
                            <div class="please-enter-part">
                                <div class="input-sec">
                                    <input type="text" placeholder="Please enter the URL of Product, example: https://www.aliexpress.com/item/title_name/32790425974.html">
                                </div>
                                <div class="add-btn">
                                   <a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                                <div class="btn-part-sec">
                                    <span class="click-btn"><a href="#">Click to Import</a></span>
                                    <span class="clear-btn"><a href="#">Clear</a></span>
                                    <span class="import-express"><a href="#">Import AliExpress, Amazon and 29 more sites <i class="fa fa-angle-down" aria-hidden="true"></i></a> </span>
                                </div>
                            </div>


                            <!--cant-import-->
                            <div class="cant-import">
                                <p> <img src="{{asset('images/cant.png')}}">Can't I Import Products from AliExpress Sometimes? Click <span><a href="#">here</a></span> to find helps</p>
                            </div>


                            <!--quick-filter-->
                            <div class="quick-filter">
                                <h5>Quick Filter</h5>
                                <div class="import-from">
                                    <h6>Import From</h6>
                                    <ul>
                                        <li class="active"><a href="#">All</a> </li>
                                        <li><a href="#">1688</a> </li>
                                        <li><a href="#"> AliExpress</a> </li>
                                        <li><a href="#">Alibaba</a> </li>
                                        <li><a href="#">Amazon</a> </li>
                                        <li><a href="#">Banggood</a> </li>
                                        <li><a href="#">Chinabrands</a> </li>
                                        <li><a href="#"> Chinavasion</a> </li>
                                        <li><a href="#">DHgate</a> </li>
                                        <li><a href="#">eBay</a> </li>
                                        <li><a href="#">Gearbest</a> </li>
                                        <li><a href="#">Taobao</a> </li>
                                        <li><a href="#">Tmall</a> </li>
                                        <li><a href="#">TMART</a> </li>
                                        <li><a href="#"> Walmart</a> </li>

                                    </ul>
                                </div>
                                <div class="import-time">
                                    <h6>Import Time</h6>
                                    <ul>
                                        <li class="active"><a href="#">All</a> </li>
                                        <li><a href="#">Today</a> </li>
                                        <li><a href="#">7 days</a> </li>
                                        <li><a href="#">30 days</a> </li>
                                    </ul>
                                </div>
                                <div class="checkbox-part">
                                    <form>
                                        <label>Specifices</label><input type="checkbox" value="prime"><value>Prime</value>
                                        <input type="checkbox" value="prime"><value>Gift</value>
                                    </form>
                                </div>
                            </div>


                            <!--create-listing-->
                            <div class="create-listing">
                                <ul>
                                    <li><select><Option>Create Listings</Option><option>Shopify</option></select></li>
                                    <li><a href="#">Create Product</a> </li>
                                    <li><a href="#">Delete</a> </li>
                                </ul>

                            </div>


                            <!--form-part-search-->
                            <div class="form-part-search">
                                <input type="text" placeholder="Search Name/ID/ASIN">
                            </div>


                            <!--table-part-->
                            <div class="list_content_list">
                                <table class="alone-table">
                                    <tr>
                                        <th class="inputcheck"><input type="checkbox"> </th>
                                        <th class="imageleft">Image</th>
                                        <th class="nameleft">Name</th>
                                        <th class="discount">Discount Price / Price</th>
                                        <th class="variation-right">Variation</th>
                                        <th class="from-right">Import From</th>
                                        <th class="time-right">Imported Time</th>
                                        <th class="action-right">Action</th>

                                    </tr>

                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_1.png')}}"> </td>
                                        <td class="nameleft"> <p>DESTINY Belt Men Luxury Famous Brand Designer High Quality Male Genuine Leather Strap White Automatic Buckle Belt Ceinture Homme</p>
                                                              <p>32798135060</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>


                                        </td>
                                        <td class="discount">
                                            <h6>USD 6.77</h6>
                                            <p>USD 19.90</p>

                                        </td>
                                        <td class="variation-right"><a href="#">60</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_2.png')}}"> </td>
                                        <td class="nameleft"> <p>Famous Brand Belt Men 100% Good Quality Cowskin Genuine Luxury Leather Men's Belts for Men,Strap Male Metal Automatic Buckle</p>
                                            <p>32671880925</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 10.06</h6>
                                            <p>USD 12.90</p>

                                        </td>
                                        <td class="variation-right"><a href="#">50</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_3.png')}}"> </td>
                                        <td class="nameleft"> <p>Helpful Nylon Belt Male Army Tactical Belt Men Military Waist Canvas Belts Cummerbunds High Quality Strap 4 Colors</p>
                                            <p>32817542418</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 2.59~2.60</h6>
                                            <p>USD 2.73~2.74</p>

                                        </td>
                                        <td class="variation-right"><a href="#">4</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_4.png')}}"> </td>
                                        <td class="nameleft"> <p>NIBESSER Fashion Men Women General Belts Canvas Unisex Candy Color Belt Solid Buckle Military Belts For Jeans Cowboy Pants 110cm</p>
                                            <p>32813212540</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 2.04~2.60</h6>
                                            <p>USD 2.34~2.99</p>

                                        </td>
                                        <td class="variation-right"><a href="#">5</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_5.png')}}"> </td>
                                        <td class="nameleft"> <p>[MILUOTA] 2016 Military Equipment Tactical Belt Man Double Ring Buckle Thicken Canvas Belts for Men Waistband MU035</p>
                                            <p>32667400892</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 7.88</h6>
                                            <p>USD 18.76</p>

                                        </td>
                                        <td class="variation-right"><a href="#">70</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_6.png')}}"> </td>
                                        <td class="nameleft"> <p>Tactical Belt Military Nylon Belt Men Army Style Casual Belt Automatic Metal Buckle Survival Cinturon Combat Belt SWAT Duty Gear</p>
                                            <p>32864874919</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 6.89~7.90</h6>
                                            <p>USD 10.29~11.79</p>

                                        </td>
                                        <td class="variation-right"><a href="#">6</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_7.png')}}"> </td>
                                        <td class="nameleft"> <p>Men Belt Marcas New Fashion cow Leather Belts for Men 12 Style Color Pin Buckle good Quality genuine leather strap for male</p>
                                            <p>32220493156</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 9.86~10.98</h6>
                                            <p>USD 17.60~19.60</p>

                                        </td>
                                        <td class="variation-right"><a href="#">10</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_8.png')}}"> </td>
                                        <td class="nameleft"> <p>Unisex Waist Belt Mens Plain Webbing Waistband Casual Canvas Belt High Quality</p>
                                            <p>32797543542</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 1.69~1.79</h6>
                                            <p>USD 2.73~2.89</p>

                                        </td>
                                        <td class="variation-right"><a href="#">10</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_9.png')}}"> </td>
                                        <td class="nameleft"> <p>2018 HOT Exterior Wild Men Metal Belt Fabric Hypoallergenic Free Elastic</p>
                                            <p>32777650809</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 2.54~2.71</h6>
                                            <p>USD 3.17~3.39</p>

                                        </td>
                                        <td class="variation-right"><a href="#">5</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_10.png')}}"> </td>
                                        <td class="nameleft"> <p>Fashion High Quality Brand Man Belt Split Leather Belt Italian Design Casual Men's Leather Belts For Jeans For Man Free Shipping</p>
                                            <p>32696884128</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 7.36</h6>
                                            <p>USD 11.50</p>

                                        </td>
                                        <td class="variation-right"><a href="#">21</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_11.png')}}"> </td>
                                        <td class="nameleft"> <p>[KURAN] New Brand designer mens belts luxury real leather belts for men metal buckle man Jeans pants genuine leather belt male s</p>
                                            <p>32858077957</p>
                                            <div class="tooltip"><i class="fa fa-file-text" aria-hidden="true"></i>
                                                <span class="tooltiptext">Item Type: Belts;Belts Material: Cowskin,Metal; Buckle Width: 4.1cm; Gender: Men; Brand Name: DESTINY;Model Number: KaBaiSe; Belt Width: 3.5cm; Style: Fashion;Pattern Type: Solid;	Buckle Length: 8.2cm; Department Name: Adult;color: white;Design: Famous designer;Factory outlet: YesAterial: Cow genuine leather;Strap buckle material: Alloy buckle;Strap buckle: Automatic buckle belt;Style: Business,Casual,Fashion,Simple;Belt Length: 110cm,115cm,120cm,125cm;</span>
                                            </div>
                                        </td>
                                        <td class="discount">
                                            <h6>--</h6>
                                            <p>USD 4.69~8.48</p>

                                        </td>
                                        <td class="variation-right"><a href="#">45</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_2.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <img src="{{asset('images/icon_4.png')}}"></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_12.png')}}"> </td>
                                        <td class="nameleft"> <p>belt men leather belt men male genuine leather strap luxury pin buckle belts for men cintos masculinos</p>
                                            <p>32860698051</p>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 5.98~8.74</h6>
                                            <p>USD 12.99~18.99</p>

                                        </td>
                                        <td class="variation-right"><a href="#">24</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_2.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <img src="{{asset('images/icon_4.png')}}"></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_13.png')}}"> </td>
                                        <td class="nameleft"> <p>2018 New Men Automatic Letter Buckle Leather Waist Strap Belts Buckle Belt cintos masculinos de couro luxo</p>
                                            <p>32852210605</p>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 2.53~2.94</h6>
                                            <p>USD 3.52~4.09</p>

                                        </td>
                                        <td class="variation-right"><a href="#">4</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_2.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <img src="{{asset('images/icon_4.png')}}"></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <table class="theme-table-backgrnd">
                                    <tr>
                                        <td class="inputcheck"><input type="checkbox"></td>
                                        <td class="imageleft"><img src="{{asset('images/img_14.png')}}"> </td>
                                        <td class="nameleft"> <p>WOWTIGER Fashion Designers Men Automatic Buckle Leather luxury Belts Business Male Alloy buckle Belts for Men Ceinture Homme</p>
                                            <p>32219959228</p>
                                        </td>
                                        <td class="discount">
                                            <h6>USD 10.25</h6>
                                            <p>USD 18.99</p>

                                        </td>
                                        <td class="variation-right"><a href="#">105</a> </td>
                                        <td class="from-right"><a href="#">Aliexpress</a> </td>
                                        <td class="time-right"><p>06-28-2018</p></td>
                                        <td class="action-right">
                                            <ul>
                                                <li> <a href="#"> <img src="{{asset('images/icon_1.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_2.png')}}"></a></li>
                                                <li><a href="#"> <img src="{{asset('images/icon_3.png')}}"></a></li>
                                                <li> <a href="#"> <img src="{{asset('images/icon_4.png')}}"></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>

                            </div>

                            <!--pagination-part-->

                            <div class="stored-in">
                                <span class="products-here">
                                      <p>Products here are stored in ShopMaster for 30 days</p>
                                </span>
                                <span class="pagnation-part">
                                    <p>1-50 of 110</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-step-backward" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-step-forward" aria-hidden="true"></i></a> </li>
                                         <li><a href="#">1/3</a> </li>
                                         <li><a href="#">Go to</a> </li>
                                         <li><a href="#">View
                                             <select>

                                                 <option>50</option>
                                                 <option>40</option>
                                                 <option>30</option>

                                             </select>Per page</a> </li>

                                    </ul>
                                </span>


                            </div>


                        </div>
                    </div>
                </div>
            </div>
@endsection
