@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Billing</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-inventory-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inventory-prod">
					<div class="tory-left">
						<ul class="breadcrumb">
							<li><a class="breadcrumb-item" href="#">Billing</a></li>
						</ul>

					</div>
					<br><br>
					<div class="col-md-12">
						<button type="button" class="save-changes btn payWithCreditCard">Pay with Credit Card</button>
						<button type="button" class="save-changes btn">Pay with PayPal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Container fluid  -->
@endsection

@push('css')
<style>
</style>
@endpush
@push('js')
<script>
$(document).ready(function() {
	$('body').on('click', '.payWithCreditCard', function() {
		
	});
});
</script>
@endpush