@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Payment</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-inventory-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="inventory-prod">
                                <div class="tory-left">
                                    <ul class="breadcrumb">
                                        <li><a class="breadcrumb-item" href="#">Payment Settings</a></li>
                                    </ul>

                                </div>
								<br><br>
								<!--table-part-->
                            <form method="post" action="<?php echo url('inventory/create'); ?>" id="inventory-form">
								<div class="col-sm-6">
								  <div class="panel panel-default">
									<div class="panel-body">
									  <select class="form-control" name="source_market" id="shop" required>
										<option value="" selected="" disabled="">Select Source Market</option>
										
										  <option value="amazon_us">amazon.com</option>
										
										  <option value="amazon_uk">amazon.co.uk</option>
										
										  <option value="amazon_ca">amazon.ca</option>
										
										  <option value="amazon_de">amazon.de</option>
										
										  <option value="amazon_fr">amazon.fr</option>
										
										  <option value="amazon_it">amazon.it</option>
										
										  <option value="amazon_in">amazon.in</option>
										
										  <option value="amazon_es">amazon.es</option>
										
										  <option value="aliexpress_us">aliexpress.com</option>
										
										  <option value="bestbuy_us">bestbuy.com</option>
										
										  <option value="bhphotovideo_us">bhphotovideo.com</option>
										
										  <option value="costco_us">costco.com</option>
										
										  <option value="kmart_us">kmart.com</option>
										
										  <option value="lowes_us">lowes.com</option>
										
										  <option value="overstock_us">overstock.com</option>
										
										  <option value="samsclub_us">samsclub.com</option>
										
										  <option value="sears_us">sears.com</option>
										
										  <option value="walmart_us">walmart.com</option>
										
										
									  </select>
									</div>
								  </div>
								</div>
								
								
							</form>
                            </div>


                            



                        </div>
                    </div>
                </div>
            </div>
<!-- End Container fluid  -->

@endsection

@push('css')
<style>

.modal {
	display:    none;
	position:   fixed;
	z-index:    1000;
	top:        0;
	left:       0;
	height:     100%;
	width:      100%;
	background: rgba( 255, 255, 255, .8 ) 
	url('http://i.stack.imgur.com/FhHRx.gif')
	50% 50%
	no-repeat;
}
body.loading {
	overflow: hidden;   
}
body.loading .modal {
	display: block;
}

</style>
@endpush
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script>
$(document).ready(function() {
	InventoryManager.init();
	$('#producturl').val('');
});
  $(function () {
    $('#myTab a:secound').tab('show')
  })
</script>
@endpush