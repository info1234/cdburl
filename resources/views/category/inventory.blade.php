@extends('layouts.app')

@section('content')

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">categories</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>

<div class="pro_categories_page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="all-categories">
					<ul>
						<li class="head_all">All Categories</li>
						<?php
							foreach($categories as $key => $categorieObj) {
								if($categorieObj['name'] == 'Uncategorized') {
						?>
							<li class="defalult_cat">
								<span>Uncategorized</span>
								<span class="mark">Default</span>
							</li>
						<?php 
								} else {
						?>
							<li class="defalult_cat bgclr defalult_cat_<?php echo $categorieObj['id']; ?>">
							   <div class="hide-part hide-part-<?php echo $categorieObj['id']; ?>">
									<span class="category_label category_label_<?php echo $categorieObj['id']; ?>"><?php echo $categorieObj['name']; ?></span>
									<div class="action_btn">
									   <a href="#" class="pencil_edit" data-edit-pencil="<?php echo $categorieObj['id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									   <!--<a href="#" class="deletecategory" data-category-id="<?php //echo $categorieObj['id']; ?>" data-toggle="modal" data-target="#myModalDeleteCategory"><i class="fa fa-trash" aria-hidden="true"></i></a>
										-->
										<a href="#" class="deletecategory" data-category-id="<?php echo $categorieObj['id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
							   </div>
								<div class="hide-sec hide-sec-<?php echo $categorieObj['id']; ?> category_box_<?php echo $categorieObj['id']; ?>">
									<div class="input_popup">
										<input type="text" id="category_edit_<?php echo $categorieObj['id']; ?>" value="<?php echo $categorieObj['name']; ?>">
									</div>

									<div class="action_btn bottom-part">
										<a href="#" class="file_text category_update category_update_<?php echo $categorieObj['id']; ?>" data-categoryid="<?php echo $categorieObj['id']; ?>"><i class="fa fa-file-text" aria-hidden="true"></i></a>
										<!--<a href="#" class="deletecategory" data-category-id="<?php //echo $categorieObj['id']; ?>" data-toggle="modal" data-target="#myModalDeleteCategory"><i class="fa fa-trash" aria-hidden="true"></i></a>-->
										<a href="#" class="deletecategory" data-category-id="<?php echo $categorieObj['id']; ?>" data-toggle="modal" data-target="#myModalDeleteCategory"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
								</div>
							</li>
							<?php } } ?>
					</ul>
				</div>
				<div class="category_btn">
					<a href="#" data-toggle="modal" data-target="#myModalCategory">Add Category</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModalCategory" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form method="post" action="<?php echo url('category/create'); ?>" id="category-form">
				<span class="category-form-error" style="margin-left:20px;margin-top:20px;color:red"></span>
				<div class="modal-header">
					<label>Category Name</label><input type="text" id="createcategory" name="category" placeholder="">
				</div>
				<div class="close_add-btn">
					<button type="submit" class="btn btn_sm_primary categoryAdd">Add</button>
					<button type="button" class="btn btn_sm_white close_category_modal" data-dismiss="modal">&times; Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="myModalDeleteCategory" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"></span><span class="sr-only">Close</span></button>
				<h2><i class="fa fa-exclamation-circle" aria-hidden="true"></i>Delete  category?</h2>
			</div>
			<div class="close_add-btn">
				<button type="button" class="btn btn_sm_primary categoryAdd">Confirm</button>
				<button type="button" class="btn btn_sm_white" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
	<div class="modal-backdrop fade in"></div>
</div>

<a href="javascript:void(0)" class="scrollToTop" ></a>
@endsection

@push('js')
<script type="text/javascript">
 $(document).ready(function() {
	$("#category-form").submit(function() {
		var category = $("#createcategory").val();
		var url = document.getElementsByName('base_url')[0].getAttribute('content');
		url = url+'/category/create';
		$(".category-form-error").html('');
		$.ajax({
			type: "post",
			url: url,
			data: {category: category},
			dataType: "json",
			success: function (response) {
			   if(response.status == 'success') {
				   $('#createcategory').val('');
				   location.reload();
			   } else if(response.status == 'error') {
				   $(".category-form-error").html(response.message);
			   }
			}
		});
		return false;
	});
	 
    $(".hide-sec").hide();
	/* $(".defalult_cat").click(function(){
		$(".hide-sec").show();
		$(".hide-part").hide();
		$(".bgclr").css("background", "none");
		$(".action_btn.bottom-part").css("top", "14px");

	}); */
	
	$('body').on('click', '.pencil_edit', function() {
		var categoryid = $(this).attr('data-edit-pencil');
		$(".hide-part-"+categoryid).hide();
		$(".hide-sec-"+categoryid).show();
		return false;
	});
	
	$('body').on('click', '.file_text', function() {
		var url = document.getElementsByName('base_url')[0].getAttribute('content');
		url = url+'/category/update';
		var categoryid = $(this).attr('data-categoryid');
		var value = $('#category_edit_'+categoryid).val();
		$.ajax({
			type: "post",
			url: url,
			data: {categoryid: categoryid, value: value},
			dataType: "json",
			success: function (response) {
			   if(response.status == 'success') {
				   $(".hide-part-"+categoryid).show();
				   $(".hide-sec-"+categoryid).hide();
				   $('.category_label_'+response.categoryid).html(response.name);
			   } else if(response.status == 'error') {
				   alert(response.message);
			   }
			}
		});
		//$(".hide-part-"+categoryid).show();
		//$(".hide-sec-"+categoryid).hide();
		return false;
	});
	
	$('body').on('click', '.deletecategory', function() {
		if(confirm('Are you sure you want to delete this category?')) {
			var categoryid = $(this).attr('data-category-id');
			var url = document.getElementsByName('base_url')[0].getAttribute('content');
			url = url+'/category/delete/'+categoryid;
			$.ajax({
				type: "delete",
				url: url,
				data: {categoryid: categoryid},
				dataType: "json",
				success: function (response) {
				   if(response.status == 'success') {
					   $('.defalult_cat_'+response.categoryid).remove();
				   } else if(response.status == 'error') {
					   
				   }
				}
			});
		}
		return false;
	});

	$(".close_category_modal").click(function() {
		location.reload();
	});

});
</script>
@endpush