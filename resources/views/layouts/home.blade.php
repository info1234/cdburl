<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="base_url" content="{{ URL::to('/') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="icon" type="image/ico" href="{{ URL::to('/public/images/favicon.ico') }}">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
	
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home/css/aos.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	
	<link href="{{ asset('css/home/css/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/home/css/swiper.min.css') }}" rel="stylesheet" type="text/css" />
	
    <!-- Main Stylesheet -->
    <link href="{{ asset('css/home/css/style.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <!-- Responsive Stylesheet -->
    <link href="{{ asset('css/home/css/responsive.css') }}" rel="stylesheet" type="text/css" media="all">
	
	@stack('css')
</head>
<body>
<!-- Header -->
	<header>
		<div class="homepage-header">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 ">
					   <a href="index.html" class="logo">
						<img src="images/logo.png" alt="logo" class="img-fluid">
					   </a>
					</div>
					<div class="col-md-9 col-sm-9 col-12">
						<ul class="contact-info">
							<li><i class="fa fa-phone"></i>+569876543210</li>
							<li><i class="fa fa-paper-plane-o"></i>+shop@gmail.com</li>
						</ul>
						<ul class="social-links ">
							<li><a href="" class="fa fa-facebook icon icon-fb"></a></li>
							<li><a href="" class="fa fa-twitter icon icon-tw"></a></li>
							<li><a href="" class="fa fa-pinterest icon icon-pin"></a></li>
							<li><a href="" class="fa fa-instagram icon icon-ig"></a></li>
						</ul>
					</div>
				</div>
			</div>
			  
			<nav class=" nav nav-flex custom-nav">
				<div class="container">
					<div class="nav-custom">
						<a class="navbar-brand" href="#"> <img src="images/logo.png" alt="logo" class="img-fluid"></a>

						<!-- Toggler/collapsibe Button -->
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
							<span class="navbar-toggler-icon fa fa-bars"></span>
						</button>

						<div class="clearfix visible-xs"></div>
						<!-- Navbar links -->
						<div class="collapse navbar-collapse" id="collapsibleNavbar">
							<ul class="navbar-nav">
							  <li class="nav-item"><a href="" class="nav-link active">Features</a></li>
								 <li class="nav-item"><a href="" class="nav-link">Pricing</a></li>
								 <li class="nav-item"><a href="" class="nav-link">Support</a></li>
								 <li class="nav-item"><a href="" class="nav-link">Contact</a></li> 
							</ul>
						</div> 
					</div> 
				</div>
			</nav>

			  
			<div class="container">
				<div class="bnr-section" id="login">
					<h3 class="text-right">Save money and Sell more</h3>
					<p class="text-right">We help you start profitable drop shipping business across multiple channels</p>
			  
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<figure  data-aos="fade-right">
							   <img src="images/banner-img.png" class="img-fluid" alt="banner">
							</figure>
						</div>
						<div class="col-md-6 col-sm-6">
						
							<!-- Nav tabs -->
							<ul class="nav nav-tabs login-tab" data-aos="fade-left">
							<?php if($userType == 1) { ?>
								<li class="nav-item">
									<a class="nav-link active navtrigger" data-trigger-text="login" data-toggle="tab" href="#home">Login</a>
								</li>
								
									<li class="nav-item">
										<a class="nav-link navtrigger" data-trigger-text="signup" data-toggle="tab" href="#menu1">Sign up for free</a>
									</li>
								<?php } ?>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content" data-aos="fade-left">
								<div class="tab-pane active" id="home">
									<div class="login-panel">
										<div class="login-error-msg"></div>
										<form method="post" id="login-form" action="{{ route('login') }}">
											{{ csrf_field() }}
											
											@if ($errors->has('email'))
												<span class="help-block login-help-block">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
											@endif
											<strong id="email" class="error-ajax"></strong>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-envelope"></i></span>
												</div>
												<input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email Address" required autofocus>
												
											</div>
											<?php if(isset($userType)) { ?>
												<input type="hidden" name="type" value="<?php echo $userType; ?>" class="form-control" >
												<input type="hidden" name="logintype" value="<?php echo $logintype; ?>" class="form-control" >
											<?php } ?>
											@if ($errors->has('password'))
												<span class="help-block login-help-block">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
											@endif
											<strong id="password" class="error-ajax"></strong>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-lock"></i></span>
												</div>
												<input type="password" class="form-control" name="password" placeholder="Password" required>
											</div>
											<div class="input-group">
												<input type="submit" class="login-btn form-control" value="Login"  >
											</div>
											  
											<div class="form-check">
												<label class="form-check-label">
													<input type="checkbox" class="form-check-input" name="remember" value="">Remember me
												</label>
												<a href="" class="text-right">Forgot password ?</a>
											</div>
										</form>
									</div>
								</div>
								<?php if($userType == 1) { ?>
								<div class="tab-pane fade" id="menu1">
									<div class="login-panel">
										<div class="register-error-msg"></div>
										<form method="POST" action="{{ route('register') }}" id="register-form">
											{{ csrf_field() }}
											
											@if ($errors->has('name'))
												<span class="help-block registerform-help-block">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
											@endif											
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-user"></i></span>
												</div>
												<input value="{{ old('name') }}" name="name" type="text" class="form-control" placeholder="User Name"required>
											</div>
											
											@if ($errors->has('email'))
												<span class="help-block registerform-help-block">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
											@endif
											<strong id="email" class="error-ajax"></strong>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-envelope"></i></span>
												</div>
												<input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email Address" required autofocus>
											</div>
										
											@if ($errors->has('password'))
												<span class="help-block registerform-help-block">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
											@endif
											<strong id="password" class="error-ajax"></strong>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-lock"></i></span>
												</div>
												<input type="password" name="password" class="form-control" placeholder="Password" required>
											</div>
											
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-lock"></i></span>
												</div>
												<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
											</div>
											
											<div class="input-group">
												<input type="submit" class="login-btn form-control" value="Signup">
											</div>
										</form>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
			  </div>
			</div>
		</div>
	</header>
	
  @yield('content')
  
	<footer>
        <div class="footer-sec">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<ul class="footer-links">
							 <li><a href="">Home</a></li>
							 <li><a href="">Features</a></li>
							 <li><a href="">Support</a></li>
							 <li><a href="">Contact</a></li>
							 <li><a href="">Terms of Use</a></li>
							 <li><a href="">Privacy Policy</a></li>
						</ul>
						<ul class="f-images">
							<li><a href=""><img src="images/crazy.png" class="img-fluid" alt="images"></a></li>
							<li><a href=""><img src="images/shippo.png" class="img-fluid" alt="images"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="text-foot">
            <p>© Shop Master 2018 All Rights Reserved</p>
        </div>
    </footer>
	
    
<!-- All Jquery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!--<script type="text/javascript" src="{{ asset('css/home/js/jquery-1.11.3.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<!--<script type="text/javascript" src="{{ asset('css/home/js/jquery-ui.min.1.9.2.js') }}"></script>-->

<script type="text/javascript" src="{{ asset('css/home/js/slick.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('css/home/js/swiper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('css/home/js/aos.js') }}"></script>
<script type="text/javascript" src="{{ asset('css/home/js/custom-scripts.js') }}"></script>

<script type="text/javascript" src="{{ asset('css/home/js/bootstrap.min.js') }}"></script>

@stack('js')

<script>
	$(document).ready(function() {
		$("body").on('click', '.navtrigger', function() {
			var text = $(this).attr('data-trigger-text');
			if(text == 'signup' && !$('#menu1').hasClass('active')) {
				$('.registerform-help-block').remove();
			} else if(text == 'login' && !$('#home').hasClass('active')) {
				$('.login-help-block').remove();
			}
		});
		
		$('#login-form').submit(function(e) {
			var data = $('#login-form').serialize();
			$(".error-ajax").html('');
			$.ajax({
				type: "POST",
				url: $('#login-form').attr('action'),
				data: data,
				dataType: "json",
				success: function (response) {
					location.reload();
				},
				error: function (response) {
					$('.login-error-msg').html('');
					var msg = '';
					if(response.status == 422 || response.status == 423) {
						$.each(response.responseJSON, function( index, value ) {
							console.log(index+' '+value);
							msg += value+'<br>';
							//$("#login-form #"+index).html(value);;
						});
						if(msg != '') {
							$('.login-error-msg').html('<div class="alert alert-danger"><strong id="name">'+msg+'</strong></div>');
						}
					} else if(response.status == 200) {
						location.reload();
					}
				}
			});
			return false;
		});
		
		$('#register-form').submit(function(e){
            e.preventDefault();
			var data = $('#register-form').serialize();
            $(".error-ajax").html('');
			$.ajax({
				type: "POST",
				url: $(this).attr('action'),
				data: data,
				dataType: "json",
				success: function (response) {
				   if(response.status == true) {
					  location.reload();
				   }
				},
				error: function (response) {
					$('.register-error-msg').html('');
					var msg = '';
					$.each(response.responseJSON, function( index, value ) {
						//$("#register-form #"+index).html(value);
						msg += value+'<br>';
					});
					if(msg != '') {
						$('.register-error-msg').html('<div class="alert alert-danger"><strong id="name">'+msg+'</strong></div>');
					}
					
				}
			});
        });
	});
</script>
	
</body>
</html>