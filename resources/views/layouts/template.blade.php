<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="base_url" content="{{ URL::to('/') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="icon" type="image/ico" href="{{ URL::to('/public/images/favicon.ico') }}">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
	
	<link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap/style.css') }}" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">-->
	<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
	@stack('css')
</head>
<body class="fix-header fix-sidebar">
	<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
	<div id="main-wrapper">
		<!-- header header  -->
		<div class="page-wrappers">
			@yield('content')
		</div>
	</div>
    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
	
	 <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ asset('js/bootstrap/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <!-- <script src="js/popper.min.js"></script> -->
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('js/bootstrap/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('js/bootstrap/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('js/bootstrap/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('js/bootstrap/scripts.js') }}"></script>
	
	@stack('js')
	
</body>
</html>