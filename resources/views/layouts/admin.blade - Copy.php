<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head base_url="{{ URL::to('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="base_url" content="{{ URL::to('/') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
	<link href="{{ asset('public/admin/css/metisMenu.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/timeline.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/startmin.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/morris.css') }}" rel="stylesheet">
	<link href="{{ asset('public/admin/css/font-awesome.min.css') }}" rel="stylesheet">
	@stack('styles')
	<?php /* ?><link href="{{ asset('css/admin.css') }}" rel="stylesheet"><?php */ ?>
</head>
<body baseurl="<?php echo url(''); ?>">
    <div id="wrapper">
	
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href=""></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<?php /* ?>
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="#"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>
				<?php */ ?>
                <ul class="nav navbar-right navbar-top-links">
					<?php /* ?>
                    <li class="dropdown navbar-inverse">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
					<?php */ ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
							<?php /* ?>
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
							
                            <li class="divider"></li>
                            <?php */ ?>
							<li>
								<a href="{{ route('logout') }}"
									onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
                            </li>
                        </ul>
                    </li>
                </ul>
               
				
                @include('admin.leftpanel')
        </nav>
		
        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('public/admin/js/metisMenu.min.js') }}"></script>
	<script src="{{ asset('public/admin/js/raphael.min.js') }}"></script>
	
	<script src="{{ asset('public/admin/js/dataTables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('public/admin/js/dataTables/dataTables.bootstrap.min.js') }}"></script>
	
	<script src="{{ asset('public/admin/js/metisMenu.min.js') }}"></script>
	<script src="{{ asset('public/admin/js/startmin.js') }}"></script>
	@stack('js')
	@stack('scripts')
	<script>
		$(document).ready(function() {
			$('#dataTables-example').DataTable({
				responsive: true,
				"aoColumnDefs" : [ {
					"bSortable" : false,
					"aTargets" : [ "sorting_disabled" ]
				} ]
			});
		});
	</script>
</body>
</html>
