<div class="left-sidebar">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav active">
			<ul id="sidebarnav">
				<li class="nav-devider"></li>
				<!-- <li class="nav-label">Home</li> -->
				 <li class=""> <a  href="{{ url('/dashboard') }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a></li>
			 <!--   2nd menu -->

				<li class="nav-label">Order Mananger</li>
				 <li> <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">All Orders<span class="label label-rouded label-primary pull-right">2</span></span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="{{ url('/order/index') }}">Recently Orders </a></li>
					</ul>
				</li>

				 <li> <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Processing Orders</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/order/index/ordered') }}">Awaiting Payment
						<span class="label label-rouded label-primary pull-right">2</span>
					</a></li>
						<li><a href="{{ url('/order/index/paid') }}">Ready to Ship
						<span class="label label-rouded label-primary pull-right">2</span>
					</a></li>
						<li><a href="{{ url('/order/index/shipped') }}">Shipped
						<span class="label label-rouded label-primary pull-right">2</span>
					</a></li>

					</ul>
				</li>

				 <li> <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Invalid Orders</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/order/index/refund') }}">Canceled
						<span class="label label-rouded label-primary pull-right">2</span>
					</a></li>
						<li><a href="{{ url('/order/index/shelve') }}">Voided
						<span class="label label-rouded label-primary pull-right">2</span>
					</a></li>
					</ul>
				</li>

				<!-- 3rd menu-->
				 <li class="nav-label">Inventory</li>
				 <li> <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Product Sourcing<span class="label label-rouded label-primary pull-right">2</span></span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="{{ url('/inventory/scraper') }}">Product Scraper
						<!-- <span class="label label-rouded label-primary pull-right">HoT</span> -->
						</a></li>
					</ul>
				</li>

				 <li> <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Product Manager</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/inventory/product') }}">Products
						<!-- <span class="label label-rouded label-primary pull-right">2</span> -->
					</a></li>
						<li><a href="{{ url('/inventory/variation') }}">Variations
						<!-- <span class="label label-rouded label-primary pull-right">2</span> -->
					</a></li>
					</ul>
				</li>

				 <li> <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Preferences</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/category/inventory') }}">Product Categories
					</a></li>
					</ul>
				</li>


				  <!--  4th menu -->
				<?php /*  ?>
				<li class="nav-label">Listing Manager</li>
				 <li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">shopify<span class="label label-rouded label-primary pull-right">2</span></span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="{{ url('/shopify/product/shopifyActive/active') }}">Active
						<!-- <span class="label label-rouded label-primary pull-right">1</span> -->
						</a></li>
						<li><a href="{{ url('/shopify/product/shopifyDraft/draft') }}">Draft
						<!-- <span class="label label-rouded label-primary pull-right">1</span> -->
						</a></li>
						<li><a href="{{ url('/shopify/product/shopifyHidden/hidden') }}">Hidden
						<!-- <span class="label label-rouded label-primary pull-right">1</span> -->
						</a></li>
					</ul>
				</li>

				<li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">General Templates</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/user/templates/commSizeChart/sizeChart') }}">Size
						<!-- <span class="label label-rouded label-primary pull-right">2</span> -->
					</a></li>
					</ul>
				</li>
				<?php  */ ?>
				<li class="nav-label">eBay</li>
				 <li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">eBay Templates</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><!--<a href="{{ url('/user/templates/eBayDescription/eBayDescription') }}">-->
						<a href="{{ url('/templates/templates') }}">Templates
					</a></li>
					</ul>
				</li>
				<!--
				<li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">eBay Presets<span class="label label-rouded label-primary pull-right">2</span></span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="{{ url('/user/templates/eBayListing/eBayListing') }}"> Listing Preset
						</a></li>
						<li><a href="{{ url('/user/templates/eBayPayment/eBayPayment') }}">Payment Policy
						</a></li>
						<li><a href="{{ url('/user/templates/eBayShipping/eBayShipping') }}">Shipping Policy
						</a></li>
						<li><a href="{{ url('/user/templates/eBayReturn/eBayReturn') }}">Return Policy
						</a></li>
					</ul>
				</li>
				-->
				<!-- 5th menu -->
				<?php /* ?>
				<li class="nav-label">eBay</li>
				<li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-envelope"></i><span class="hide-menu">Messages<span class="label label-rouded label-primary pull-right">2</span></span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="{{ url('/ebay/message/inbox') }}">inbox
						</a></li>
						<li><a href="{{ url('/ebay/message/send') }}">Send
						</a></li>
					</ul>
				</li>

				 <li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Message Assistant</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/ebay/message/boats') }}">Message Bots
						<!-- <span class="label label-rouded label-primary pull-right">2</span> -->
					</a></li>
					</ul>
				</li>
				<?php */ ?>
				 <!--6th menu -->
				 <li class="nav-label">Images</li>
				 <li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">My Images<span class="label label-rouded label-primary pull-right">2</span></span></a>
					<ul aria-expanded="false" class="collapse">
					<li><a href="{{ url('/image/index') }}">My Images
						</a></li>
						<!--<li><a href="{{ url('/album/manage') }}">Manage Images
						</a></li>
						<!--<li><a href="{{ url('/album/upload') }}">Upload Images
						</a></li>-->
					</ul>
				</li>

				 <li> <a class="has-arrow  " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Perferences</span></a>
					<ul aria-expanded="false" class="collapse nav-inner-val">
						<li><a href="{{ url('/category/image') }}">Image Categories
					</a></li>
					</ul>
				</li>
			</ul>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</div>