@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Templates</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->

<div class="product-sec-page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="inven_prod">
                                <ul class="breadcrumbs">
                                    <li>
										Templates & Presets
										<?php if($params == 'commSizeChart' && $params2 == 'sizeChart') { ?>
											- General Templates - Size Chart
										<?php } else if($params == 'eBayDescription' && $params2 == 'eBayDescription') { ?>
											- eBay Templates - Seller Descriptions
										<?php } else if($params == 'eBayListing' && $params2 == 'eBayListing') { ?>
											- eBay Presets - Listing Preset
										<?php } else if($params == 'eBayPayment' && $params2 == 'eBayPayment') { ?>
											- eBay Presets - Payment Policy
										<?php } else if($params == 'eBayShipping' && $params2 == 'eBayShipping') { ?>
											- eBay Presets - Shipping Policy
										<?php } else if($params == 'eBayReturn' && $params2 == 'eBayReturn') { ?>
											- eBay Presets - Return Policy
										<?php } ?>
									</li>
                                   
                                </ul>
                            </div>

                            <!--table-part-->
                            
                            <div class="listing_crete">
                                <ul>
                                    <li>
                                        <a href="#">
                                          Delete
                                      </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="import_export">
                                <ul>

                                    <li class="aad_prodt"><a href="#">Add Template</a> </li>
                                </ul>
                            </div>
                            <div class="suppliers_sec">
                                
                            </div>
                            
                            <div class="parent_sku">
                                <ul>
                                    <li style="">
                                        <a href="#">
                                            <select>
                                                <option>Parents SKU</option>
                                                <option>SkU Aliases</option>
                                                <option>Product Name</option>
                                                <option>Produt Identifiers</option>
                                            </select>
                                        </a>
                                    </li>
                                    <li><input type="text" placeholder="Search" style="">  </li>
                                </ul>
                            </div>
                            <!--table-part-->

                                    <div class="list_contentlist">
                                        <table class="alonetable">
                                            <tr>
                                                <th class="input_check"><input type="checkbox"> </th>
                                                <th class="image_left">Nickname</th>
                                                <th class="name_left">Tags</th>
                                                <th class=" modified_right">Modified</th>
                                                <th class="variation_right">Action</th>

                                            </tr>

                                        </table>
										
                                        <table class="theme-tablebackgrnd">
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left">Name</td>
                                                <td class="name_left">
													Tags name
                                                </td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6>2018-07-03</h6>
                                                    <p>Last Modified</p>
                                                    <h6>2018-07-03</h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>

                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                            <!--pagination-part-->

                            

                        </div>
                    </div>
                </div>
            </div>
@endsection
