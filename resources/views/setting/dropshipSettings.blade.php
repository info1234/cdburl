@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Dashboard</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
	<!-- Start Page Content -->
	<div class="row">
	   
		<div class="col-md-2">
			<div class="card p-10">
				<div class="media justify-content-center">
					<a href="{{ url('/inventory/scraper') }}">
						<div class="media-left meida media-middle text-center">
							<span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
							  <h5 class="p-t-10">Import Products</h5>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="card p-10">
				<div class="media justify-content-center">
					<a href="{{ url('/inventory/scraper/importlist') }}">
						<div class="media-left meida media-middle text-center">
							<span><i class="fa fa-archive f-s-40 color-warning"></i></span>
							  <h5 class="p-t-10">Create Listing</h5>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="card p-10">
				<div class="media justify-content-center">
					<div class="media-left meida media-middle text-center">
						<span><i class="fa fa-user f-s-40 color-danger"></i></span>
						 <h5 class="p-t-10">Publish Listing</h5>
					</div>
				  
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="card p-10">
				<div class="media justify-content-center">
					<div class="media-left meida media-middle text-center">
						<span><i class="fa fa-user f-s-40 color-danger"></i></span>
						 <h5 class="p-t-10">Connect Store</h5>
					</div>
				   
				</div>
			</div>
		</div>

		<div class="col-md-2">
			<div class="card p-10">
				<div class="media justify-content-center">
					<div class="media-left meida media-middle text-center">
						<span><i class="fa fa-archive f-s-40 color-warning"></i></span>
						  <h5 class="p-t-10">Add Preset</h5>
					</div>
				   
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="card p-10">
				<div class="media ">
					<div class="media-left meida media-middle text-center">
						<span><i class="fa fa-user f-s-40 color-danger"></i></span>
						 <h5 class="p-t-10">Dropship Settings</h5>
					</div>
					   
				</div>
			</div>
		</div>
	</div>

	<div class="row bg-white m-l-0 m-r-0 box-shadow ">

		<!-- column -->
	   
		<!-- column -->

		<!-- column -->
	   
		<!-- column -->
	</div>
  

	<div class="row">
		<div class="col-lg-8">
			<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-title">
						<h4 class="secondry-colr">Sales</h4>
					</div>
					<div class="recent-comment">
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media no-border">
							<div class="media-body">
								<h4 class="media-heading">Mr. Michael</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /javascript:void(0) card -->
			</div>
			<!-- /javascript:void(0) column -->
			<div class="col-lg-6">
				<div class="card">
					<div class="card-title">
						<h4 class="secondry-colr">Orders</h4>
					</div>
					<div class="recent-comment">
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media no-border">
							<div class="media-body">
								<h4 class="media-heading">Mr. Michael</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /javascript:void(0) card -->
			</div>


			</div>
		</div>

	   <div class="col-lg-4">
				<div class="card">
					<div class="card-title">
						<h4 class="secondry-colr">Products</h4>
					</div>
					<div class="recent-comment">
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media no-border">
							<div class="media-body">
								<h4 class="media-heading">Mr. Michael</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /javascript:void(0) card -->
			</div>

	</div>

	  <div class="row">
		<div class="col-lg-8">
			<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-title">
						<h4 class="secondry-colr">Listings</h4>
					</div>
					<div class="recent-comment">
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media no-border">
							<div class="media-body">
								<h4 class="media-heading">Mr. Michael</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /javascript:void(0) card -->
			</div>
			<!-- /javascript:void(0) column -->
			<div class="col-lg-6">
				<div class="card">
					<div class="card-title">
						<h4 class="secondry-colr">Notifications</h4>
					</div>
					<div class="recent-comment">
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media no-border">
							<div class="media-body">
								<h4 class="media-heading">Mr. Michael</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /javascript:void(0) card -->
			</div>


			</div>
		</div>

	   <div class="col-lg-4">
				<div class="card">
					<div class="card-title">
						<h4 class="secondry-colr">FAQs</h4>
						<a href="" class="float-right secondry-colr">Support Center </a>
					</div>
					<div class="recent-comment">
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">john doe</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
						<div class="media no-border">
							<div class="media-body">
								<h4 class="media-heading">Mr. Michael</h4>
								<p>Cras sit amet nibh libero, in gravida nulla. </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /javascript:void(0) card -->
			</div>

	</div>


	<!-- End PAge Content -->
</div>
<!-- End Container fluid  -->

@endsection
