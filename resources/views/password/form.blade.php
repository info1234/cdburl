@extends('layouts.app')

@section('content')

	<!-- Bread crumb -->
	<div class="row page-titles primary-bg-color">
		<div class="col-md-5 align-self-center">
			<h3 class="text-primary white-color">My Account / Change Password</h3>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="flash-message">
								@foreach (['danger', 'warning', 'success', 'info'] as $msg)
								  @if(Session::has('alert-' . $msg))
									<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
								  @endif
								@endforeach
							 </div>
		  
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul style="list-style: none;">
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form class="form-horizontal form-material" action="{{ route('changePassword') }}" method="post">
								{{csrf_field()}}
								<div class="form-group">
									<label class="col-md-12">Current Password</label>
									<div class="col-md-12">
										<input name="password" class="form-control form-control-line" type="password" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">New Password</label>
									<div class="col-md-12">
										<input name="newPassword" class="form-control form-control-line" type="password" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Confirm New Password</label>
									<div class="col-md-12">
										<input name="confirmNewPassword" class="form-control form-control-line" type="password" required>
									</div>
								</div>
								<div class="col-md-12">
									<button type="submit" class="save-changes btn">Save Changes</button>
								</div>
							</form>
						</div>
						<!-- /javascript:void(0) card -->
					</div>
				</div>
			</div>
		</div>
		<!-- End PAge Content -->
	</div>

@endsection
