@extends('layouts.app')

@section('content')

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Template</h3> </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->

<div class="product-sec-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				
			<form id="template-form" action="<?php echo url('templates/add'); ?>" enctype="multipart/form-data" method="POST">
				<!--table-part-->
				<div  class="tab-content">
					<div class="tab-pane active" id="tab_a">
						<div class="quick_fill">							
							<h5>Template</h5>
						   <div class="form-group">
								<label class="col-md-12">Template</label>
								<div class="col-md-12">
									<select class="template">
										<option value="1" <?php if($template != null && $template->template_id == 1) { ?>selected<?php } ?>>Default Templates</option>
										<option value="2" <?php if($template != null && $template->template_id == 2) { ?>selected<?php } ?>>New Template</option>
									</select>
								</div>
								<!--
								<div class="col-md-6">
									<textarea class="form-control"></textarea>
								</div>
								--><a class="float-right" style="margin-right:10px" target="_blank" href="<?php echo url('template/preview'); ?>">Preview</a>
							</div>
							
							 <div class="form-group displaytemplate">
								<?php if($template == null) { ?>
									@include('templates/templateStatic1', [])
								<?php } else if($template != null && $template->template_id == 1) { ?>
										@include('templates/templateStatic1', [])
								<?php } else if($template != null && $template->template_id == 2) { ?>
										@include('templates/templateStatic2', [])
								<?php } ?>
							</div>
						</div>
						
						
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



@endsection
@push('css')
@endpush
@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		/* $("body").on('click', '.template', function() {
			if($(this).val() == 2) {
				$('.template_content').removeClass('hide');
			} else {
				$('.template_content').addClass('hide');
			}
		});
		$("#txtEditor").Editor(); */
		$(".template").on('change', function() {
			var val = $(this).val();
			if(val != '') {
				var url = document.getElementsByName('base_url')[0].getAttribute('content');
				url = url+'/template/change';
				
				$.ajax({
					type: "post",
					url: url,
					data: {val: val},
					dataType: "html",
					success: function (response) {
					   $('.displaytemplate').html(response);
					}
				});
			}
		});
	});
</script>
@endpush