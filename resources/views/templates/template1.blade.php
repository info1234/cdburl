@extends('layouts.template')

@section('content')

<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-sec-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inven_prod">
					<ul class="breadcrumb">
						<li><a class="breadcrumb-item" href="#"><?php echo $product->name; ?></a></li>
					</ul>
				</div>

				<!--table-part-->
				<div class="quick_fill">
					<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row" id="gradient">
                <div class="col-md-4">
                    <?php if($product->image != null) { ?><img src="<?php echo $product->image; ?>" class="img-responsive"><?php } ?>
                </div>
                <div class="col-md-8" id="overview">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <ul class="pb-product-details-ul">
                                <li>Title: <?php echo $product->name; ?></li>
                                <li>Category: <?php echo $product->catname; ?></li>
                                <li>Price: $<?php echo $product->price; ?></li>
                                <li>Quantity: <?php echo $product->quantity; ?></li>
                            </ul>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="tabs_div">
                    <ul>
                        <li>Identifier</li>
                        <li>Description</li>
                    </ul>
                    <div>
                        <table class="table">
                            <tbody>
                               <?php $identifierByProduct = getIdentifierByProduct($product->id); ?>
							   <?php foreach($identifierByProduct as $obj) { ?>
								<tr>
                                    <td class="success"><?php echo $obj['product_Identifier'] ?>: </td>
									 <td class="success"><?php echo $obj['product_Identifier_value'] ?> </td>
                                </tr>
							   <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="success"><?php echo $product->description; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-glow/all.min.css" />
<style>
    .pb-product-details-ul {
        list-style-type: none;
        padding-left: 0;
        color: black;
    }

        .pb-product-details-ul > li {
            padding-bottom: 5px;
            font-size: 15px;
        }

    

    #hits {
        border-right: 1px solid white;
        border-left: 1px solid white;
        vertical-align: middle;
        padding-top: 15px;
        font-size: 17px;
        color: white;
    }

    #fan {
        vertical-align: middle;
        padding-top: 15px;
        font-size: 17px;
        color: white;
    }

    .pb-product-details-fa > span {
        padding-top: 20px;
    }
</style>
@endpush

@push('js')
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript">
$(document).ready( function () {
	 $(".tabs_div").shieldTabs();
});
</script>
@endpush