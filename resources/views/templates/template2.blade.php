@extends('layouts.template')

@section('content')

<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container">
	<br>  <p class="text-center"><?php echo $product->name; ?></a></p>
<hr>

	
<div class="card">
	<div class="row">
		<aside class="col-sm-5 border-right">
<article class="gallery-wrap"> 
<div class="img-big-wrap">
  <div> <a href="#"><img src="<?php echo $product->image; ?>"></a></div>
</div> <!-- slider-product.// -->
<!--<div class="img-small-wrap">
  <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
  <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
  <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
  <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
</div>--> <!-- slider-nav.// -->
</article> <!-- gallery-wrap .end// -->
		</aside>
		<aside class="col-sm-7">
<article class="card-body p-5">
	<h3 class="title mb-3"><?php echo $product->name; ?></h3><br><br>

<p class="price-detail-wrap"> 
	<span class="price h3 text-warning"> 
		<span class="currency">$<?php echo $product->price; ?></span>
	</span> 
</p> <!-- price-detail-wrap .// -->
<dl class="item-property">
  <dt>Description</dt>
  <dd><p><?php echo $product->description; ?></p></dd>
</dl>
<dl class="param param-feature">
  <dt>Category</dt>
  <dd><?php echo $product->catname; ?></dd>
</dl>  <!-- item-property-hor .// -->
<dl class="param param-feature">
  <dt>Quantity</dt>
  <dd><?php echo $product->quantity; ?></dd>
</dl>  <!-- item-property-hor .// -->
<dl class="param param-feature">
  <dt>Identifier</dt>
  <?php $identifierByProduct = getIdentifierByProduct($product->id); ?>
  <?php foreach($identifierByProduct as $obj) { ?>
	
	<dd><?php echo $obj['product_Identifier'] ?>: <?php echo $obj['product_Identifier_value'] ?></dd>
  <?php } ?>
</dl>  <!-- item-property-hor .// -->

<hr>
</article> <!-- card-body.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</div> <!-- card.// -->


</div>
<!--container.//-->


<br><br><br>
@endsection

@push('css')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

@endpush

@push('js')

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endpush