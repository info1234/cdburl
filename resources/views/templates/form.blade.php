@extends('layouts.app')

@section('content')

<?php

?>

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Template</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->

<div class="product-sec-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				
			<form id="template-form" action="<?php echo url('templates/add'); ?>" enctype="multipart/form-data" method="POST">
				<!--table-part-->
				<div  class="tab-content">
					<div class="tab-pane active" id="tab_a">
						<div class="quick_fill">
							<div class="flash-message">
								@foreach (['danger', 'warning', 'success', 'info'] as $msg)
								  @if(Session::has('alert-' . $msg))
									<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
								  @endif
								@endforeach
							 </div>
							 
							@if ($errors->any())
								<div class="form-group">
									<div class="alert alert-danger">
										<ul style="list-style: none;">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								</div>
							@endif
							
							<h5>Template Information</h5>
						   <div class="form-group">
								<label class="col-md-12">Name*</label>
								<div class="col-md-12">
									<input name="sku" value="<?php  ?>" id="name" class="form-control form-control-line" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label for="example-name" class="col-md-12">Tag	</label>
								<div class="col-md-12">
									<input type="text" class="form-control form-control-line" value="<?php //echo $name; ?>" id="product_name" name="tag">
								</div>
							</div>
							
						</div>
						
						<div class="quick_fill">
							<h5>Template Setting</h5>
						   <div class="form-group">
								<label class="col-md-12">Template*</label>
								<div class="col-md-12">
									<!--<input name="template"  class="template" value="1" checked type="radio"> Pre-designed Template <a class="descriptionAdd" href="javascript:" data-name="pre" data-type="templates">Browser Templates</a>-->
									<br><input name="template" value="2" class="template" type="radio"> Custom Template
								</div>
								
								<div class="template_content hidex">
									<hr>
									<!--<div class="dropdown pull-right open" id="customAddTag">
										<select>
											<option></option>
										</select>
                                                <button data-toggle="dropdown" class="dropdown-toggle btn btn_sm_white" aria-expanded="true"><span class="sm_icon dp18">code</span>Insert listing content</button>
                                                <ul class="dropdown-menu pull-right z_600">
                                                    <li><a href="javascript:" data-name="listingTitle">Listing Title<span class="sm_icon dp18">add</span></a></li>
                                                    
                                                    <li><a href="javascript:" data-name="videoLink">Video Link<span class="sm_icon dp18">add</span></a></li>
                                                    <li><a href="javascript:" data-name="img">Image (Max to 20)<span class="sm_icon dp18">add</span></a></li>
                                                    <li><a href="javascript:" data-name="itemSpecifics">Item Specifics<span class="sm_icon dp18">add</span></a></li>
                                                    <li><a href="javascript:" data-name="desktopDescription">Desktop Description<span class="sm_icon dp18">add</span></a></li>
                                                    <li><a href="javascript:" data-name="moblieDescription">Mobile Description<span class="sm_icon dp18">add</span></a></li>
                                                    
                                                </ul>
                                            </div>-->
									
									<label class="col-md-12">Template Content</label>
									<div class="col-md-12">
										<textarea class="form-control" id="txtEditor" name="description"></textarea>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="save-changes btn">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('css/editor.css') }}">
@endpush
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/editor.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").on('click', '.template', function() {
			if($(this).val() == 2) {
				$('.template_content').removeClass('hide');
			} else {
				$('.template_content').addClass('hide');
			}
		});
		$("#txtEditor").Editor();
		
	});
</script>
@endpush