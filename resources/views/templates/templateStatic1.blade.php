<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-sec-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inven_prod">
					<ul class="breadcrumb">
						<li><a class="breadcrumb-item" href="#">Samsung A345</a></li>
					</ul>
				</div>

				<!--table-part-->
				<div class="quick_fill">
					<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row" id="gradient">
                <div class="col-md-4">
                    <img src="http://www.prepbootstrap.com/Content/images/shared/misc/s7e.png" class="img-responsive">
                </div>
                <div class="col-md-8" id="overview">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <ul class="pb-product-details-ul">
                                <li>Title: Samsung A345</li>
                                <li>Category: samsung</li>
                                <li>Price: $500</li>
                                <li>Quantity: 1</li>
                            </ul>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="tabs_div">
                    <ul>
                        <li>Identifier</li>
                        <li>Description</li>
                    </ul>
                    <div>
                        <table class="table">
                            <tbody>
								<tr>
                                    <td class="success">ASIN: </td>
									 <td class="success">BQ574ATWE </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="success">
										16MP front facing camera with low light mode and LED flash Unlock your phone by simply letting the camera see your face
									</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-glow/all.min.css" />

<style>
    .pb-product-details-ul {
        list-style-type: none;
        padding-left: 0;
        color: black;
    }

        .pb-product-details-ul > li {
            padding-bottom: 5px;
            font-size: 15px;
        }

   

    #hits {
        border-right: 1px solid white;
        border-left: 1px solid white;
        vertical-align: middle;
        padding-top: 15px;
        font-size: 17px;
        color: white;
    }

    #fan {
        vertical-align: middle;
        padding-top: 15px;
        font-size: 17px;
        color: white;
    }

    .pb-product-details-fa > span {
        padding-top: 20px;
    }
</style>

<script src="{{ asset('js/bootstrap/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/scripts.js') }}"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $(".tabs_div").shieldTabs();
    });
</script>
