@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Images</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-inventory-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inventory-prod">
					<div class="tory-left">
						<ul class="breadcrumb">
							<li><a class="breadcrumb-item" href="#"><strong>Image</strong></a></li>
						</ul>

					</div>
					<br><br>
					<!--table-part-->
					<div class="container">
						<?php foreach($images as $imageObj) { ?>
							<div class="row">
								<?php foreach($imageObj as $obj) {
										if($obj['image_upload_type'] == 1) {
											$image = $obj['image'];
										} else {
											$image = 'images/product/'.$obj['image'];
										}
								?>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
									<img src="<?php echo $image; ?>" style="height:180px;" class="img-responsive">
								</div>
								
								<?php } ?>
							</div><br>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Container fluid  -->

@endsection

@push('css')
<style>


</style>
@endpush
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script>
$(document).ready(function() {
	
});

</script>
@endpush