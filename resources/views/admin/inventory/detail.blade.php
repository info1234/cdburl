@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <div class="row">
            
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
					<h4 class="card-title">Product Detail</h4>
                  
                    <div class="form-group">
						<label for="exampleInputName1"><strong>Name</strong></label>
						<p><?php echo $inventory['name']; ?></p>
                    </div>
					
					<div class="form-group">
						<label for="exampleInputName1"><strong>Category</strong></label>
						<p><?php echo $inventory['categoryname']; ?></p>
                    </div>
					
					<div class="form-group">
						<label for="exampleInputName1"><strong>Price</strong></label>
						<p><?php echo $inventory['price']; ?></p>
                    </div>
					
					<div class="form-group">
						<label for="exampleInputName1"><strong>Product Quantity</strong></label>
						<p><?php echo $inventory['quantity']; ?></p>
                    </div>
					
					<?php if(getImageByProductId($inventory['id'])->count() > 0) { ?>
						<div class="form-group">
							<label for="exampleInputName1"><strong>Images</strong></label>
							<p>
								<?php
									
									foreach(getImageByProductId($inventory['id']) as $imageObj) {
										if($imageObj['image_upload_type'] == 1) {
											$image = $imageObj['image'];
										} else {
											$image = 'images/product/'.$imageObj['image'];
										}
										
									?>
										<img src="{{asset($image)}}" height="80" width="100" style="border:1px solid black">
									<?php } ?>
							</p>
						</div>
					<?php } ?>
					
					<?php if(getIdentifierByProduct($inventory['id'])->count() > 0) { ?>
						<div class="form-group">
							<label for="exampleInputName1"><strong>Product Identifier</strong></label>
							<div class="row">
								<div class="col-md-4">
									<strong>Product Identifier</strong>
								</div>
								<div class="col-md-4">
									<strong>Value</strong>
								</div>
							</div>
							<?php foreach(getIdentifierByProduct($inventory['id']) as $identifierObj) {								
							?>
								<div class="row">
									<div class="col-md-4">
										<?php echo $identifierObj['product_Identifier']; ?>
									</div>
									<div class="col-md-4">
										<?php echo $identifierObj['product_Identifier_value']; ?>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
					
					<?php if(getVariantsByProductId($inventory['id'])->count() > 0) { ?>
					<div class="form-group">
						<label for="exampleInputName1"><strong>Variation</strong></label>
						<div class="row">
							<div class="col-md-4">
								<strong>Name</strong>
							</div>
							<div class="col-md-4">
								<strong>Value</strong>
							</div>
						</div>
						<?php foreach(getVariantsByProductId($inventory['id']) as $variantObj) {								
							?>
							<div class="row">
								<div class="col-md-4">
									<?php echo $variantObj['name']; ?>
								</div>
								<div class="col-md-4">
									<?php echo $variantObj['value']; ?>
								</div>
							</div>
						<?php } ?>
					</div>
					<?php } ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

@endsection