@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-lg-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
				  <h4 class="card-title">Inventory</h4>
				  <div class="table-responsive">
					<table class="table table-striped">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Image</th>
						  <th>Name</th>
						  <th>Category</th>
						  <th>Brand</th>
						  <th>Price</th>
						  <th>Quantity</th>
						  <th>Cost</th>
						  <th>Action</th>
						</tr>
					  </thead>
					  <tbody>
						<?php foreach($inventory as $inventoryObj) { ?>
							<tr>
								<td class="py-1"><?php echo $inventoryObj['id']; ?></td>
								<td>
									<?php if($inventoryObj['image'] != null) {
											if($inventoryObj['image_upload_type'] == 1) {
												$image = $inventoryObj['image'];
											} else {
												$image = 'images/product/'.$inventoryObj['image'];
											}
											
									?>
										<img src="{{asset($image)}}">
									<?php } ?>
								</td>
								<td><?php echo custom_echo($inventoryObj['name'], 50); ?></td>
								<td><?php echo $inventoryObj['categoryname']; ?></td>
								<td><?php echo $inventoryObj['brand']; ?></td>
								<td><?php echo $inventoryObj['price']; ?></td>
								<td><?php echo $inventoryObj['quantity']; ?></td>
								<td><?php echo $inventoryObj['unit_cost']; ?></td>
								<td>
									<a href="<?php echo url('admin/inventory/detail/'.$inventoryObj['id']); ?>">
										<i class="menu-icon mdi mdi-restart"></i>
									</a>
								</td>
							</tr>
						<?php } ?>
					  </tbody>
					</table>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection