@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
          <div class="row">
            
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Users</h4>
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Mobile</th>
						  <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php foreach($users as $userObj) { ?>
							<tr>
								<td class="py-1"><?php echo $userObj['id']; ?></td>
								<td><?php echo $userObj['name']; ?></td>
								<td><?php echo $userObj['email']; ?></td>
								<td><?php echo $userObj['mobile']; ?></td>
								<td>
									<a href="<?php echo url('admin/inventorylisting/'.$userObj['id']); ?>"><i class="menu-icon mdi mdi-restart"></i></a>
								</td>
							</tr>
						<?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
           
          </div>
        </div>


@endsection