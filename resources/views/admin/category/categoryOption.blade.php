<ul class="dropdown-menu multi-level">
	<?php foreach($categories as $categoryObj) {
			if($categoryObj->pid == null) {
	?>
			<li><a href="#" data-cat="0" categoryid="<?php echo $categoryObj->id; ?>" class="categoryOption"><?php echo $categoryObj->name; ?></a></li>
			<?php } else { ?>
				<li class="dropdown-submenu">
					<a href="#" class="dropdown-toggle dropDownTarget" data-cat="<?php echo $categoryObj->id; ?>" data-url="<?php echo url("admin/deal/getcategorybyId", ["id" => $categoryObj->id]); ?>" data-toggle="dropdown"><?php echo $categoryObj->name; ?></a>
				</li>
			<?php } ?>
	<?php } ?>
	
</ul>

<?php /* ?>										
<ul class="dropdown-menu">
	<li><a href="#">Action</a></li>
	<li class="dropdown-submenu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
		<ul class="dropdown-menu">
			<li class="dropdown-submenu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
				<ul class="dropdown-menu">
					<li><a href="#">Action</a></li>
					<li><a href="#">Another action</a></li>
					<li><a href="#">Something else here</a></li>
					<li class="divider"></li>
					<li><a href="#">Separated link</a></li>
					<li class="divider"></li>
					<li><a href="#">One more separated link</a></li>
				</ul>
			</li>
		</ul>
	</li>
</ul>
<?php /*/ ?>