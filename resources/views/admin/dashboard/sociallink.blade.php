@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Setting</h1>
		</div>
	</div>
	<?php
		$facebook = \App\Setting::where("setting_key", 'facebook_link')->first();
		$youtube = \App\Setting::where("setting_key", 'youtube_link')->first();
		$twitter = \App\Setting::where("setting_key", 'twitter_link')->first();
		$linkdin = \App\Setting::where("setting_key", 'linkdin_link')->first();
		$google = \App\Setting::where("setting_key", 'google_link')->first();
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Social Link</div>
				<div class="panel-body">
					
					<div class="col-lg-12">
						@if(session()->has('success_social_link'))
							<div class="alert alert-success">
								{{ session()->get('success_social_link') }}
							</div>
						@endif
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul style="list-style: none;">
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>
					<div class="col-md-7">
						<form role="form" id="settingform" class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url('/admin/link'); ?>">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Facebook</label>
								<input class="form-control" value="<?php if($facebook) { echo $facebook->value; } ?>" name="facebook" type="text">
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Youtube</label>
								<input class="form-control" value="<?php if($youtube) { echo $youtube->value; } ?>" name="youtube" type="text">
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Twitter</label>
								<input class="form-control" value="<?php if($twitter) { echo $twitter->value; } ?>" name="twitter" type="text">
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Linkdin</label>
								<input class="form-control" value="<?php if($linkdin) { echo $linkdin->value; } ?>" name="linkdin" type="text">
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="form-group">
								<label>Google+</label>
								<input class="form-control" value="<?php if($google) { echo $google->value; } ?>" name="google" type="text">
							</div>
						</div>
					
						<button type="submit" class="btn btn-primary">
							Save
						</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/editor.css') }}">
	<link href="{{ asset('public/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script>

$(document).ready(function() {
	DealManager.init();
	$('body').on('change', '.pageoption', function(e) {
		var value = $(this).val();
		$(".categoryoptioncontainer").attr('style', 'display:none');
		if(value == 2) {
			$(".categoryoptioncontainer").attr('style', '');
		}
	});
    /* CKEDITOR.replace('myckeditor');  
    CKEDITOR.on('dialogDefinition', function(ev) {
        //dialogDefinition is a ckeditor event it's fired when ckeditor dialog instance is called  
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        if (dialogName == 'image') { //dialogName is name of dialog and identify which dialog is fired.  
            var infoTab = dialogDefinition.getContents('info'); // get tab of the dialog  
            var browse = infoTab.get('browse'); //get browse server button  
            browse.onClick = function() {  
                //here we can invoke our custom fileuploader or server files popup  
                alert('open your file uploader or server files popup');  
            };  
        }  
	}); */
});

/* CKEDITOR.on( 'dialogDefinition', function( evt ) {
    var dialog = evt.data;

    if ( dialog.name == 'link' ) {
        // Get dialog definition.
        var def = evt.data.definition;

        // Add some stuff to definition.
        def.addContents( {
            id: 'custom',
            label: 'My custom tab',
            elements: [
                {
                    id: 'myField1',
                    type: 'text',
                    label: 'My Text Field'
                },
                {
                    id: 'myField2',
                    type: 'text',
                    label: 'Another Text Field'
                }
            ]
        });

    }
} );

CKEDITOR.replace( 'ckeditor' ); */
/* CKEDITOR.on('dialogDefinition', function(ev) {

    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if (dialogName == 'image') {
        dialogDefinition.onShow = function () {
            // This code will open the Upload tab.
            this.selectPage('Upload');
        };
    }
});

CKEDITOR.replace( 'text',
    {
        filebrowserBrowseUrl : '/browser/browse.php',
		filebrowserImageBrowseUrl : '/browser/browse.php?type=Images',
		filebrowserUploadUrl : '/uploader/upload.php', //document.getElementsByName('base_url')[0].getAttribute('content')+'/admin/upload/
		filebrowserImageUploadUrl : '/uploader/upload.php?type=Images'
    }); */
/* CKEDITOR.replace('textarea', {
		filebrowserImageUploadUrl: '/upload'
	});

	CKEDITOR.on('dialogDefinition', function(ev) {
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;

		if (dialogName == 'image') {
			dialogDefinition.onLoad = function() {
				var dialog = CKEDITOR.dialog.getCurrent();

				var uploadTab = dialogDefinition.getContents('Upload');
				var uploadButton = uploadTab.get('uploadButton');
				console.log('uploadButton', uploadButton);

				uploadButton['onClick'] = function(evt){
					console.log('fire in the hole', evt);
				}

				uploadButton['filebrowser']['onSelect'] = function(fileUrl, errorMessage) {
					console.log('working');
				}
			};

		}

	}); */
	/* CKEDITOR.on('dialogDefinition', function(e) {
		if (e.data.name == 'image') {
			var dialog = e.data.definition;
			oldOnShow = dialog.onShow;
			dialog.onShow = function() {
				 oldOnShow.apply(this, arguments);
				 this.selectPage('Upload');
			};
		}
	}); */ 
/* CKEDITOR.on('dialogDefinition', function(ev) {
    var editor = ev.editor;
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if (dialogName == 'image') {
        var infoTab = dialogDefinition.getContents( 'info' );
        infoTab.remove( 'txtBorder' ); //Remove Element Border From Tab Info
        infoTab.remove( 'txtHSpace' ); //Remove Element Horizontal Space From Tab Info
        infoTab.remove( 'txtVSpace' ); //Remove Element Vertical Space From Tab Info
        infoTab.remove( 'txtWidth' ); //Remove Element Width From Tab Info
        infoTab.remove( 'txtHeight' ); //Remove Element Height From Tab Info

        //Remove tab Link
        dialogDefinition.removeContents( 'Link' );
    }
}); */
	/* $("#txtEditor").Editor();
	$("body").on('submit', '#settingform', function() {
		var text = $(".Editor-editor").html();
		$("#txtEditor").val(text);
	}); */
	/* CKEDITOR.replace('text', {
        filebrowserBrowseUrl = '/elfinder/ckeditor',
        filebrowserImageBrowseUrl : '/elfinder/ckeditor',
        uiColor : '#9AB8F3',
        height : 300
    }); */
	

	/* CKEDITOR.on('dialogDefinition', function (ev) {
	  var dialogName = ev.data.name;
	  var dialog = ev.data.definition.dialog;

	  if (dialogName == 'image') {
		  dialog.on('show', function () {
			  this.selectPage('Upload');
		  });
	  }
	}); */
	
	
</script>
@endpush
