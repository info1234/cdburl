<?php foreach($deals as $obj) { ?>
	<tr>
		<td><?php echo $obj->name; ?></td>
		<td>
			<?php
				$categoryObj = \App\Category::find($obj->category_id);
				if($categoryObj != null) {
					//if($categoryObj->slug_name != 'miscellaneous') {
						echo $categoryObj->name;
					//}
				}
			?>
		</td>
		<td><?php echo ucfirst($obj->store_id); ?></td>
		<td>$<?php echo $obj->price; ?></td>
		<td><?php echo date('d/m/Y', strtotime($obj->created_at)); ?></td>
	</tr>
<?php } ?>