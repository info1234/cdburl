@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
	<div class="col-lg-12">
		<h1 class="page-header">Setting</h1>
	</div>
	<form role="form" class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url('/admin/setting'); ?>">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Upload Header Image</label> <?php if($headerlogoimage != null) { ?><img src="<?php echo url('public/images/logo/header/'.$headerlogoimage->value); ?>" /> <?php } ?>
								<input class="form-control" type="file" name="image" value="" />
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
		<?php /* ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-lg-12">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Number of Frontpage Deal</label>
									<select class="form-control" name="frontpagedeal">
										<option>Select Frontpage Deal</option>
										<option value="24" <?php if($frontPerPage != null && $frontPerPage->value == 24) { ?>selected<?php } ?>>Show 24 Per Page</option>
										<option value="30" <?php if($frontPerPage != null && $frontPerPage->value == 30) { ?>selected<?php } ?>>Show 30 Per Page</option>
										<option value="36" <?php if($frontPerPage != null && $frontPerPage->value == 36) { ?>selected<?php } ?>>Show 36 Per Page</option>
										<option value="42" <?php if($frontPerPage != null && $frontPerPage->value == 42) { ?>selected<?php } ?>>Show 42 Per Page</option>
									</select>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-lg-12">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Number of Coupon to Show Per Page</label>
									<select class="form-control" name="couponperpage">
										<option>Select Coupon Per Page</option>
										<option value="10" <?php if($couponPerPage != null && $couponPerPage->value == 10) { ?>selected<?php } ?>>Show 10 Per Page</option>
										<option value="20" <?php if($couponPerPage != null && $couponPerPage->value == 20) { ?>selected<?php } ?>>Show 20 Per Page</option>
										<option value="30" <?php if($couponPerPage != null && $couponPerPage->value == 30) { ?>selected<?php } ?>>Show 30 Per Page</option>
										<option value="40" <?php if($couponPerPage != null && $couponPerPage->value == 40) { ?>selected<?php } ?>>Show 40 Per Page</option>
									</select>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
		
		<?php */ ?>
		<button type="submit" class="btn btn-primary">
			Save Setting
		</button>
	</form>
		
</div>
@endsection
