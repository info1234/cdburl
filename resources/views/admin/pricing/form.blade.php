@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-lg-12 grid-margin stretch-card">
			<div class="row flex-grow">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $pricingObj->type; ?></h4>
									<form class="forms-sample" method="post" action="<?php echo url('admin/pricing/edit/'.$pricingObj->id); ?>">
										<div class="flash-message">
											@foreach (['danger', 'warning', 'success', 'info'] as $msg)
											  @if(Session::has('alert-' . $msg))
												<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
											  @endif
											@endforeach
										</div>

										@if ($errors->any())
											<div class="alert alert-danger">
												<ul style="list-style: none;">
													@foreach ($errors->all() as $error)
														<li>{{ $error }}</li>
													@endforeach
												</ul>
											</div>
										@endif
										<div class="form-group">
											<label for="price">Price</label>
											<input name="price" value="<?php echo $pricingObj->price; ?>" type="text" class="form-control" id="price">
										</div>
										<div class="form-group">
											<label for="imageHosting">Price Per</label>
											<select name="price_per" class="form-control">
												<option value="">Select</option>
												<option value="mo" <?php if($pricingObj->price_per == 'mo') { ?>selected<?php } ?>>mo</option>
												<option value="year" <?php if($pricingObj->price_per == 'year') { ?>selected<?php } ?>>year</option>
											</select>
										</div>
										<div class="form-group">
											<label for="exampleInputName">Integrated Channel</label>
											<input name="integrated_channel" value="<?php echo $pricingObj->integrated_channel; ?>" type="text" class="form-control" id="exampleInputName">
										</div>
										<div class="form-group">
											<label for="multipleAccountsforEachChannel">Multiple Accounts for Each Channel</label>
											<input name="multiple_account" value="<?php echo $pricingObj->multiple_account; ?>" type="text" class="form-control" id="multipleAccountsforEachChannel">
										</div>
										<div class="form-group">
											<label for="staffAccounts">Staff Accounts</label>
											<input name="staff_accounts" value="<?php echo $pricingObj->staff_accounts; ?>" type="text" class="form-control" id="staffAccounts">
										</div>
										<div class="form-group">
											<label for="managedActiveListings">Managed Active Listings</label>
											<input name="managed_active_listing" value="<?php echo $pricingObj->managed_active_listing; ?>" type="text" class="form-control" id="managedActiveListings">
										</div>
										<div class="form-group">
											<label for="ordersPerMonth">Orders per Month</label>
											<input name="orders_per_month" value="<?php echo $pricingObj->orders_per_month; ?>" type="text" class="form-control" id="ordersPerMonth">
										</div>
										<div class="form-group">
											<label for="imageHosting">Image Hosting</label>
											<select name="image_hosting" class="form-control">
												<?php foreach(pricingImageHosting() as $key => $pricingImageHostingObj) { ?>
													<option value="<?php echo $key; ?>" <?php if($key == $pricingObj->image_hosting) { ?> selected <?php } ?>><?php echo $pricingImageHostingObj; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group">
											<label for="powerfulPricingRules">Powerful Pricing Rules</label>
											<select name="powerful_pricing_rules" class="form-control">
												<option value="0" <?php if($pricingObj->powerful_pricing_rules == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->powerful_pricing_rules == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="dailyInventoryMonitor">Daily Inventory Monitor</label>
											<select name="daily_inventory_monitor" class="form-control">
												<option value="0" <?php if($pricingObj->daily_inventory_monitor == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->daily_inventory_monitor == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="dailyPriceMonitor">Daily Price Monitor</label>
											<select name="daily_price_monitor" class="form-control">
												<option value="0" <?php if($pricingObj->daily_price_monitor == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->daily_price_monitor == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="automaticOrderFulfillment">Automatic Order Fulfillment</label>
											<select name="automatic_order_fulfillment" class="form-control">
												<option value="0" <?php if($pricingObj->automatic_order_fulfillment == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->automatic_order_fulfillment == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="orderTrackingNumberSync">Order Tracking Number Sync</label>
											<select name="order_tracking_number_sync" class="form-control">
												<option value="0" <?php if($pricingObj->order_tracking_number_sync == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->order_tracking_number_sync == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="dropshippingSupplierAPI">Dropshipping Supplier API</label>
											<select name="dropshipping_supplier_api" class="form-control">
												<option value="0" <?php if($pricingObj->dropshipping_supplier_api == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->dropshipping_supplier_api == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										
										<div class="form-group">
											<label for="eBayTemplatespresets ">eBay Templates & Presets</label>
											<select name="ebay_templates_and_presets" class="form-control">
												<option value="0" <?php if($pricingObj->ebay_templates_and_presets == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->ebay_templates_and_presets == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="eBayAutoRestock">eBay Auto Restock</label>
											<select name="ebay_auto_restock" class="form-control">
												<option value="0" <?php if($pricingObj->ebay_auto_restock == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->ebay_auto_restock == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="eBayMessageAssistant ">eBay Message Assistant</label>
											<select name="ebay_message_assistant" class="form-control">
												<option value="0" <?php if($pricingObj->ebay_message_assistant == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->ebay_message_assistant == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="uspsShippingLabels">USPS Shipping Labels</label>
											<select name="usps_shipping_labels" class="form-control">
												<option value="0" <?php if($pricingObj->usps_shipping_labels == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->usps_shipping_labels == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<div class="form-group">
											<label for="automaticTrackingInfoUpdates">Automatic Tracking Info Updates</label>
											<select name="automatic_tracking_info_updates" class="form-control">
												<option value="0" <?php if($pricingObj->automatic_tracking_info_updates == 0) { ?>selected<?php } ?>>No</option>
												<option value="1" <?php if($pricingObj->automatic_tracking_info_updates == 1) { ?>selected<?php } ?>>Yes</option>
											</select>
										</div>
										<?php $support = json_decode($pricingObj->support); ?>
										<div class="form-group">
											<label for="support">Support</label>
											<div class="col-md-6">
												<div class="form-group">
												  <div class="form-check">
													<label class="form-check-label">
														<input type="checkbox" name="support[email]" <?php if(isset($support->email)) { ?> checked <?php } ?> value="Email" class="form-check-input"> Email
													</label>
												  </div>
												  <div class="form-check">
													<label class="form-check-label">
														<input type="checkbox" name="support[live_chat]" <?php if(isset($support->live_chat)) { ?> checked <?php } ?> value="Live Chat" class="form-check-input"> Live chat
													</label>
												  </div>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success mr-2">Submit</button>
								  </form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('body').on('keyup', '#price',function() {
				var val = $(this).val();
				var newPrice = val.replace(/[^0-9\.]/g, '');
				$(this).val(newPrice);
			});
		});
	</script>
@endpush