@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-lg-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
				  <h4 class="card-title">Pricing</h4>
				  <div class="table-responsive">
					<table class="table table-striped">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Type</th>
						  <th>Price</th>
						  <th>Price Per</th>
						  <th>Integrated Channel</th>
						  <th>Multiple Accounts for Each Channel</th>
						  <th>Staff Accounts</th>
						  <th>Managed Active Listings</th>
						  <th>Orders per Month</th>
						  <th>Image Hosting</th>
						  <th>Support</th>
						  <th>Action</th>
						</tr>
					  </thead>
					  <tbody>
						<?php foreach($pricing as $pricingObj) { ?>
							<tr>
								<td class="py-1"><?php echo $pricingObj['id']; ?></td>
								<td><?php echo $pricingObj['type']; ?></td>
								<td>
									<?php if($pricingObj['price'] > 0) { ?>
											$<?php echo $pricingObj['price']; ?>
									<?php } ?>
								</td>
								<td><?php echo $pricingObj['price_per']; ?></td>
								<td><?php echo $pricingObj['integrated_channel']; ?></td>
								<td><?php echo $pricingObj['multiple_account']; ?></td>
								<td><?php echo $pricingObj['staff_accounts']; ?></td>
								<td><?php echo $pricingObj['managed_active_listing']; ?></td>
								<td><?php echo $pricingObj['orders_per_month']; ?></td>
								<td><?php echo $pricingObj['image_hosting']; ?></td>
								<td>
									<?php $data = json_decode($pricingObj['support'], true); 
											if(is_array($data) && count($data) > 0) {
												echo implode(', ', $data);
											}
									?>
								</td>
								<td>
									<a href="<?php echo url('admin/pricing/edit/'.$pricingObj['id']); ?>">
										<i class="menu-icon mdi mdi-restart"></i>
									</a>
								</td>
							</tr>
						<?php } ?>
					  </tbody>
					</table>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection