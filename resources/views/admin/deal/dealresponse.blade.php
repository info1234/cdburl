
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Url</div>

                <div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
						<?php
							$url = url('comment/handle'); ?>
							<div class="col-lg-12">
								<div class="form-group">
									<label>Url</label>
									<input type="text" class="form-control" id="response" name="response" value="" required>
								</div>
								<div class="form-group">
									<a href="#" class="responsesubmit">
									<button type="button" class="btn btn-primary">
										Submit
									</button>
									</a>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('styles')
@endpush

@push('scripts')
	<script type="text/javascript">
		$( document ).ready(function() {
			
		});
	</script>
@endpush