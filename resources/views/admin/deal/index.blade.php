@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Deal</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Deal
					&nbsp;<a href="<?php echo url('admin/deal/create'); ?>">
						Create Deal
					</a>
				</div>
				
                <div class="panel-body">
                    <div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>	
								<tr>
									<th>Name</th>
									<!--<th>Store</th>-->
									<th>Category</th>
									<!--<th>Brand</th>-->
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($deals as $obj) { ?>
									<tr>
										<td><?php echo $obj->name; ?></td>
										<?php //echo $obj->deal_type_price; ?>
										
											<?php
												/* $objStore = \App\Store::find($obj->store_id);
												if(isset($objStore)) {
													echo $objStore->name;
												} */
											?>
										
										<td><?php $categoryObj = \App\Category::find($obj->category_id); 
												if($categoryObj && $categoryObj->slug_name != 'miscellaneous') {
													echo $categoryObj->name;
												} ?></td>
										<?php /* $brandObj = \App\Brand::find($obj->brand_id); 
												if($brandObj) {
													echo $brandObj->name;
												} */
										//echo //$obj->category->name; ?>
										<td>
											<a class="" href="<?php echo url('admin/deal/edit', ['id' => $obj->id]); ?>">
												Edit
											</a>
											<?php 
												$html = 'InActive';
												$status = 0;
												if($obj->approval == 0) {
													$html = 'Active';
													$status = 1;
												} ?>
											&nbsp;&nbsp;<a class="approval approval_<?php echo $obj->id; ?>" data-deal-id="<?php echo $obj->id; ?>" data-deal-status="<?php echo $status; ?>" href="#">
												<?php echo $html; ?>
											</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php /* $deals->links() */ ?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		CategoryManager.init();
		DealManager.init();
	</script>
@endpush