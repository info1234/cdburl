@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Inventory / <?php echo $productText; ?></h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
<form id="product-form" enctype="multipart/form-data" method="POST">
  <div class="row">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-title">
							<h4 class="secondry-colr">Basic Information</h4>
						</div>
						<div class="recent-comment">
							
								{{ csrf_field() }}
								<div class="col-lg-12">
									<div class="form-group">
										<label>SKU*</label>
										<input class="form-control" type="text" id="sku" name="sku" value="" required />
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group">
										<label>SKU Aliases</label>
										<input class="form-control" type="text" id="sku" name="sku" value="" />
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group">
										<label>Product Identifier</label>
										<input class="form-control" type="text" id="Product_Identifier" name="Product_Identifier" value="" />
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group">
										<label>Product Name*</label>
										<input class="form-control" type="text" id="name" name="name" value="" required />
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group">
										<label>Short Description</label>
										<input class="form-control" type="text" id="description" name="description" value="" required />
									</div>
								</div>
							
						</div>
						
						
					</div>
					<!-- /javascript:void(0) card -->
				</div>
			</div>
		</div>
	</div>
</form>
	<!-- End PAge Content -->
</div>

@endsection
