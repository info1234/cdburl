@extends('layouts.home')

@section('content')
<!-- services-section -->
	<section class="service-slider" data-aos="fade-up">
	<div class="container">
	 <div class="regular slider">

		<div>
		  <img src="{{asset('images/s1.png')}}" alt="service-image-1">
		</div>
		<div>
				<img src="{{asset('images/s2.png')}}" alt="service-image-2">
		</div>
		<div>
			 <img src="{{asset('images/s1.png')}}" alt="service-image-3">
		</div>
		<div>
				<img src="{{asset('images/s4.png')}}" alt="service-image-4">
		</div>
	  <div>
		  <img src="{{asset('images/s1.png')}}" alt="service-image-1">
		</div>
		<div>
				<img src="{{asset('images/s2.png')}}" alt="service-image-2">
		</div>
		<div>
			 <img src="{{asset('images/s1.png')}}" alt="service-image-3">
		</div>
		<div>
			<img src="{{asset('images/s4.png')}}" alt="service-image-4">
		</div>
	  </div>
	</div>
	</section>
	
	<section class="selling-channels">
        <h5 class="text-center shipng-icon"><i class="fa fa-shopping-bag"></i></h5>
		<h3 class="text-center m-heading">A complete solution for selling on multiple channels</h3>
		<p class="text-center main-des">We help you start profitable drop shipping business across multiple channels</p>
		 <div class="container">
		   <div class="channel-inr">
			<div class="row">
				  <div class="col-md-8 col-sm-8"  >
					<h3 class="text-left">Product Sourcing and Rapid listing</h3>
					  <ul class="product-list text-left">
						<li>Import products from Aliexpress, Amazon and 32 more suppliers</li>
						<li>Auto repricer and inventory monitor</li>
						<li>Import product images and set up product variations</li>
						<li>Bulk import, edit and list products</li>
						<li>Lists product to mulitple channels easily</li>
					  </ul>  

				  </div>

				<div class="col-md-4 col-sm-4 " >
				  <figure>
					<img src="{{asset('images/p1.png')}}" alt="product-channel-image" class="img-fluid">
				  </figure>
				</div>
			</div>
		  </div>
		  
		  <div class="channel-inr">
			<div class="row">
			
			<div class="col-md-4 col-sm-4 " >
				  <figure>
					<img src="{{asset('images/p2.png')}}" alt="product-channel-image" class="img-fluid">
				  </figure>
				</div>
				  <div class="col-md-8 col-sm-8">
					<h3 class="text-right">Smart Tools and eBay Features</h3>
					  <ul class="product-list text-right">
						<li> Set up drop shipping price automatically</li>
						<li> One-click translate Chinese to English</li>
						<li> Prefill eBay policies and insert size chart template</li>
						<li> Select eBay category and fill out specifics automatically</li>
						<li>One-click import with Chrome extension of ShopMaster</li>
					  </ul>  

				  </div>

				
			</div>
		  </div>
		  
		  <div class="channel-inr br-0">
			<div class="row">
				  <div class="col-md-8 col-sm-8" >
					<h3 class="text-left">Order Management and More</h3>
					  <ul class="product-list text-left">
						<li> Visit source product directly in order management</li>
						<li> Mark orders as shipped</li>
						<li> Sales and orders statistics</li>
						<li> Free Image Hosting</li>
						<li> Free STARTER plan accelerate your business</li>
					  </ul>  

				  </div>

				<div class="col-md-4 col-sm-4 " >
				  <figure>
					<img src="{{asset('images/p3.png')}}" alt="product-channel-image" class="img-fluid" >
				  </figure>
				</div>
			</div>
		  </div>
		 </div>

	</section>
	
	<section class="pricing"  data-aos-duration="600" data-aos="fade-up">

<h5 class="text-center shipng-icon"><i class="fa fa-shopping-bag"></i></h5>
		<h3 class="text-center m-heading">Pricing for business of any size</h3>
		<p class="text-center main-des">No credit card required.</p>
		<div class="text-center">
		 <button class="get-started btn">Get Started Now</button>
		
		</div>
  <div class="container">

	   <table>
	   
	    <tr>
			<?php
				foreach($tablehead as $tableheadObj) {
					foreach($tableheadObj as $key => $tableheadObjData) {
						if($key == 0) {
			?>
						<th>Pricing</th>
			<?php } else {
					$typeHeader = $tableheadObjData['type'];
					if($tableheadObjData['price'] > 0) { ?>
						<th>
							<?php echo $tableheadObjData['type']; ?><br/><span>$<?php echo $tableheadObjData['price']; ?>/<?php echo $tableheadObjData['price_per']; ?></span>
						</th>
			<?php } else { ?>
						<th>
							<?php echo $tableheadObjData['type']; ?><br/><span>FREE</span>
						</th>
			<?php } ?>
				<?php }} } ?>
		
		</tr>
		
		<tr>
			<?php foreach($pricingData as $data) { ?>
				<td class="f-chld"><?php echo $data['Integrated_Channel']; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach($pricingData as $data) { ?>
				<td><?php echo $data['multiple_account']; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach($pricingData as $data) { ?>
				<td><?php echo $data['staff_accounts']; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach($pricingData as $data) { ?>
				<td><?php echo $data['managed_active_listing']; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach($pricingData as $data) { ?>
				<td><?php echo $data['orders_per_month']; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach($pricingData as $data) { ?>
				<td><?php echo $data['image_hosting']; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['powerful_pricing_rules']; ?></td>
					<?php } else {
					if($data['powerful_pricing_rules'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['daily_inventory_monitor']; ?></td>
					<?php } else {
					if($data['daily_inventory_monitor'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['daily_price_monitor']; ?></td>
					<?php } else {
					if($data['daily_price_monitor'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['automatic_order_fulfillment']; ?></td>
					<?php } else {
					if($data['automatic_order_fulfillment'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['order_tracking_number_sync']; ?></td>
					<?php } else {
					if($data['order_tracking_number_sync'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['dropshipping_supplier_api']; ?></td>
					<?php } else {
					if($data['dropshipping_supplier_api'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['ebay_templates_and_presets']; ?></td>
					<?php } else {
					if($data['ebay_templates_and_presets'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['ebay_auto_restock']; ?></td>
					<?php } else {
					if($data['ebay_auto_restock'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['ebay_message_assistant']; ?></td>
					<?php } else {
					if($data['ebay_message_assistant'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['usps_shipping_labels']; ?></td>
					<?php } else {
					if($data['usps_shipping_labels'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['automatic_tracking_info_updates']; ?></td>
					<?php } else {
					if($data['automatic_tracking_info_updates'] == 1) { ?>
						<td><i class="fa fa-check"></i></td>
				<?php } else { ?>
						<td></td>
			<?php } } } ?>
		</tr>
		
		<tr>
			<?php foreach($pricingData as $key => $data) {
					if($key == 0) { ?>
						<td><?php echo $data['support']; ?></td>
					<?php } else { ?>
					<td>
						<?php if(isset($data['support']->email) && isset($data['support']->live_chat)) { ?>
							Email,Live chat
						<?php } else if(isset($data['support']->email)) { ?>
							Email
						<?php } else if(isset($data['support']->live_chat)) { ?>
							Live chat
						<?php } ?>
					</td>
			<?php } }  ?>
		</tr>
		
		<tr>
			 <td  class="last-td">Select Plan</td>
			 <td  class="last-td"><button class="btn get-started" onclick="window.location.href='#login'">get started now</button></td>
			 <td class="last-td"><button class="btn get-started" onclick="window.location.href='#login'">get started now</button></td>
			 <td  class="last-td"><button class="btn get-started" onclick="window.location.href='#login'">get started now</button></td>
			 <td  class="last-td"><button class="btn get-started" onclick="window.location.href='#login'">get started now</button></td>
	    </tr>
	   </table>
  
  </div>


</section>

<section class="client-slider" >
     <div class="container">
	  
	  
	   <div class=" slider client">

   
	<div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-1">
    </div>
	<div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-2">
    </div><div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-3">
    </div>
	<div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-4">
    </div>
	<div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-5">
    </div>
	<div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-6">
    </div>
	<div>
      <img src="{{asset('images/cl.png')}}" alt="client-image-7">
    </div>
    
  </div>
	 
	 </div>
  
  </section> 
@endsection
