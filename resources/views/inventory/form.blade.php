@extends('layouts.app')

@section('content')

<?php
$url = url('inventory/add');
$sku = sku();
$name = $brand = $short_description = $description = $price = $msrp = $quantity = $unit_cost = $MPN = $condition = $dimensions_length_value = $dimensions_width_value = $dimensions_height_value = $dimensions_option = $weight = $weight_option = $customs_description = $declared_value = $declared_option = $country = $tariff_number = '';
$productId = null;
$variants = array();
$variantCount = 0;
$category = 1;
if($obj != null) {
	$productId = $obj->id;
	$variants = getVariantsByProductId($productId);
	$variantCount = $variants->count();
	$url = url('inventory/edit/'.$productId);
	$name = $obj->name;
	$sku = $obj->sku;
	$brand = $obj->brand;
	$short_description = $obj->short_description;
	$description = $obj->description;
	$price = $obj->price;
	$msrp = $obj->msrp;
	$quantity = $obj->quantity;
	$unit_cost = $obj->unit_cost;
	$MPN = $obj->MPN;
	$condition = $obj->condition;
	$dimensions_length_value = $obj->dimensions_length_value;
	$dimensions_width_value = $obj->dimensions_width_value;
	$dimensions_height_value = $obj->dimensions_height_value;
	$dimensions_option = $obj->dimensions_option;
	$weight = $obj->weight;
	$weight_option = $obj->weight_option;
	$customs_description = $obj->customs_description;
	$declared_value = $obj->declared_value;
	$declared_option = $obj->declared_option;
	$country = $obj->country;
	$tariff_number = $obj->tariff_number;
	$category = $obj->category;
}
?>

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Inventory Add Product</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->

<div class="product-sec-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inven_prod">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#tab_a" data-toggle="tab">Product </a> </li> &nbsp;&nbsp;&nbsp;
					  
					  <li><a href="#tab_b" data-toggle="tab">Description </a> </li> &nbsp;&nbsp;&nbsp;
					  <li><a href="#tab_c" data-toggle="tab">Shipping & Customs</a></li>
					</ul>
				</div>
			<form id="inventory-form" action="<?php echo $url; ?>" enctype="multipart/form-data" method="POST">
				<!--table-part-->
				<div  class="tab-content">
					<div class="tab-pane active" id="tab_a">
						<div class="quick_fill">
							<div class="flash-message">
								@foreach (['danger', 'warning', 'success', 'info'] as $msg)
								  @if(Session::has('alert-' . $msg))
									<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
								  @endif
								@endforeach
							 </div>
							 
							@if ($errors->any())
								<div class="form-group">
									<div class="alert alert-danger">
										<ul style="list-style: none;">
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								</div>
							@endif
							
							<h5>Basic Information</h5> 
							<?php if($productId != null) { ?>
								<a class="float-right" style="margin-right:10px" target="_blank" href="<?php echo url('inventory/detail/'.$productId); ?>">Preview</a>
							<?php } ?>
						   <div class="form-group">
								<label class="col-md-12">Sku*</label>
								<div class="col-md-12">
									<input name="sku" value="<?php echo $sku; ?>" id="sku" class="form-control form-control-line" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">SKU Aliases</label>
								<div class="col-md-12">
									<div>
										<div class="input-group">
											<select name="" class="form-control input-group-addon col-4 SkuoptionClone SkuoptionCloneEach SkuselectBox_1 SkuoptionCloneOrg SkuoptionCloneChange" data-select-count="1">
													<option value="">All Stores</option>
											</select>
											<input type="hidden" name="SKU_aliases[1][option]" class="SkuselectBoxHidden_1" value="" />
											<input type="text" name="SKU_aliases[1][value]" class="form-control col-4" value="" />&nbsp; <a href="#" class="Skuaddoption" data-option="<?php if(count($skuAliases) > 0) { echo count($skuAliases)+1; } else { ?>1<?php } ?>" data-count="<?php if(count($skuAliases) > 0) { echo count($skuAliases)+1; } else { ?>1<?php } ?>">Add</a>
										</div>
									</div>
									
								</div>
								<div class="SkuoptionContainer">
									<?php if(!empty($skuAliases)) { 
											$skuAliasesKey = 1;
											foreach($skuAliases as $skuAliasesObj) {
												$skuAliasesKey++;
									?>
												<div class="SkucloneContainer">
													<div class="col-md-12">
													<div>
														<div class="input-group">
															<select class="SkuoptionClone Skuselectclone form-control input-group-addon col-4 SkuselectBox_<?php echo $skuAliasesKey; ?> SkuoptionCloneEach" data-select-count="<?php echo $skuAliasesKey; ?>">
																<option value="">All Stores</option>
															</select>
															<input class="SkuselectBoxHidden SkuselectBoxHidden_2" name="SKU_aliases[<?php echo $skuAliasesKey; ?>][option]" value="<?php echo $skuAliasesObj['SKU_aliases_option']; ?>" type="hidden">
															<input class="form-control col-4 Skuselectcloneinput SkuinputBox_2 SkuinputCloneEach" value="<?php echo $skuAliasesObj['SKU_aliases_value']; ?>" name="SKU_aliases[<?php echo $skuAliasesKey; ?>][value]" type="text">&nbsp; <a href="#" class="Skuromoveoption" data-option="<?php echo $skuAliasesKey; ?>">Remove</a>
														</div>
													</div>
													</div>
												</div>
									<?php } } ?>
								</div>
							</div>
							
							<div class="form-group">
								<label for="example-email" class="col-md-12">Product Identifier	</label>
								<div class="col-md-12">
									<div>
										<div class="input-group">
											<select name="" class="form-control input-group-addon col-4 optionClone optionCloneEach selectBox_1 optionCloneOrg optionCloneChange" data-select-count="1">
												<?php $productIdentifier = '';
													$identifierkey = 0;
													foreach(identifier() as $key => $identifierObj) {
														if($identifierkey == 0) { $productIdentifier = $identifierObj; }
														$identifierkey++;
													?>
													<option value="<?php echo $identifierObj; ?>"><?php echo $identifierObj; ?></option>
												<?php } ?>
											</select>
											<input type="hidden" name="product_Identifier[1][option]" class="selectBoxHidden_1" value="<?php echo $productIdentifier; ?>" />
											<input type="text" name="product_Identifier[1][value]" class="form-control col-4" value="" />&nbsp; <a href="#" class="addoption" data-option="<?php if(count($productIdentifierData) > 0) { echo count($productIdentifierData)+1; } else { ?>1<?php } ?>" data-count="<?php if(count($productIdentifierData) > 0) { echo count($productIdentifierData)+1; } else { ?>1<?php } ?>">Add</a>
										</div>
									</div>
								</div>
								<div class="optionContainer">
									<?php $productIdentifierDataOptionKey = 1;
									
										foreach($productIdentifierData as $key => $productIdentifierDataObj) {
											$productIdentifierDataOptionKey++;
										?>
										<div class="cloneContainer">
											<div class="col-md-12">
												<div>
													<div class="input-group">
														<select class="optionClone selectclone form-control input-group-addon col-4 selectBox_<?php echo $productIdentifierDataOptionKey; ?> optionCloneEach" data-select-count="2" disabled="disabled">
															<?php foreach(identifier() as $key => $identifierObj) {
																$selected = '';
																if($identifierObj == $productIdentifierDataObj['product_Identifier']) {
																	$selected = 'selected';
																}
															?>
																<option value="<?php echo $identifierObj; ?>" <?php echo $selected; ?>><?php echo $identifierObj; ?></option>
															<?php } ?>
														</select>
														<input class="selectBoxHidden selectBoxHidden_<?php echo $productIdentifierDataOptionKey; ?>" name="product_Identifier[<?php echo $productIdentifierDataOptionKey; ?>][option]" value="<?php echo $productIdentifierDataObj['product_Identifier']; ?>" type="hidden">
														
														<input class="form-control col-4 selectcloneinput inputBox_<?php echo $productIdentifierDataOptionKey; ?> inputCloneEach" value="<?php echo$productIdentifierDataObj['product_Identifier_value']; ?>" name="product_Identifier[<?php echo $productIdentifierDataOptionKey; ?>][value]" type="text">&nbsp; <a href="#" class="romoveoption" data-option="<?php echo $productIdentifierDataOptionKey; ?>">Remove</a>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label for="example-name" class="col-md-12">Product Name*	</label>
								<div class="col-md-12">
									<input type="text" class="form-control form-control-line" value="<?php echo $name; ?>" id="product_name" name="product_name">
								</div>
							</div>
							
							<div class="form-group">
								<label for="example-description" class="col-md-12">Short Description	</label>
								<div class="col-md-12">
									<textarea class="form-control"name="short_description" rows="6" style="height:auto">
										<?php echo $short_description; ?>
									</textarea>
								</div>
							</div>
						</div>
						
						<div class="quick_fill">
							<h5>Pricing & Inventory</h5>
						   <div class="form-group">
								<label class="col-md-12">Price*</label>
								<div class="col-md-12">
									<input name="price" id="price" value="<?php echo $price; ?>" class="form-control form-control-line" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">MSRP</label>
								<div class="col-md-12">
									<input name="MSRP" id="msrp" value="<?php echo $msrp; ?>" class="form-control form-control-line" type="text">
								</div>
							</div>
							<div class="form-group">
								<label for="example-email" class="col-md-12">Quantity</label>
								<div class="col-md-12">
									<input type="text" value="<?php echo $quantity; ?>" class="form-control form-control-line" name="quantity" id="quantity">
								</div>
							</div>
							<div class="form-group">
								<label for="example-email" class="col-md-12">Unit Cost</label>
								<div class="col-md-12">
									<input type="text" value="<?php echo $unit_cost; ?>" class="form-control form-control-line" name="unit_cost" id="unit_cost">
								</div>
							</div>
						</div>
						
						<div class="quick_fill">
							<h5>Specifics</h5>
						   <div class="form-group">
								<label class="col-md-12">Category</label>
								<div class="col-md-12">
									<select class="form-control" name="category">
										<?php foreach(category() as $categoryObjKey => $value) { ?>
											<option value="<?php echo $value['id']; ?>" <?php if($category == $value['id']) { ?> selected <?php } ?>><?php echo $value['name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Brand</label>
								<div class="col-md-12">
									<input name="brand" id="brand" value="<?php echo $brand; ?>" class="form-control form-control-line" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-12">MPN</label>
								<div class="col-md-12">
									<input name="MPN" id="MPN" value="<?php echo $MPN; ?>" class="form-control form-control-line" type="text">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-12">Condition</label>
								<div class="col-md-12">
									<select class="form-control" name="condition" id="condition">
										<option>--</option>
										<?php foreach(condition() as $conditionObj) {
												$selected = '';
												if($condition == $conditionObj) {
													$selected = 'selected';
												}
										?>
											<option value="<?php echo $conditionObj; ?>" <?php echo $selected; ?>><?php echo $conditionObj; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
						</div>
						
						<div class="quick_fill">
							<h5>Images</h5>
						   
							<div class="form-group">
								<label class="col-md-12">Add Images</label>
								<div class="col-md-12">
									<input name="images[]" id="fileupload" multiple="multiple" class="form-control form-control-line" type="file">
								</div>
								<div id="dvPreview"></div>
							</div>
						</div>
						
						<div class="quick_fill">
							<h4 class="secondry-colr">Variations 
								<?php if($variantCount == 0) { ?>
									<a href="#" class="float-right addvariation" style="margin-right:10px">Add</a>
								<?php } ?>
							</h4>
							
							<div class="addvariationcontainer" style="<?php if($variantCount == 0) { ?>display:none<?php } ?>">
								<?php if($variantCount == 0) { ?>
									<div class="groupvariationcontainer">
										<div class="input-group">
											<input name="variation[0][name]" class="form-control col-xs-4 col-4" placeholder="Option Name" type="text">
											<input name="variation[0][value]" class="form-control col-xs-4 col-4" placeholder="Option Values" value="" type="text">
										</div>
									</div>
								<?php } ?>
								<div class="addvariationcontent">
									<?php
											foreach($variants as $variantObj) { ?>
												<div class="groupvariationcontainerwrapper">
													<div class="groupvariationcontainer groupvariationcontainer_<?php echo $variantObj['id']; ?>">
														<div class="input-group">
															<input value="<?php echo $variantObj['name']; ?>" class="variation_name form-control col-xs-4 col-4" name="variation[<?php echo $variantObj['id']; ?>][name]" type="text" placeholder="Option Name" >
															<input value="<?php echo $variantObj['value']; ?>" class="variation_value form-control col-xs-4 col-4" name="variation[<?php echo $variantObj['id']; ?>][value]" type="text" placeholder="Option Values">&nbsp;
															<a href="#" class="removevariationdata" data-remove-variation-id="<?php echo $variantObj['id']; ?>">
																<i class="fa fa-trash" aria-hidden="true"></i>
															</a>
														</div>
													</div>
												</div>
									<?php 	}
										 ?>
								</div>
								<br>
								<div class="input-group">
									<a href="#" class="addAnotherVariation" data-variation-count="0" data-variation-increase-count="0">+ Add another variation</a>
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="tab-pane" id="tab_b">
						<div class="quick_fill">

							<h5>Description</h5>
						   <div class="form-group">
								<div class="col-md-12">
									<textarea name="description" class="form-control" rows="15" style="height:auto">
										<?php echo $short_description; ?>
									</textarea>
								</div>
							</div>
						</div>
					</div>
					
					<div class="tab-pane" id="tab_c">
						<div class="quick_fill">

							<h5>Shipping</h5>
							<div class="form-group">
								<label class="col-md-12">Dimensions</label>
								<div class="form-group row col-md-12">
								  <div class="col-xs-2">
									<input class="form-control" id="ex1" value="<?php echo $dimensions_length_value; ?>" type="text" name="dimensions_length" placeholder="Length">
								  </div>&nbsp;&nbsp;
								  <div class="col-xs-2">
									<input class="form-control" id="ex2" value="<?php echo $dimensions_width_value; ?>" type="text" name="dimensions_width" placeholder="Width">
								  </div>&nbsp;&nbsp;
								  <div class="col-xs-2">
									<input class="form-control" id="ex3" value="<?php echo $dimensions_height_value; ?>" type="text" name="dimensions_height" placeholder="Height">
								  </div>&nbsp;&nbsp;
								  <div class="col-xs-2">
									<select class="form-control" name="dimensions_option">
										<?php foreach(dimensions() as $dimensionsObj) {
												$selected = '';
												if($dimensions_option == $dimensionsObj) {
													$selected = 'selected';
												}
										?>
											<option value="<?php echo $dimensionsObj; ?>" <?php echo $selected; ?>><?php echo $dimensionsObj; ?></option>
										<?php } ?>
									</select>
								  </div>
								</div> 
							</div>
							
							<div class="form-group">
								<label class="col-md-12">Weight</label>
								<div class="form-group row col-md-12">
								  <div class="col-xs-6">
									<input class="form-control" value="<?php echo $weight; ?>" name="weight" id="ex1" type="text" placeholder="Length">
								  </div>&nbsp;&nbsp;
								  
								  <div class="col-xs-2">
									<select class="form-control" name="weight_option">
										<?php foreach(weight() as $weightobj) {
												$selected = '';
												if($weight_option == $weightobj) {
													$selected = 'selected';
												}
										?>
											<option value="<?php echo $weightobj; ?>" <?php echo $selected; ?>><?php echo $weightobj; ?></option>
										<?php } ?>
									</select>
								  </div>
								</div> 
							</div>
						</div>
						
						<div class="quick_fill">

							<h5>Customs</h5>
							<div class="form-group">
								<label class="col-md-12">Description</label>
								<div class="form-group row col-md-12">
								  <div class="col-md-6">
									<input class="form-control" id="ex1" value="<?php echo $customs_description; ?>" name="customs_description" type="text" placeholder="Description">
								  </div>&nbsp;&nbsp;
								  
								</div> 
							</div>
							
							<div class="form-group">
								<label class="col-md-12">Declared Value	</label>
								<div class="form-group row col-md-4">
								  <div class="col-md-6 col-xs-2">
									<input class="form-control" value="<?php echo $declared_value; ?>" id="ex1" name="declared_value" type="text" placeholder="value">
								  </div>&nbsp;&nbsp;
								  <div class="col-xs-2">
									<select class="form-control" name="declared_option_value">
										<?php foreach(declaredValue() as $declaredValueObj) {
												$selected = '';
												if($declared_option == $declaredValueObj) {
													$selected = 'selected';
												}
										?>
										<option value="<?php echo $declaredValueObj; ?>" <?php echo $selected; ?>><?php echo $declaredValueObj; ?></option>
										<?php } ?>
									</select>
								  </div>
								</div> 
							</div>
							
							<div class="form-group">
								<label class="col-md-12">Country of Orgin</label>
								<div class="form-group row col-md-12">
									<select id="customsOrginCountry" name="country" class="form-control">
										<option value="">Default country of orgin</option>
										<?php foreach(countryData() as $key => $countryObj) {
											$selected = '';
											if($country == $key) {
												$selected = 'selected';
											}
										?>
											<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $countryObj; ?></option>
										<?php } ?>
									</select>
								  
								</div> 
							</div>
							
							<div class="form-group">
								<label class="col-md-12">Tariff Number</label>
								<div class="form-group row col-md-12">
									<div class="col-md-6">
										<input class="form-control" value="<?php echo $tariff_number; ?>" id="ex1" name="tariffNumber" type="text" placeholder="Tariff Number (optional)">
									</div>&nbsp;&nbsp;
								</div> 
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="save-changes btn">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="optionContainerClone" style="display:none">
    <div class="cloneContainer">
		<div class="col-md-12">
		<div>
			<div class="input-group">
				<select class="optionClone selectclone form-control input-group-addon col-4">
					<?php foreach(identifier() as $identifierobj) { ?>
						<option value="<?php echo $identifierobj; ?>"><?php echo $identifierobj; ?></option>
					<?php } ?>
				</select>
				<input type="hidden" class="selectBoxHidden" />
				<input type="text" class="form-control col-4 selectcloneinput" value="" />&nbsp; <a href="#" class="romoveoption" data-option="">Remove</a>
			</div>
		</div>
		</div>
    </div>
</div>

<div class="SkuoptionContainerClone" style="display:none">
    <div class="SkucloneContainer">
		<div class="col-md-12">
		<div>
			<div class="input-group">
				<select class="SkuoptionClone Skuselectclone form-control input-group-addon col-4">
					<option value="">All Stores</option>
				</select>
				<input type="hidden" class="SkuselectBoxHidden" />
				<input type="text" class="form-control col-4 Skuselectcloneinput" value="" />&nbsp; <a href="#" class="Skuromoveoption" data-option="">Remove</a>
			</div>
		</div>
		</div>
    </div>
</div>

<div class="groupvariationcontainerClone" style="display:none">
    <div class="groupvariationcontainerwrapper">
		<div class="groupvariationcontainer">
			<div class="input-group">
				<input class="variation_name form-control col-xs-4 col-4" placeholder="Option Name" type="text">
				<input class="variation_value form-control col-xs-4 col-4" placeholder="Option Values" type="text">&nbsp;
				<a href="#" class="removevariation"><i class="fa fa-trash" aria-hidden="true"></i><a>
			</div>
		</div>
	</div>
</div>
@endsection
@push('css')
<style>
	.nav-tabs { border-bottom: none;  }
</style>
@endpush
@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$(".addvariation").click(function() {
			$('.addvariationcontainer').attr('style', '');
			return false;
		});
		
		$(".addAnotherVariation").click(function() {
			var count = $(this).attr('data-variation-count');
			var increasecount = $(this).attr('data-variation-increase-count');
			var clone = $(".groupvariationcontainerwrapper", $(".groupvariationcontainerClone")).clone();
			var decreasecount = parseInt(count)-1;
			var increase = parseInt(increasecount)+1;
			$('.removevariation', clone).addClass('removevariation_'+increase).attr('data-remove-icon', increase);
			$(".groupvariationcontainer", clone).addClass('groupvariationcontainer_'+increase);
			$('.variation_name', clone).attr('name', 'variation['+decreasecount+'][name]');
			$('.variation_value', clone).attr('name', 'variation['+decreasecount+'][value]');
			
			$('.addvariationcontent').append(clone);
			$(this).attr('data-variation-count',decreasecount);
			$(this).attr('data-variation-increase-count',increase);
			return false;
		});
		
		$('body').on('click', '.removevariation', function() {
			var obj = $(this);
			//$('.groupvariationcontainer_'+obj.attr('data-remove-icon')).parent().remove();
			$(this).parent().parent().parent().remove();
			return false;
		});
		
		$('body').on('click', '.removevariationdata', function() {
			var thisobj = $(this);
			var variationid = thisobj.attr('data-remove-variation-id');
			var url = document.getElementsByName('base_url')[0].getAttribute('content');
			url = url+'/inventory/removevariantbyid/'+variationid;
			
			$.ajax({
				type: "delete",
				url: url,
				data: {variationid: variationid},
				dataType: "json",
				success: function (response) {
				   if(response.status == 'success') {
					   $('.groupvariationcontainer_'+response.variantid).remove();
				   }
				}
			});
			return false;
		});
		
		$('body').on('change', ".optionCloneEach", function() {
			var key = $(this).attr('data-select-count');
			$('.selectBoxHidden_'+key).val($(this).val());
		});
		$('#price').keyup(function() {
			var val = $(this).val();
			var newPrice = val.replace(/[^0-9\.]/g, '');
			$(this).val(newPrice);
		});
		$('#msrp').keyup(function() {
			var val = $(this).val();
			var newPrice = val.replace(/[^0-9\.]/g, '');
			$(this).val(newPrice);
		});
		$('#quantity').keyup(function() {
			var val = $(this).val();
			var newPrice = val.replace(/[^0-9\.]/g, '');
			$(this).val(newPrice);
		});
		$('#unit_cost').keyup(function() {
			var val = $(this).val();
			var newPrice = val.replace(/[^0-9\.]/g, '');
			$(this).val(newPrice);
		});
		
        $('.optionContainer').on('click', '.romoveoption', function() {
            var key = $(this).attr('data-option');
            /* $('.selectBox_'+key).remove();
			$('.inputBox_'+key).remove();
            $(this).remove(); */
			var remove = $('.selectBox_'+key+' option:selected');
			
			$('.selectBox_'+key).parent().parent().parent().parent('.cloneContainer').remove();
            var len = $('.optionCloneEach').length;
            $('.selectBox_'+len).removeAttr('disabled');
			//$('.selectBox_'+len).append('<option value='+remove.val()+'>'+remove.text()+'</option>');
			return false;
        });
        
        $('.addoption').click(function() {
            var optionLength = $('.optionCloneOrg > option').length;
            var getOptionLength = $('.optionCloneEach').length;
            if(optionLength > getOptionLength) {
                $('.optionCloneEach').attr('disabled', 'disabled');
                var clone = $('.cloneContainer', $('.optionContainerClone')).clone();
                var pre = $(this).attr('data-option');
                $('.selectBox_'+pre).attr('disabled', 'disabled');
                var preValue = $('.selectBox_'+pre).val();

                var key = parseInt(pre)+1; 
                $(this).attr('data-option', key);
                $('.romoveoption', clone).attr('data-option', key);
                $('.selectclone', clone).addClass('selectBox_'+key).addClass('optionCloneEach');
				$('.selectcloneinput', clone).addClass('inputBox_'+key).addClass('inputCloneEach');
				
				$('.selectclone', clone).attr('data-select-count', key);
				
				//$('.selectclone', clone).attr('name', 'product_Identifier[name]['+key+']');
				//
				$('.selectBoxHidden', clone).attr('name', 'product_Identifier['+key+'][option]');
				$('.selectcloneinput', clone).attr('name', 'product_Identifier['+key+'][value]');
                
                var options = $('.optionCloneEach option:selected');
                options.each(function (index, value) {
					$('.selectclone option[value="' + $(this).val() + '"]', clone).remove();
                });
				
				$('.selectBoxHidden', clone).addClass('selectBoxHidden_'+key);
				$('.selectBoxHidden', clone).val($('.selectclone', clone).find(":selected").val());
				
                $('.optionContainer').append(clone);
                $(this).attr('data-count', $('.optionCloneEach').length);
            }
			return false;
        });
		
		$('.Skuaddoption').click(function() {
            var optionLength = $('.SkuoptionCloneOrg > option').length;
            var getOptionLength = $('.SkuoptionCloneEach').length;
            
                var clone = $('.SkucloneContainer', $('.SkuoptionContainerClone')).clone();
                var pre = $(this).attr('data-option');
                var preValue = $('.SkuselectBox_'+pre).val();

                var key = parseInt(pre)+1; 
                $(this).attr('data-option', key);
                $('.Skuromoveoption', clone).attr('data-option', key);
                $('.Skuselectclone', clone).addClass('SkuselectBox_'+key).addClass('SkuoptionCloneEach');
				$('.Skuselectcloneinput', clone).addClass('SkuinputBox_'+key).addClass('SkuinputCloneEach');
				
				$('.Skuselectclone', clone).attr('data-select-count', key);
				
				$('.SkuselectBoxHidden', clone).attr('name', 'SKU_aliases['+key+'][option]');
				$('.Skuselectcloneinput', clone).attr('name', 'SKU_aliases['+key+'][value]');
                
				
				$('.SkuselectBoxHidden', clone).addClass('SkuselectBoxHidden_'+key);
				$('.SkuselectBoxHidden', clone).val($('.Skuselectclone', clone).find(":selected").val());
				
                $('.SkuoptionContainer').append(clone);
                $(this).attr('data-count', $('.SkuoptionCloneEach').length);
            
			return false;
        });
		
		$('.SkuoptionContainer').on('click', '.Skuromoveoption', function() {
            var key = $(this).attr('data-option');
			var remove = $('.SkuselectBox_'+key+' option:selected');
			$('.SkuselectBox_'+key).parent().parent().parent().parent('.SkucloneContainer').remove();
            var len = $('.SkuoptionCloneEach').length;
            $('.SkuselectBox_'+len).removeAttr('disabled');
			return false;
        });
        
    });
	
	$(function () {
		$("#fileupload").change(function () {
			if (typeof (FileReader) != "undefined") {
				var dvPreview = $("#dvPreview");
				dvPreview.html("");
				var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
				$($(this)[0].files).each(function (i) {
					
					var file = $(this);
					if (regex.test(file[0].name.toLowerCase())) {
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = $("<img />");
							/* if(i==0) {
								img.attr("style", "height:150px;width: 150px;margin-left:5px");
							} else {
								
								img.attr("style", "height:80px;width: 80px;margin-left:5px");
							} */
							img.attr("style", "height:80px;width: 80px;margin-left:5px");
							img.attr("src", e.target.result);
							dvPreview.append(img);
						}
						reader.readAsDataURL(file[0]);
					} else {
						alert(file[0].name + " is not a valid image file.");
						dvPreview.html("");
						return false;
					}
				});
			} else {
				alert("This browser does not support HTML5 FileReader.");
			}
		});
	});
	
	/* $("#inventory-form").submit(function() {
			var product_name = $("#product_name");
			var price = $("#price");
			var msrp = $("#msrp");
			var quantity = $("#quantity");
			var unit_cost = $("#unit_cost");
			
			product_name.parent().parent().removeClass('has-error');
			var error = false;
			if(product_name.val() == '') {
				product_name.parent().parent().addClass('has-error');
				error = true;
			}
			//has-error
			if(error == true) {
				return false;
			}
			
		}); */
</script>
@endpush