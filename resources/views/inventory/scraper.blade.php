@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Scraper</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-inventory-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inventory-prod">
					<div class="tory-left">
						<ul class="breadcrumb">
							<li><a class="breadcrumb-item" href="#">Inventory</a></li>
							<li><a class="breadcrumb-item" href="#">Product Scraper</a></li>
						</ul>

					</div>
					<br><br>
					<!--table-part-->
					<form method="post" action="<?php echo url('inventory/create'); ?>" id="inventory-form">
					
						<div class="flash-message">
							@foreach (['danger', 'warning', 'success', 'info'] as $msg)
							  @if(Session::has('alert-' . $msg))
								<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
							  @endif
							@endforeach
						 </div>
						 
						<div class="col-sm-6">
						  <div class="panel panel-default">
							<div class="panel-body">
							  <select class="form-control" name="source_market" id="shop" required>
									<option value="amazon_in" data-href="https://www.amazon.in/dp/">amazon.in</option>
									<option value="amazon_us" data-href="https://www.amazon.com/gp/product/">amazon.com</option>
									<option value="amazon_uk" data-href="https://www.amazon.co.uk/gp/product/">amazon.co.uk</option>
									<option value="amazon_ca" data-href="https://www.amazon.ca/dp/">amazon.ca</option>
									<option value="amazon_de" data-href="https://www.amazon.de/gp/product/">amazon.de</option>
									<option value="amazon_fr" data-href="https://www.amazon.fr/dp/">amazon.fr</option>
									<option value="amazon_it" data-href="https://www.amazon.it/dp/">amazon.it</option>
									<option value="amazon_es" data-href="https://www.amazon.es//dp/">amazon.es</option>
								<?php /*  ?>
									<option value="aliexpress_us">aliexpress.com</option>
								
									<option value="bestbuy_us">bestbuy.com</option>
								
									<option value="bhphotovideo_us">bhphotovideo.com</option>
								
								  <option value="costco_us">costco.com</option>
								
								  <option value="kmart_us">kmart.com</option>
								
								  <option value="lowes_us">lowes.com</option>
								
								  <option value="overstock_us">overstock.com</option>
								
								  <option value="samsclub_us">samsclub.com</option>
								
								  <option value="sears_us">sears.com</option>
								
								  <option value="walmart_us">walmart.com</option>
								<?php  */ ?>
								
							  </select>
							</div>
						  </div>
						</div>
					
						<div class="col-sm-6">
							<div class="flash-message-product-error">
								
							</div>
						  <div class="panel panel-default">
							<div class="panel-heading">Product Ids <span class="loadimage"></span></div>
								<div class="panel-body"><span class="checkproductid"></span>
									<input type="text" id="producturl" name="producturl" class="form-control" required />
									<!--<textarea name="producturl" id="producturl" class="form-control" style="height:auto"></textarea>-->
									<button type="button" class="btn btn-primary searchurlcontentdata">
										Go
									</button>
								</div>
							</div>
						</div>
						<input type="hidden" id="submitform" value="0" />
						<div class="scrapProduct"></div>
						
						<div class="form-group inventory-submit-button hide">
							<div class="col-md-12">
								<button type="button" class="save-changes btn submitButton">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="scrapProductContainer" style="display:none">
	<div class="cloneContent">
		<div class="col-sm-6 title_html_content">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Title:</strong> <span class="title_html"></span></div>
			</div>
		</div>
		
		<div class="col-sm-6 desc_html_content">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Description:</strong> <span class=""></span></div>
				<span class="desc_html"></span>
			</div>
		</div>
		
		<div class="imagelist"></div>
		<div class="col-sm-6 price_html_content">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Price:</strong> <span class="price_html"></span></div>
			</div>
		</div>
		<div class="col-sm-6 price_html_content">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Category:</strong> <span class="category_html"></span></div>
			</div>
		</div>
		<div class="col-sm-6 quantity_html_content">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Quantity:</strong> <span class="quantity_html">1</span></div>
			</div>
		</div>
		
		<input type="hidden" id="title" class="form-control" />
		<input type="hidden" id="desc" class="form-control" />
		<input type="hidden" id="domain" class="form-control" />
		<input type="hidden" id="price" class="form-control" /> 
		<input type="hidden" id="totalprice" class="form-control" />
		<input type="hidden" id="store" class="form-control" />
		<input type="hidden" id="quantity" class="form-control" />
		<input type="hidden" id="url" class="form-control" />
		<input type="hidden" id="asin" class="form-control" />
		<div class="category"></div>
		<hr>
	</div>
</div>
<!-- End Container fluid  -->

@endsection

@push('css')
<style>

.modal {
	display:    none;
	position:   fixed;
	z-index:    1000;
	top:        0;
	left:       0;
	height:     100%;
	width:      100%;
	background: rgba( 255, 255, 255, .8 ) 
	url('http://i.stack.imgur.com/FhHRx.gif')
	50% 50%
	no-repeat;
}
body.loading {
	overflow: hidden;   
}
body.loading .modal {
	display: block;
}

</style>
@endpush
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script>

$(document).ready(function() {
/* var a = [ 'B077Q42J3F', 'B078W754D7', 'B078W754D7' ];

var unique = a.filter(function(itm, i, a) {
    return i == a.indexOf(itm);
});

alert(unique.length); */
/* var array = ["this is a string", "this is another string", "i like strings", "i like strings"] ;
function array_unique(array) {
    var unique = [];
    for ( var i = 0 ; i < array.length ; ++i ) {
        if ( unique.indexOf(array[i]) == -1 )
            unique.push(array[i]);
    }
    return unique.length;
}
alert(array_unique(array)); */

	/* $('#producturl').keyup(function() {
		if($(this).val() == '') {
			return false;
		}
		var value = $(this).val();
		var uniqueArr = [];
		var myArray = value.split(' ');
		alert(array_unique(myArray));
	});
	
	function array_unique(array) {
		var unique = [];
		for ( var i = 0 ; i < array.length ; ++i ) {
			if ( unique.indexOf(array[i]) == -1 )
				unique.push(array[i]);
		}
		return unique.length;
	} */
	
	/* $('#inventory-form').submit(function() {
		if($("#submitform").val() == 0) {
			return false;
		}
	}); */
	
	InventoryManager.init();
	$('#producturl').val('');
});
  $(function () {
    $('#myTab a:secound').tab('show')
  })
</script>
@endpush