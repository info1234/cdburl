@extends('layouts.app')

@section('content')

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Variations</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->

<div class="product-sec-page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="inven_prod">
                                <ul class="breadcrumb">
                                    <li><a class="breadcrumb-item" href="#">Inventory</a></li>
                                    <li><a class="breadcrumb-item" href="#">Product</a></li>
                                </ul>
                            </div>

                            <!--table-part-->
                            <div class="quick_fill">
                                <h5>Quick Filter</h5>
                                <div class="select_catagori">
                                    <p>Category</p>
                                    <select>
                                        <option> All category</option>
                                        <option>uncategory</option>
                                    </select>
                                </div>
                                <div class="dotted-border">
                                <div class="select_catagori">
                                    <p>Listed on</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Not Listed</a> </li>
                                        <li><a href="#">eBay</a> </li>
                                        <li><a href="#">Shopify</a> </li>
                                        <li><a href="#">Wish</a> </li>
                                        <li><a href="#">WooCommerce</a> </li>
                                    </ul>
                                </div>
                                <div class="select_catagori">
                                    <p>Supplier</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">1688</a> </li>
                                        <li><a href="#">Aliexpress</a> </li>
                                        <li><a href="#">Alibaba</a> </li>
                                        <li><a href="#">Amazon</a> </li>
                                        <li><a href="#">Banggood</a> </li>
                                        <li><a href="#">Chinabrands</a> </li>
                                        <li><a href="#"> Chinavasion</a> </li>
                                        <li><a href="#"> Dhgate</a> </li>
                                        <li><a href="#">eBay</a> </li>
                                        <li><a href="#"> Gearbest</a> </li>
                                        <li><a href="#">Taobao</a> </li>
                                        <li><a href="#"> Tmall</a> </li>
                                        <li><a href="#"> TMARTt</a> </li>
                                        <li><a href="#"> Walmart</a> </li>
                                        <li><a href="#"> Others</a> </li>
                                    </ul>
                                </div>
                                <div class="select_catagori">
                                    <p>Other</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Has Notes</a> </li>

                                    </ul>
                                </div>
                                </div>
                                <div class="select_catagori">
                                    <p>Last Modified</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Today</a> </li>
                                        <li><a href="#">7 Days</a> </li>
                                        <li><a href="#">30 Days</a> </li>
                                        <li><a href="#">Custom Range</a> </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="listing_crete">
                                <ul>
                                    <li>
                                        <a href="#">
                                          <select>
                                              <option>Create Listings</option>
                                              <option>Shopify</option>
                                          </select>
                                      </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <select>
                                                <option>Bulk Actions</option>
                                                <option>Duplicate</option>
                                                <option>Delete</option>
                                            </select>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="import_export">
                                <ul>

                                    <li class="aad_prodt"><a href="#">Add Products</a> </li>
                                </ul>
                            </div>
                            <div class="suppliers_sec">
                                <ul>
                                    <li class="active"><a href="#">All<span>1385</span></a> </li>
                                    <li><a href="#">Has Suppliers<span>1385</span></a> </li>
                                    <li><a href="#">No Suppliers<span>0</span></a> </li>
                                </ul>
                            </div>
                            <div class="sort_by">
                                <h6>Sort By</h6>
                                <ul>
                                    <li>
                                        <a href="#">
                                            <select>
                                                <option>Last Modified</option>
                                                <option>Creation Date</option>
                                                <option>Parents SKU</option>
                                                <option>Products Name</option>
                                            </select>
                                        </a>
                                    </li>
                                    <li><a href="#"><img src="{{asset('images/icon_z.png')}}"><img src="{{asset('images/icon_a.png')}}">  </a> </li>
                                </ul>
                            </div>
                            <div class="parent_sku">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <select>
                                                <option>Parents SKU</option>
                                                <option>SkU Aliases</option>
                                                <option>Product Name</option>
                                                <option>Produt Identifiers</option>
                                            </select>
                                        </a>
                                    </li>
                                    <li><input type="text" placeholder="Search">  </li>
                                </ul>
                            </div>
                            <!--table-part-->

                                    <div class="list_contentlist">
                                        <table class="alonetable">
                                            <tr>
                                                <th class="input_check"><input type="checkbox"> </th>
                                                <th class="image_left">Image</th>
                                                <th class="name_left">Name</th>
                                                <th class="dis_count">SKU</th>
                                                <th class="variation_right">Price</th>
                                                <th class="from_right">Unit Cost</th>
                                                <th class="time_right">Quality</th>
                                                <th class="modified_right">Modified</th>
                                                <th class="action_right">Action</th>

                                            </tr>

                                        </table>
                                        <table class="theme-tablebackgrnd">
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left"><img src="{{asset('images/tab_1.png')}}"> <a href="#">Notes</a> </td>
                                                <td class="name_left">
                                                    <a href="#">Funko Pop Games: Overwatch-Ana with Shrike Skin Exclusive Collectible Figure, Multicolor</a>
                                                    <p>.</p>

                                                    <ul>
                                                        <li>Uncategorized</li>
                                                        <li> Supplier: <span><a href="#">Amazon</a></span></li>
                                                    </ul>

                                                </td>
                                                <td class="dis_count">
                                                    <h6>10061</h6>

                                                </td>
                                                <td class="variation_right"><h6>10.99</h6> </td>
                                                <td class="from_right"><h6>10.99</h6> </td>
                                                <td class="time_right"><h6>3</h6></td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6>2018-07-03</h6>
                                                    <p>Last Modified</p>
                                                    <h6>2018-07-03</h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>

                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="theme-tablebackgrnd">
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left"><img src="{{asset('images/tab_2.png')}}"> <a href="#">Notes</a> </td>
                                                <td class="name_left">
                                                    <a href="#">RCA Mini Auto Power Outlet Dual USB Charger</a>
                                                    <p>1 Variations</p>

                                                    <ul>
                                                        <li>Uncategorized</li>
                                                        <li> Supplier: <span><a href="#">Amazon</a></span></li>
                                                    </ul>

                                                </td>
                                                <td class="dis_count">
                                                    <h6>10062</h6>

                                                </td>
                                                <td class="variation_right"><h6>17.87</h6> </td>
                                                <td class="from_right"><h6>17.87</h6> </td>
                                                <td class="time_right"><h6>3</h6></td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6>2018-07-03</h6>
                                                    <p>Last Modified</p>
                                                    <h6>2018-07-03</h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>

                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="theme-tablebackgrnd">
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left"><img src="{{asset('images/tab_1.png')}}"> <a href="#">Notes</a> </td>
                                                <td class="name_left">
                                                    <a href="#">[LFMB]Famous Brand Belt Men Top Quality Genuine Luxury Leather Belts for Men,Strap Male Metal Automatic Buckle</a>
                                                    <p>77 Variations</p>

                                                    <ul>
                                                        <li>Uncategorized</li>
                                                        <li> Supplier: <span><a href="#"> Aliexpress</a></span></li>
                                                    </ul>

                                                </td>
                                                <td class="dis_count">
                                                    <h6>10060</h6>

                                                </td>
                                                <td class="variation_right"><h6>5.82
                                                    ~9.89</h6> </td>
                                                <td class="from_right"><h6>5.82
                                                    ~9.89</h6> </td>
                                                <td class="time_right"><h6>231</h6></td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6>2018-07-03</h6>
                                                    <p>Last Modified</p>
                                                    <h6>2018-07-03</h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>

                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="theme-tablebackgrnd">
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left"><img src="{{asset('images/tab_1.png')}}"> <a href="#">Notes</a> </td>
                                                <td class="name_left">
                                                    <a href="#">100% cowhide genuine leather belt for men Strap male Smooth buckle vintage jeans cowboy Casual designer brand belt Free shipping</a>
                                                    <p>10 Variations</p>

                                                    <ul>
                                                        <li>Uncategorized</li>
                                                        <li> Supplier: <span><a href="#"> Aliexpress</a></span></li>
                                                    </ul>

                                                </td>
                                                <td class="dis_count">
                                                    <h6>10056</h6>

                                                </td>
                                                <td class="variation_right"><h6>10.24</h6> </td>
                                                <td class="from_right"><h6>10.24</h6> </td>
                                                <td class="time_right"><h6>18</h6></td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6>2018-07-03</h6>
                                                    <p>Last Modified</p>
                                                    <h6>2018-07-03</h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li> <a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="theme-tablebackgrnd">
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left"><img src="{{asset('images/tab_1.png')}}"> <a href="#">Notes</a> </td>
                                                <td class="name_left">
                                                    <a href="#">
                                                        Men's canvas clasp Colorful Keychain Military nylon metal button Oyster mouth shape Paste belts Cool alloy buckle men eagle belt</a>
                                                    <p>3 Variations</p>

                                                    <ul>
                                                        <li>Uncategorized</li>
                                                        <li> Supplier: <span><a href="#"> Aliexpress</a></span></li>
                                                    </ul>

                                                </td>
                                                <td class="dis_count">
                                                    <h6>10057</h6>

                                                </td>
                                                <td class="variation_right"><h6>3.57</h6> </td>
                                                <td class="from_right"><h6>3.57</h6> </td>
                                                <td class="time_right"><h6>9</h6></td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6>2018-07-03</h6>
                                                    <p>Last Modified</p>
                                                    <h6>2018-07-03</h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a></li>

                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                            <!--pagination-part-->

                            <div class="stored-in">
                                <span class="products-here">
                                      <p>Products here are stored in ShopMaster for 30 days</p>
                                </span>
                                <span class="pagnation-part">
                                    <p>1-50 of 110</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-step-backward" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"> <i class="fa fa-trash" aria-hidden="true"></i></a> </li>

                                         <li><a href="#">1/3</a> </li>
                                         <li><a href="#">Go to</a> </li>
                                         <li><a href="#">View
                                             <select>

                                                 <option>50</option>
                                                 <option>40</option>
                                                 <option>30</option>

                                             </select>Per page</a> </li>

                                    </ul>
                                </span>


                            </div>

                        </div>
                    </div>
                </div>
            </div>

@endsection
