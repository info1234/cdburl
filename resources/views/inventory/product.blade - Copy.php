@extends('layouts.app')

@section('content')

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Product</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="product-sec-page">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="inven_prod">
                                <ul class="breadcrumb">
                                    <li><a class="breadcrumb-item" href="#">Inventory</a></li>
                                    <li><a class="breadcrumb-item" href="#">Product</a></li>
                                </ul>
                            </div>

                            <!--table-part-->
                            <div class="quick_fill">
                                <h5>Quick Filter</h5>
                                <div class="select_catagori">
                                    <p>Category</p>
                                    <select>
                                        <option> All category</option>
                                        <option>uncategory</option>
                                    </select>
                                </div>
                                <div class="dotted-border">
                                <div class="select_catagori">
                                    <p>Product Type</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Standalone</a> </li>
                                        <li><a href="#">Parent</a> </li>
                                    </ul>
                                </div>
                                <div class="select_catagori">
                                    <p>Listed on</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Not Listed</a> </li>
                                        <li><a href="#">eBay</a> </li>
                                        <li><a href="#">Shopify</a> </li>
                                        <li><a href="#">Wish</a> </li>
                                        <li><a href="#">WooCommerce</a> </li>
                                    </ul>
                                </div>
                                <div class="select_catagori">
                                    <p>Supplier</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">1688</a> </li>
                                        <li><a href="#">Aliexpress</a> </li>
                                        <li><a href="#">Alibaba</a> </li>
                                        <li><a href="#">Amazon</a> </li>
                                        <li><a href="#">Banggood</a> </li>
                                        <li><a href="#">Chinabrands</a> </li>
                                        <li><a href="#"> Chinavasion</a> </li>
                                        <li><a href="#"> Dhgate</a> </li>
                                        <li><a href="#">eBay</a> </li>
                                        <li><a href="#"> Gearbest</a> </li>
                                        <li><a href="#">Taobao</a> </li>
                                        <li><a href="#"> Tmall</a> </li>
                                        <li><a href="#"> TMARTt</a> </li>
                                        <li><a href="#"> Walmart</a> </li>
                                        <li><a href="#"> Others</a> </li>
                                    </ul>
                                </div>
                                <div class="select_catagori">
                                    <p>Other</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Has Notes</a> </li>

                                    </ul>
                                </div>
                                </div>
                                <div class="select_catagori">
                                    <p>Last Modified</p>
                                    <ul>
                                        <li><a href="#"><span>All</span></a> </li>
                                        <li><a href="#">Today</a> </li>
                                        <li><a href="#">7 Days</a> </li>
                                        <li><a href="#">30 Days</a> </li>
                                        <li><a href="#">Custom Range</a> </li>
                                    </ul>
                                </div>
								
                            </div>
                            <div class="listing_crete">
                                <ul>
                                    <li>
                                        <a href="#">
                                          <select>
                                              <option>Create Listings</option>
                                              <option>Shopify</option>
                                          </select>
                                      </a>
                                    </li>
                                    <li><a href="#">Bulk Edit</a> </li>
                                    <li>
                                        <a href="#">
                                            <select>
                                                <option>Bulk Actions</option>
                                                <option>Duplicate</option>
                                                <option>Delete</option>
                                            </select>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <select>
                                                <option>Move Category</option>
                                                <option>Uncategorized</option>
                                                <option>gjflld</option>
                                            </select>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="import_export">
                                <ul>
                                    <li><a href="#" class="importModalOpen"><img src="{{asset('images/import.png')}}" >Import</a> </li>
                                    <li><a href="{{ url('/inventory/export') }}"><img src="{{asset('images/export.png')}}" >Export</a> </li>
                                    <li class="aad_prodt"><a href="{{ url('/inventory/add') }}">Add Products</a> </li>
                                </ul>
                            </div>
                            <div class="suppliers_sec">
                                <ul>
                                    <li class="active"><a href="#">All<span>56</span></a> </li>
                                    <li><a href="#">Has Suppliers<span>56</span></a> </li>
                                    <li><a href="#">No Suppliers<span>0</span></a> </li>
                                </ul>
                            </div>
                            <div class="sort_by">
                                <h6>Sort By</h6>
                                <ul>
                                    <li>
                                        
                                            <select name="sort_by" id="sort_by">
                                                <option value="updated_at">Last Modified</option>
                                                <option value="created_at">Creation Date</option>
                                                <option value="sku">Parents SKU</option>
                                                <option value="name">Products Name</option>
                                            </select>
                                        
                                    </li>
                                    <li><a href="#"><img src="{{asset('images/icon_z.png')}}"><img src="{{asset('images/icon_a.png')}}">  </a> </li>
                                </ul>
                            </div>
                            <div class="parent_sku">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <select>
                                                <option>Parents SKU</option>
                                                <option>SkU Aliases</option>
                                                <option>Product Name</option>
                                                <option>Produt Identifiers</option>
                                            </select>
                                        </a>
                                    </li>
                                    <li><input type="text" placeholder="Search">  </li>
                                </ul>
                            </div>
							<form id="search-form" method="post">
								<input type="hidden" name="sort_by_search" id="sort_by_search" />
							</form>
                            <!--table-part-->

                                    <div class="list_contentlist">
                                        <table class="alonetable" id="myTable">
											<thead>
                                            <tr>
                                                <th class="input_check"><input type="checkbox"> </th>
                                                <th class="image_left">Image</th>
                                                <th class="name_left">Name</th>
                                                <th class="dis_count">SKU</th>
                                                <th class="variation_right">Price</th>
                                                <th class="from_right">Unit Cost</th>
                                                <th class="time_right">Quantity</th>
                                                <th class="modified_right">Modified</th>
                                                <th class="action_right">Action</th>

                                            </tr>
											</thead>
											<tbody>
											<?php 
												if(!empty($products)) {
												foreach($products as $productObj) { ?>
                                            <tr>
                                                <td class="input_check"><input type="checkbox"></td>
                                                <td class="image_left">
													<?php if($productObj->image != null) {
															if($productObj->image_upload_type == 1) {
																$image = $productObj->image;
															} else {
																$image = 'images/product/'.$productObj->image;
															}
															
													?>
														<img src="{{asset($image)}}">
													<?php } ?>
													<a href="#">Notes</a> </td>
                                                <td class="name_left">
                                                    <a href="<?php echo url('inventory/edit/'.$productObj->id); ?>"><?php echo $productObj->name; ?></a>
                                                    <!--<p>3 Variations</p>-->
													<!--
                                                    <ul>
                                                        <li><?php //echo getCategoryById($productObj->category)->name; ?></li>
                                                        <li> Supplier: <span><a href="#"> Aliexpress</a></span></li>
                                                    </ul>
													-->
                                                </td>
                                                <td class="dis_count">
                                                    <h6><?php echo $productObj->sku; ?></h6>

                                                </td>
                                                <td class="variation_right"><h6><?php echo $productObj->price; ?></h6> </td>
                                                <td class="from_right"><h6><?php echo $productObj->unit_cost; ?></h6> </td>
                                                <td class="time_right"><h6><?php echo $productObj->quantity; ?></h6></td>
                                                <td class="modified_right">
                                                    <p>Creation Time</p>
                                                    <h6><?php echo $productObj->created_at; ?></h6>
                                                    <p>Last Modified</p>
                                                    <h6><?php echo $productObj->updated_at; ?></h6>
                                                </td>
                                                <td class="action_right">
                                                    <ul>
                                                        <li> <a href="<?php echo url('inventory/edit/'.$productObj->id); ?>"> <i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                        <li><a href="#"> <i class="fa fa-shopping-bag" aria-hidden="true"></i></a></li>
                                                        <li><a href="#" class="duplicateproduct" data-product-id="<?php echo $productObj->id; ?>"> <i class="fa fa-files-o" aria-hidden="true"></i></a></li>
                                                        <li> <a href="#"> <i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
											<?php } } ?>
											</tbody>
                                        </table>
										
                                    </div>


                            <!--pagination-part-->
							@if ($products->hasMorePages())
                            <div class="stored-in">
                                <span class="products-here">
                                      <p>Products here are stored in ShopMaster for 30 days</p>
                                </span>
                                <span class="pagnation-part">
                                    <p>1-50 of 110</p>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-step-backward" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
                                         <li><a href="#"><i class="fa fa-step-forward" aria-hidden="true"></i></a> </li>
                                         <li><a href="#">1/3</a> </li>
                                         <li><a href="#">Go to</a> </li>
                                         <li><a href="#">View
                                             <select>

                                                 <option>50</option>
                                                 <option>40</option>
                                                 <option>30</option>

                                             </select>Per page</a> </li>

                                    </ul>
                                </span>


                            </div>
							@endif
                        </div>
                    </div>
                </div>
            </div>
<!-- Modal -->

	<div class="modal fade" id="myModalImport" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title">Import Products</h4>
			  <button type="button" class="close myModalImportdismiss" data-dismiss="modal">&times;</button>
			</div>
			<form id="inventory-import-form" action="<?php echo url('inventory/import'); ?>" enctype="multipart/form-data" method="POST">
				<div class="modal-body">
					<p>Choose file for upload</p>
					<p><input type="file" id="importfile" name="import" /></p>
					Download a <a href="<?php echo url('inventory/sample'); ?>">sample Excel template</a> to see an example of the format required.
				</div>
				<div class="modal-footer">
				  <button type="submit" class="btn btn-default">Import</button>
				  <button type="button" class="btn btn-danger btn-simple closeModalImport" data-dismiss="modal">Close</button>
				</div>
				</div>
			</form>
		  </div>
		  
	</div>
	
	<div class="modal fade" id="modalduplicateproduct" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title">Duplicate Product</h4>
			  <button type="button" class="close myModalImportdismiss" data-dismiss="modal">&times;</button>
			</div>
			<form id="duplicateproduct-form" action="<?php echo url('inventory/duplicate'); ?>" method="POST">
				<div class="modal-body">
					<p>Confirm required information</p>
					<p>Name</p>
					
				</div>
				<div class="modal-footer">
				  <button type="submit" class="btn btn-default">Import</button>
				  <button type="button" class="btn btn-danger btn-simple closeModalImport" data-dismiss="modal">Close</button>
				</div>
				</div>
			</form>
		  </div>
		  
	</div>

@endsection

@push('css')
@endpush

@push('js')
<script type="text/javascript">
$(document).ready( function () {
	$('.importModalOpen').click(function() {
		$("#myModalImport").modal('show');
		return false;
	});
	
	/* $('.duplicateproduct').click(function() {
		$("#modalduplicateproduct").modal('show');
		return false;
	}); */
	
	$("#inventory-import-form").submit(function() {
		var filename = $("#importfile").val();
		/* var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
        } */
		if(filename == '') {
			alert('Please upload file.');
			return false;
		}
	});
	
	$('.closeModalImport').click(function() {
		location.reload();
		//$("#myModalImport").removeClass('show');
		//$(".modal-backdrop").css("background", "none");
		return false;
	});
	
	$('body').on('click', '.duplicateproduct', function() {
		return false;
		var productid = $(this).attr('data-product-id');
		var url = document.getElementsByName('base_url')[0].getAttribute('content');
		url = url+'/inventory/duplicate';
		
		$.ajax({
			type: "post",
			url: url,
			data: {productid: productid},
			dataType: "json",
			success: function (response) {
			   if(response.status == 'success') {
				   
			   }
			}
		});
		//location.reload();
		return false;
	});
});
</script>
@endpush