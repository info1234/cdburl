@extends('layouts.app')

@section('content')

<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Dashboard</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="inven_prod">
				  <ul class="nav nav-tabs" id="myTab" role="tablist">
				  <li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#dashboard" role="tab" aria-controls="dashboard">Dashboard</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#createlisting" role="tab" aria-controls="createlisting">Create Listing</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#listings" role="tab" aria-controls="listings">Listings</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#orders" role="tab" aria-controls="orders">Orders</a>
				  </li>
				</ul>

				<div class="tab-content">
				  <div class="tab-pane active" id="dashboard" role="tabpanel">
						<div style="padding:20px">
							Dashboard
						</div>
				  </div>
				  <div class="tab-pane" id="createlisting" role="tabpanel">
						<div style="padding:20px">
							Create Listings
							<form method="post" action="<?php echo url('inventory/create'); ?>" id="inventory-form">
								<div class="col-sm-6">
								  <div class="panel panel-default">
									<div class="panel-heading">Source Market</div>
									<div class="panel-body">
									  <select class="form-control" name="source_market" id="shop" required>
										<option value="" selected="" disabled="">Select Source Market</option>
										
										  <option value="amazon_us">amazon.com</option>
										
										  <option value="amazon_uk">amazon.co.uk</option>
										
										  <option value="amazon_ca">amazon.ca</option>
										
										  <option value="amazon_de">amazon.de</option>
										
										  <option value="amazon_fr">amazon.fr</option>
										
										  <option value="amazon_it">amazon.it</option>
										
										  <option value="amazon_in">amazon.in</option>
										
										  <option value="amazon_es">amazon.es</option>
										
										  <option value="aliexpress_us">aliexpress.com</option>
										
										  <option value="bestbuy_us">bestbuy.com</option>
										
										  <option value="bhphotovideo_us">bhphotovideo.com</option>
										
										  <option value="costco_us">costco.com</option>
										
										  <option value="kmart_us">kmart.com</option>
										
										  <option value="lowes_us">lowes.com</option>
										
										  <option value="overstock_us">overstock.com</option>
										
										  <option value="samsclub_us">samsclub.com</option>
										
										  <option value="sears_us">sears.com</option>
										
										  <option value="walmart_us">walmart.com</option>
										
										
									  </select>
									</div>
								  </div>
								</div>
							
								<div class="col-sm-6">
								  <div class="panel panel-default">
									<div class="panel-heading">Product <span class="loadimage"></span></div>
										<div class="panel-body">
											<input type="text" id="producturl" name="producturl" class="form-control" required /> 
											<button type="button" class="btn btn-primary searchurlcontentdata">
												Go
											</button>
										</div>
									</div>
								</div>
								
								<div class="col-sm-6 title_html_content hide">
									<div class="panel panel-default">
										<div class="panel-heading">Title: <span class="title_html"></span></div>
									</div>
								</div>
								
								<div class="col-sm-6 desc_html_content hide">
									<div class="panel panel-default">
										<div class="panel-heading">Description: <span class=""></span></div>
										<span class="desc_html"></span>
									</div>
								</div>
								
								<div class="imagelist"></div>
								<div class="col-sm-6 price_html_content hide">
									<div class="panel panel-default">
										<div class="panel-heading">Price: <span class="price_html"></span></div>
									</div>
								</div>
								<div class="col-sm-6 quantity_html_content hide">
									<div class="panel panel-default">
										<div class="panel-heading">Quantity: <span class="quantity_html">1</span></div>
									</div>
								</div>
								
								<input type="hidden" id="title" name="name" class="form-control" />
								<input type="hidden" id="desc" name="desc" class="form-control" />
								<input type="hidden" id="domain" name="domain" class="form-control" />

								<input type="hidden" id="price" name="price" class="form-control" /> 
								<input type="hidden" id="totalprice" name="totalprice" class="form-control" />
								<input type="hidden" id="store" name="store" class="form-control" />
								<input type="hidden" id="quantity" name="quantity" class="form-control" />
								<input type="hidden" id="url" name="url" class="form-control" />							
								<div class="form-group inventory-submit-button hide">
									<div class="col-md-12">
										<button type="submit" class="save-changes btn">Save</button>
									</div>
								</div>
							</form>
						</div>
				  </div>
				  
				  
				  <div class="tab-pane" id="listings" role="tabpanel">...</div>
				  <div class="tab-pane" id="orders" role="tabpanel">...</div>
				</div>

			</div>
			<!--table-part-->
			
			
		</div>
	</div>
</div>
@endsection


@push('css')
<style>

.modal {
	display:    none;
	position:   fixed;
	z-index:    1000;
	top:        0;
	left:       0;
	height:     100%;
	width:      100%;
	background: rgba( 255, 255, 255, .8 ) 
	url('http://i.stack.imgur.com/FhHRx.gif')
	50% 50%
	no-repeat;
}
body.loading {
	overflow: hidden;   
}
body.loading .modal {
	display: block;
}

</style>
@endpush
@push('js')
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script>
$(document).ready(function() {
	InventoryManager.init();
	$('#producturl').val('');
});
  $(function () {
    $('#myTab a:secound').tab('show')
  })
</script>
@endpush

