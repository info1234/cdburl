@extends('layouts.app')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles primary-bg-color">
	<div class="col-md-5 align-self-center">
		<h3 class="text-primary white-color">Order</h3> </div>
	<!-- <div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div> -->
</div>
 
 <div class="product-inventory-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="inventory-prod">
					<div class="tory-left">
						<ul class="breadcrumb">
							<li><a class="breadcrumb-item" href="#">Order</a></li>
							<li><a class="breadcrumb-item" href="#">Ready to Ship</a></li>
						</ul>

					</div>
				</div>


				<!--quick-filter-->
				<div class="quick-filter">
					<h5>Quick Filter</h5>
					<div class="import-from">
						<h6>Channels</h6>
						<ul>
							<li class="active"><a href="#">All</a> </li>
							<li><a href="#">eBay</a> </li>
							<li><a href="#"> Shopify</a> </li>
							<li><a href="#">Wish</a> </li>
							<li><a href="#">WooCommerce</a> </li>

						</ul>
					</div>
					<div class="import-time">
						<h6>My Stores</h6>
						<ul>
							<li class="active"><a href="#">All</a> </li>
							<li><a href="#">aarvik</a> </li>
						</ul>
					</div>
				</div>

				<div class="import_export">
					<ul>
						<li><a href="#"><img src="{{asset('images/import.png')}}" >Import</a> </li>
						<li><a href="#"><img src="{{asset('images/export.png')}}" >Exort</a> </li>
					</ul>
				</div>

				
				<div class="suppliers_sec"></div>
					<div class="sort_by">
						<h6>Sort By</h6>
						<ul>
							<li>
								<a href="#">
									<select>
										<option>Order ID</option>
										<option>Creation Time</option>
										<option>Recipient</option>
										<option>Paid Time</option>
										<option>Price</option>
										<option>Sku</option>
									</select>
								</a>
							</li>
							<li><a href="#"><img src="{{asset('images/icon_z.png')}}"><img src="{{asset('images/icon_a.png')}}">  </a> </li>
						</ul>
					</div>
					<div class="parent_sku">
						<ul>
							<li>
								<a href="#">
									<select>
										<option>Order ID</option>
										<option>SkU</option>
										<option>Recipient</option>
										<option>Buyer</option>
									</select>
								</a>
							</li>
							<li><input type="text" placeholder="Search">  </li>
						</ul>
					</div>

					<div class="list_contentlist">
						<table class="alonetable">
							<tr>
								<th class="input_check"><input type="checkbox"> </th>
								<th class="image_left">Item Details</th>
								<th class="name_left">Shipment</th>
								<th class="dis_count">Status</th>
								<th class="variation_right">Action</th>
							</tr>
						</table>
						<table class="theme-tablebackgrnd">
							<tr>
							THERE IS NO DATA TO DISPLAY
							</tr>
						</table>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection
