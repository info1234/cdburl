@extends('layouts.admin')

@section('content')
<?php
$name = $store_id = $couponType = $type = $text = $title = $coupontypePrice = $price =
$deal = $deal_id = '';
$button = 'Create';
$url = url('admin/coupon/create');
if($couponObj != null) {
	$title = $couponObj->title;
	$couponType = $couponObj->coupon_type;
	$text = $couponObj->text;
	$coupontypePrice = $couponObj->coupon_type_price;
	$button = 'Update';
	$store_id = $couponObj->store_id;
	$price = $couponObj->price;
	$deal = $couponObj->deal;
	//$deal_id = $couponObj->deal_id;
	$url = url('admin/coupon/edit', ['id' => $couponObj->id]);
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Coupon</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $button; ?> Coupon
					<a class="btn btn-link" href="<?php echo url('admin/coupon/index'); ?>">
						Coupon list
					</a>
				</div>
				
                <div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form role="form" class="form-horizontal" id="couponform" method="POST" enctype="multipart/form-data" action="<?php echo $url; ?>">
								{{ csrf_field() }}
								<div class="col-lg-12">
								
									<div class="form-group">
										<label>Title</label>
										<input class="form-control" type="text" name="title" value="{{ $title }}" />
									</div>
									
									<div class="form-group">
										<label>Store</label>
										<select class="form-control" name="store_id">
											<option value="">Select Store</option>
											<?php foreach($stores as $storeObj) { ?>
												<option value="<?php echo $storeObj->id; ?>" <?php if($store_id == $storeObj->id) { ?> selected <?php } ?>>
													<?php echo $storeObj->name; ?>
												</option>
											<?php } ?>
										</select>
									</div>
									
									<div class="form-group">
										<label>Image</label>
										<input class="form-control" type="file" name="image" />
									</div>
									
									<div class="form-group">
										<label>Price</label>
										<input class="form-control" type="text" name="price" value="{{ $price }}" />
									</div>
									
									<div class="form-group">
										<label>Deal</label>
										<input class="form-control" type="text" name="deal" value="{{ $deal }}" />
									</div>
									
									<div class="form-group">
										<label>Deal Price Type</label>
										<select class="form-control couponTypePrice" id="" name="coupon_type_price">
											<option value="">Select Coupon Price Type</option>
											<option value="Discount" <?php if($coupontypePrice == "Discount") { ?> selected <?php } ?>>Discount</option>
											<option value="Percentage" <?php if($coupontypePrice == "Percentage") { ?> selected <?php } ?>>Percentage </option>
										</select>
									</div>
									
									<div class="form-group">
										<label>Coupon Type</label>
										<select class="form-control" name="coupon_type">
											<option value="">Select Coupon Type</option>
											<?php 
											$couponCons = array(1 => 'COUPON', 2 => 'SALES & OFFERS');
											foreach($couponCons as $key => $val) { ?>
												<option value="<?php echo $key; ?>" <?php if($key == $couponType) { ?> selected <?php } ?>>
													<?php echo $val; ?>
												</option>
											<?php } ?>
										</select>
									</div>
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" id="txtEditor" name="text"><?php echo $text; ?></textarea>
									</div>
									
									<div class="form-group">
										<button type="submit" class="btn btn-primary">
											<?php echo $button; ?>
										</button>
									</div>
								
								</div>
								  <?php /* ?>
								  <tr>
									<th>Deal</th>
									<th>
										<select class="form-control deal_coupon" name="deal_id">
											<option value="">Select Deal</option>
											<?php foreach($deals as $dealObj) { ?>
												<option value="<?php echo $dealObj->id; ?>" <?php if($deal_id == $dealObj->id) { ?> selected <?php } ?>>
													<?php echo $dealObj->name; ?>
												</option>
											<?php } ?>
										</select>
									</th>
								  </tr>
								  <?php */ ?>
								  
							 </form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/editor.css') }}">
@endpush

@push('scripts')
	<script type="text/javascript" src="{{ URL::asset('js/editor.js') }}"></script>
	<script type="text/javascript">
		$( document ).ready(function() {
			CouponManager.init();
			<?php if($couponObj != null) { ?>
				var text = '<?php echo $couponObj->text; ?>';
				$("#txtEditor").Editor();
				$("#txtEditor").Editor("setText", text);
			<?php } else { ?>
				$("#txtEditor").Editor();
			<?php } ?>
		});
	</script>
@endpush