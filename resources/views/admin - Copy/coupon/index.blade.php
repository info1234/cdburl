@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Coupon</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Coupon
					<a class="btn btn-link" href="<?php echo url('admin/coupon/create'); ?>">
						Coupon Create
					</a>
				</div>
				
                <div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($coupons as $obj) { ?>
									<tr>
										<td><?php echo $obj->title; ?></td>
										<td>
											<a class="btn btn-link" href="<?php echo url('admin/coupon/edit', ['id' => $obj->id]); ?>">
											Edit
											</a>
											<a class="btn btn-link" href="<?php echo url('admin/coupon/delete', ['id' => $obj->id]); ?>">
											Delete
											</a>
										</td>
									</tr>
							  <?php } ?>
							</tbody>
						</table>
						<?php /* $coupons->links() */ ?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection