@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Store</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Store
				<a class="btn btn-link" href="<?php echo url('admin/store/create'); ?>">
					Create
				</a>
				</div>
				
                <div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Store Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Country</th>
									<th>State</th>
									<th>City</th>
									<th>Code</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							  <?php foreach($stores as $obj) { ?>
								  <tr>
									<td><?php echo $obj->name; ?></td>
									<td><?php echo $obj->email; ?></td>
									<td><?php echo $obj->phone_number; ?></td>
									<td><?php echo $obj->county; ?></td>
									<td><?php echo $obj->state; ?></td>
									<td><?php echo $obj->city; ?></td>
									<td><?php echo $obj->code; ?></td>
									<td>
										<a class="btn btn-link" href="<?php echo url('admin/store/edit', ['id' => $obj->id]); ?>">
											Edit
										</a>
									</td>
								  </tr>
							  <?php } ?>
							</tbody>
						</table>
						<?php /* $stores->links() */ ?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection