@extends('layouts.admin')

@section('content')
<?php
$name = $description = $email = $address =
$state = $city = $code = $phonenumber = $county = '';
$button = 'Create';
$url = url('admin/store/create');
if($storeObj != null) {
	$name = $storeObj->name;
	$description = $storeObj->description;
	$email = $storeObj->email;
	$address = $storeObj->address;
	$state = $storeObj->state;
	$city = $storeObj->city;
	$code = $storeObj->code;
	$phonenumber = $storeObj->phone_number;
	$county = $storeObj->county;
	$button = 'Update';
	$url = url('admin/store/edit', ['id' => $storeObj->id]);
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Store</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $button; ?> Store
					<a class="btn btn-link" href="<?php echo url('admin/store/index'); ?>">
						Store list
					</a>
				</div>
				
                <div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form role="form" enctype="multipart/form-data" class="form-horizontal" method="POST" action="<?php echo $url; ?>">
								{{ csrf_field() }}
								<div class="col-lg-12">
								
									<div class="form-group">
										<label>Store Name</label>
										<input class="form-control" type="text" name="name" value="{{ $name }}" />
									</div>
									
									<div class="form-group">
										<label>Email</label>
										<input class="form-control" type="email" name="email" value="{{ $email }}" />
									</div>
									
									<?php if($storeObj == null) { ?>
										<div class="form-group">
											<label>password</label>
											<input class="form-control" id="password" type="password" class="form-control" name="password" required>
										</div>
									<?php } ?>
									
									<div class="form-group">
										<label>Phone Number</label>
										<input class="form-control" type="phone" name="phone" value="{{ $phonenumber }}" />
									</div>
									
									<div class="form-group">
										<label>Country</label>
										<input class="form-control" type="county" name="county" value="{{ $county }}" />
									</div>
									
									<div class="form-group">
										<label>State</label>
										<input class="form-control" type="state" name="state" value="{{ $state }}" />
									</div>
									
									<div class="form-group">
										<label>City</label>
										<input class="form-control" type="city" name="city" value="{{ $city }}" />
									</div>
									
									<div class="form-group">
										<label>Code</label>
										<input class="form-control" type="code" name="code" value="{{ $code }}" />
									</div>
									
									<div class="form-group">
										<label>Address</label>
										<input class="form-control" type="address" name="address" value="{{ $address }}" />
									</div>
									
									<div class="form-group">
										<label>Image</label>
										<input class="form-control" type="file" name="image" />
									</div>
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" rows="4" cols="50" name="description">
											<?php echo $description; ?>
										</textarea> 
									</div>
									
									<div class="form-group">
										<button type="submit" class="btn btn-primary">
											<?php echo $button; ?>
										</button>
									</div>
									
								</div>
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		CategoryManager.init();
	</script>
@endpush