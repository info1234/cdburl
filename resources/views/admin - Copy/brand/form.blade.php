@extends('layouts.admin')

@section('content')
<?php
$name = '';
$button = 'Create';
$url = url('admin/brand/create');
if($brandObj != null) {
	$name = $brandObj->name;
	$button = 'Update';
	$url = url('admin/brand/edit', ['id' => $brandObj->id]);
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Brand</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $button; ?> Brand
					<a class="btn btn-link" href="<?php echo url('admin/brand/index'); ?>">
						Brand list
					</a>
				</div>
				
                <div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form role="form" class="form-horizontal" method="POST" action="<?php echo $url; ?>">
								{{ csrf_field() }}
								<div class="col-lg-12">
								
									<div class="form-group">
										<label>Name</label>
										<input class="form-control" type="text" name="name" value="{{ $name }}" />
									</div>
									
									<div class="form-group">
										<button type="submit" class="btn btn-primary">
											<?php echo $button; ?>
										</button>
									</div>
								
								</div>
								
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		//CategoryManager.init();
	</script>
@endpush