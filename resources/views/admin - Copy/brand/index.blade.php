@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Brand</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Brand
					<a class="btn btn-link" href="<?php echo url('admin/brand/create'); ?>">
						Brand Create
					</a>
				</div>

                <div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>	
								<tr>
									<th>Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($brands as $obj) { ?>
									<tr>
										<td><?php echo $obj->name; ?></td>
										<td>
											<a class="btn btn-link" href="<?php echo url('admin/brand/edit', ['id' => $obj->id]); ?>">
												Edit
											</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php /* $brands->links() */ ?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection