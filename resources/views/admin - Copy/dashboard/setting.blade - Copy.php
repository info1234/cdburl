@extends('layouts.admin')

@section('content')
<div id="page-wrapper">
	<div class="col-lg-12">
		<h1 class="page-header">Setting</h1>
	</div>
	<form role="form" id="settingform" class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url('/admin/setting'); ?>">
		<?php /*  ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
								@if(session()->has('header_logo_image'))
									<div class="alert alert-success">
										{{ session()->get('header_logo_image') }}
									</div>
								@endif
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Upload Header Image</label> <?php if($headerlogoimage != null) { ?><img src="<?php echo url('public/images/logo/header/'.$headerlogoimage->value); ?>" height="50" /> <?php } ?>
								<input class="form-control" type="file" name="image" value="" />
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
		<?php  */ ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
				
					<div class="panel-body">
						
						<div class="col-lg-12">
							<div class="form-group">
								<label>Advertisement</label>
							</div>
						</div>
						<div class="col-lg-12">
						@if(session()->has('success_deal_page_advertisement'))
							<div class="alert alert-success">
								{{ session()->get('success_deal_page_advertisement') }}
							</div>
						@endif
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Page</label>
								<select name="pageoption" class="form-control pageoption">
									<option value="">Select Advertisement Page</option>
									<option value="1">Home Page Advertisement</option>
									<option value="2">Deal Page Advertisement</option>
								</select>
							</div>
						</div>
						<?php $homeAddObj = \App\Setting::select("value", "text")->where("setting_key", "home_page_add")->first();
							$homeAddText = '';
							$homeAdvertisementTitle = '';
							if($homeAddObj != null) {
								//$homeAddText = $homeAddObj->text;
								//$homeAdvertisementTitle = $homeAddObj->value;
							}
						?>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Title</label>
								<input type="text" class="form-control" value="<?php echo $homeAdvertisementTitle; ?>" name="homeadvertisementtitle" />
							</div>
						</div>
						
						<?php $categories = \App\Category::leftjoin('categories AS P', 'categories.id', '=', 'P.parent_id')
								 ->select('categories.id', 'categories.name', 'P.id as pid')
								 ->whereNull('categories.parent_id')
								 ->groupBy('categories.id')
								 ->get(); ?>
						
							<div class="form-group categoryoptioncontainer" style="display:none">
								<label style="float:left;">Category</label>
								<ul class="nav navbar-nav" style="float:left;list-style-type: none; ">
									<li>
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Select Category <b class="caret"></b>
										<span class="categoryOptionTargetSpan"></span></a>
										<ul class="dropdown-menu multi-level">
											<?php foreach($categories as $categoryObj) {
													if($categoryObj->pid == null) {
											?>
													<li><a href="#" data-cat="0" categoryid="<?php echo $categoryObj->id; ?>" class="categoryOption"><?php echo $categoryObj->name; ?></a></li>
													<?php } else { ?>
														<li class="dropdown-submenu">
															<a href="#" class="dropdown-toggle dropDownTarget" data-cat="<?php echo $categoryObj->id; ?>" data-url="<?php echo url("admin/deal/getcategorybyId", ["id" => $categoryObj->id]); ?>" data-toggle="dropdown"><?php echo $categoryObj->name; ?></a>
														</li>
													<?php } ?>
											<?php } ?>
											
										</ul>
									</li>
									<input type="hidden" name="category" value="" class="categoryOptionTargetInput" />
								</ul>
							</div>
						
						<div class="col-lg-12">
							<div class="form-group">
								<label>Description</label>
								
								<!--<textarea class="form-control" id="txtEditor" name="uploadhomeimage"></textarea>-->
								<textarea class="form-control ckeditor myckeditor" id="page_article" name="text"><?php echo $homeAddText; ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary">
			Save
		</button>
	</form>
		
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/editor.css') }}">
	<link href="{{ asset('public/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<?php /*<script type="text/javascript" src="{{ URL::asset('js/editor.js') }}"></script>-->
<!--<script src="http://cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script> <script src="{{ asset('public/js/ckeditor.js') }}"></script> */ ?>
<script src="http://cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
<script>

$(document).ready(function() {
	DealManager.init();
	$('body').on('change', '.pageoption', function(e) {
		var value = $(this).val();
		$(".categoryoptioncontainer").attr('style', 'display:none');
		if(value == 2) {
			$(".categoryoptioncontainer").attr('style', '');
		}
	});
    CKEDITOR.replace('myckeditor');  
    CKEDITOR.on('dialogDefinition', function(ev) {
        //dialogDefinition is a ckeditor event it's fired when ckeditor dialog instance is called  
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        if (dialogName == 'image') { //dialogName is name of dialog and identify which dialog is fired.  
            var infoTab = dialogDefinition.getContents('info'); // get tab of the dialog  
            var browse = infoTab.get('browse'); //get browse server button  
            browse.onClick = function() {  
                //here we can invoke our custom fileuploader or server files popup  
                alert('open your file uploader or server files popup');  
            };  
        }  
	});
});

/* CKEDITOR.on( 'dialogDefinition', function( evt ) {
    var dialog = evt.data;

    if ( dialog.name == 'link' ) {
        // Get dialog definition.
        var def = evt.data.definition;

        // Add some stuff to definition.
        def.addContents( {
            id: 'custom',
            label: 'My custom tab',
            elements: [
                {
                    id: 'myField1',
                    type: 'text',
                    label: 'My Text Field'
                },
                {
                    id: 'myField2',
                    type: 'text',
                    label: 'Another Text Field'
                }
            ]
        });

    }
} );

CKEDITOR.replace( 'ckeditor' ); */
/* CKEDITOR.on('dialogDefinition', function(ev) {

    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if (dialogName == 'image') {
        dialogDefinition.onShow = function () {
            // This code will open the Upload tab.
            this.selectPage('Upload');
        };
    }
});

CKEDITOR.replace( 'text',
    {
        filebrowserBrowseUrl : '/browser/browse.php',
		filebrowserImageBrowseUrl : '/browser/browse.php?type=Images',
		filebrowserUploadUrl : '/uploader/upload.php', //document.getElementsByName('base_url')[0].getAttribute('content')+'/admin/upload/
		filebrowserImageUploadUrl : '/uploader/upload.php?type=Images'
    }); */
/* CKEDITOR.replace('textarea', {
		filebrowserImageUploadUrl: '/upload'
	});

	CKEDITOR.on('dialogDefinition', function(ev) {
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;

		if (dialogName == 'image') {
			dialogDefinition.onLoad = function() {
				var dialog = CKEDITOR.dialog.getCurrent();

				var uploadTab = dialogDefinition.getContents('Upload');
				var uploadButton = uploadTab.get('uploadButton');
				console.log('uploadButton', uploadButton);

				uploadButton['onClick'] = function(evt){
					console.log('fire in the hole', evt);
				}

				uploadButton['filebrowser']['onSelect'] = function(fileUrl, errorMessage) {
					console.log('working');
				}
			};

		}

	}); */
	/* CKEDITOR.on('dialogDefinition', function(e) {
		if (e.data.name == 'image') {
			var dialog = e.data.definition;
			oldOnShow = dialog.onShow;
			dialog.onShow = function() {
				 oldOnShow.apply(this, arguments);
				 this.selectPage('Upload');
			};
		}
	}); */ 
/* CKEDITOR.on('dialogDefinition', function(ev) {
    var editor = ev.editor;
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if (dialogName == 'image') {
        var infoTab = dialogDefinition.getContents( 'info' );
        infoTab.remove( 'txtBorder' ); //Remove Element Border From Tab Info
        infoTab.remove( 'txtHSpace' ); //Remove Element Horizontal Space From Tab Info
        infoTab.remove( 'txtVSpace' ); //Remove Element Vertical Space From Tab Info
        infoTab.remove( 'txtWidth' ); //Remove Element Width From Tab Info
        infoTab.remove( 'txtHeight' ); //Remove Element Height From Tab Info

        //Remove tab Link
        dialogDefinition.removeContents( 'Link' );
    }
}); */
	/* $("#txtEditor").Editor();
	$("body").on('submit', '#settingform', function() {
		var text = $(".Editor-editor").html();
		$("#txtEditor").val(text);
	}); */
	/* CKEDITOR.replace('text', {
        filebrowserBrowseUrl = '/elfinder/ckeditor',
        filebrowserImageBrowseUrl : '/elfinder/ckeditor',
        uiColor : '#9AB8F3',
        height : 300
    }); */
	

	/* CKEDITOR.on('dialogDefinition', function (ev) {
	  var dialogName = ev.data.name;
	  var dialog = ev.data.definition.dialog;

	  if (dialogName == 'image') {
		  dialog.on('show', function () {
			  this.selectPage('Upload');
		  });
	  }
	}); */
	
	
</script>
@endpush
