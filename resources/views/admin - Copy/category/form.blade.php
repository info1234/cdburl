@extends('layouts.admin')

@section('content')
<?php
$name = $description = $parent_id = $category_type = '';
$button = 'Create';
$url = url('admin/category/create');
if($categoryObj != null) {
	$name = $categoryObj->name;
	$description = $categoryObj->description;
	$parent_id = $categoryObj->parent_id;
	$category_type = $categoryObj->category_type;
	$button = 'Update';
	$url = url('admin/category/edit', ['id' => $categoryObj->id]);
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Category</h1>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $button; ?> Category
					<a href="<?php echo url('admin/category/index'); ?>">
						Category list
					</a>
				</div>
				
                <div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul style="list-style: none;">
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo $url; ?>">
								{{ csrf_field() }}
								<div class="col-lg-12">
								
									<div class="form-group">
										<label>Name</label>
										<input class="form-control" type="text" name="name" value="{{ $name }}" required />
									</div>
									
									<div class="form-group">
										<div class="radio">
											<label>
												<input name="category_type" class="categoryoptiontype" value="2" <?php if($category_type == 2) { ?>checked<?php } ?> type="radio">
												Parent Category
											</label>
										</div>
										<div class="radio">
											<label>
												<input name="category_type" class="categoryoptiontype" value="1" <?php if($category_type == 1) { ?>checked<?php } ?> type="radio">
												Sub Category
											</label>
										</div>
										
                                    </div>
									
									<div class="form-group">
										<label>Image</label>
										<input class="form-control" type="file" name="image" />
									</div>
									<?php $parentCategory = 'display:none';
										if($category_type == 1) {
											$parentCategory = '';
										}									
									?>
									<div class="form-group allparantcategory" style="<?php echo $parentCategory; ?>">
										<label>Parent</label>
										<select class="form-control" name="parent_id">
											<option value="">Select Parent</option>
											<?php 
											$categories = \App\Category::select('id', 'name')->get();
											foreach($categories as $parentCategoryObj) { ?>
											<option value="<?php echo $parentCategoryObj->id; ?>" <?php if($parent_id > 0 && $parent_id == $parentCategoryObj->id) { ?> selected <?php } ?>>
													<?php echo $parentCategoryObj->name; ?>
												</option>
											<?php } ?>
										</select>
									</div>
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" rows="4" cols="50" name="description">
											<?php echo $description; ?>
										</textarea>
									</div>
									
									<div class="form-group">
										<button type="submit" class="btn btn-primary" class="categoryadminsubmit">
											<?php echo $button; ?>
										</button>
									</div>
								
								</div>
								
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		CategoryManager.init();
	</script>
@endpush