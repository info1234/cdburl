@extends('layouts.admin')

@section('content')

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Category</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Category
					&nbsp;<a href="<?php echo url('admin/category/create'); ?>">
						Create Category
					</a>
				</div>
				
                <div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>	
								<tr>
									<th>Category</th>
									<th>Parent Category</th>
									<?php /* ?><th class="sorting_disabled">Feature Category</th><?php */ ?>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="category_listing">
								<form id="formId" method="post">
								<?php foreach($categories as $obj) {
										if($obj->name != 'Miscellaneous') {
								?>
									<tr>
										<td><?php echo $obj->name; ?></td>
										<td>
											<?php
												if($obj->parent_id > 0) {
													$categoryObj = \App\Category::find($obj->parent_id);
													if($categoryObj) {
														echo $categoryObj->name;
													}
												} ?>
										</td>
										<?php /* ?>
										<td>
											<input type="checkbox" class="featurecategory featurecategory<?php echo $obj->id; ?>" data-fkid="<?php echo $obj->id; ?>" name="feature_category[]" value="<?php echo $obj->feature_category; ?>" <?php if($obj->feature_category == 1) { ?> checked <?php } ?> />
										</td>
										<?php */ ?>
										<td>
										<?php /* ?>
											<a class="btn btn-link deleteCategory" href="<?php echo url('admin/category/delete', ['id' => $obj->id]); ?>">
												Delete
											</a>
											<?php */ ?>
											<a class="" href="<?php echo url('admin/category/edit', ['id' => $obj->id]); ?>">
												Edit
											</a>
										</td>
									</tr>
										<?php }} ?>
								</form>
							</tbody>
						</table>
						<?php /* $categories->links() */ ?>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		CategoryManager.init();
	</script>
@endpush