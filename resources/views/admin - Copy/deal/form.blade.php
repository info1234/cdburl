@extends('layouts.admin')

@section('content')
<?php

$name = $description = $category_id = $store_id = $storename = $categoryname =
$dealtypePrice = $price = $dealprice = $dealType = $brand_id = $shipping = $urlsite = $total = '';
$button = 'Create';
$url = url('admin/category/create');
if($dealObj != null) {
	$category_id = $dealObj->category_id;
	$urlsite = $dealObj->url;
	$dealprice = $dealObj->deal;
	$store_id = $dealObj->store_id;
	$categoryname = $dealObj->category->name;
	$dealtypePrice = $dealObj->deal_type_price;
	$description = $dealObj->description;
	$name = $dealObj->name;
	$price = $dealObj->price;
	$total = $dealObj->total;
	$dealType = $dealObj->deal_type;
	$brand_id = $dealObj->brand_id;
	$shipping = $dealObj->shipping;
	$button = 'Update';
	$url = url('admin/deal/edit', ['id' => $dealObj->id]);
}
?>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Deal</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $button; ?> Deal
					<a href="<?php echo url('admin/deal/index'); ?>">
						Deal list
					</a>
				</div>
				
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-7">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul style="list-style: none;">
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							@if(session()->has('message'))
								<div class="alert alert-success">
									{{ session()->get('message') }}
								</div>
							@endif
							<form role="form" class="form-horizontal" id="dealForm" enctype="multipart/form-data" method="POST" action="<?php //echo $url; ?>">
								{{ csrf_field() }}
								<div class="col-lg-12">
									<div class="form-group">
										<label>Url</label> <!--<img src="" class="imageloading" style="display:none" />-->
										<input class="form-control" type="text" id="url" name="url" value="<?php echo $urlsite; ?>" required />
										<button type="button" class="btn btn-primary searchurlcontentdata">
											Go
										</button>
									</div>

									<div class="form-group">
										<label>Name</label>
										<input class="form-control" type="text" id="name" name="name" value="<?php echo $name; ?>" required />
									</div>
									
									<div class="form-group" style="display:none">
										<label>Response</label>
										<input class="form-control" type="text" id="response_cookie" name="response_cookie" value="" />
									</div>
									
									<div class="form-group">
										<label style="float:left;">Category</label>
										<ul class="nav navbar-nav" style="float:left;list-style-type: none; ">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">Select Category <b class="caret"></b>
												<span class="categoryOptionTargetSpan"><?php if($categoryname != 'Miscellaneous') { echo $categoryname; } ?></span></a>
												<ul class="dropdown-menu multi-level">
													<?php foreach($categories as $categoryObj) {
															if($categoryObj->slug_name != "miscellaneous") {
															if($categoryObj->pid == null) {
													?>
															<li><a href="#" data-cat="0" categoryid="<?php echo $categoryObj->id; ?>" class="categoryOption"><?php echo $categoryObj->name; ?></a></li>
															<?php } else { ?>
																<li class="dropdown-submenu">
																	<a href="#" class="dropdown-toggle dropDownTarget" data-cat="<?php echo $categoryObj->id; ?>" data-url="<?php echo url("admin/deal/getcategorybyId", ["id" => $categoryObj->id]); ?>" data-toggle="dropdown"><?php echo $categoryObj->name; ?></a>
																</li>
															<?php } ?>
													<?php } } ?>
													
												</ul>
											</li>
											<input type="hidden" name="category" value="<?php echo $category_id; ?>" class="categoryOptionTargetInput" />
										</ul>
									</div>
									
									<div class="form-group">
										<label>Image</label>
										<span class="imagelist">
											
										</span>
										<img src="" id="setimagecontent" height="50" style="display:none" />
										<input type="hidden" name="setimagecontenttext" class="setimagecontenttext" />
										<input class="form-control" type="file" name="image[]" multiple />
									</div>
									
									<div class="form-group deal_image_list_content">
										@include('admin.deal.deal_image_list', ['images' => $images])
									</div>

									<div class="form-group priceshowlabel" <?php if($price == 0) { ?>style="display:none"<?php } ?>>
										<label>Price</label>
										&nbsp;&nbsp;<span><strong class="showprice"><?php echo '$'.$price; ?></strong></span>
									</div>
									
									<input class="form-control" type="hidden" id="price" name="price" value="<?php echo $price; ?>" />
									<input class="form-control" type="hidden" id="totalprice" name="totalprice" value="<?php echo $total; ?>" />
									<input class="form-control" type="hidden" id="store" name="store" value="" />
									
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control" desc="<?php echo $description; ?>" id="txtEditor" name="description"><?php echo $description; ?></textarea>
									</div>
									
									<div class="form-group">
										<button type="submit" class="btn btn-primary" class="categoryadminsubmit">
											<?php echo $button; ?>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal"></div>
@endsection
@push('styles')
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/editor.css') }}">
	<link href="{{ asset('public/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet">
	<style>
		.modal {
			display:    none;
			position:   fixed;
			z-index:    1000;
			top:        0;
			left:       0;
			height:     100%;
			width:      100%;
			background: rgba( 255, 255, 255, .8 ) 
			url('http://i.stack.imgur.com/FhHRx.gif')
			50% 50%
			no-repeat;
		}
		body.loading {
			overflow: hidden;   
		}
		body.loading .modal {
			display: block;
		}
	</style>
@endpush
@push('scripts')
	<script type="text/javascript" src="{{ URL::asset('js/editor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/fancybox/source/jquery.fancybox.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/jquery-sortable.js') }}"></script>
	<script type="text/javascript">
		$( document ).ready(function() {
			$(function  () {
			  $("ol.example").sortable();
			});
			
			$('.fancybox').click(function () {
				/* $.fancybox([
					{ href : '#fancybox-popup-form', height:'300',width:'300' }
				]); */ 
				return false;
			});
			//$("#btnForm").fancybox();
			DealManager.init();
			/* $("body").on('click', '#maindealform', function(event) {
				var content = $('#mycontent').html();
				$.fancybox({
					  'content': content
				});
			}); */
			/* $(function() {
				$("#btnForm").fancybox({
					'onStart': function() { $("#divForm").css("display","block"); },            
					'onClosed': function() { $("#divForm").css("display","none"); }
				});
			}); */
			/* var content = $('#mycontent').html();
			$.fancybox({
				  'content': content
			}); */

			<?php if($dealObj != null) { ?>
				var text = '';
				$("#txtEditor").Editor();
				var html = $("#txtEditor").attr('desc');
				$("#txtEditor").Editor("setText", html);
			<?php } else { ?>
				$("#txtEditor").Editor();
			<?php } ?>
		});
	</script>
@endpush